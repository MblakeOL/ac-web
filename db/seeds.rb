# images = [
#   URI("http://s3.amazonaws.com/assistedchoice/communities/photos/000/000/092/medium/renaissancePeachtree1_compressed.JPG?1412876145"),
#   URI("http://s3.amazonaws.com/assistedchoice/communities/photos/000/000/182/medium/sunriseBuckhead1_(1)_compressed.JPG?1412883550")
# ]

if Location.count == 0 
  puts "Creating Atlanta locations now..."
  Rake::Task["locations:import_all"].invoke 
end

def random_lat
  (33771263..33879359).to_a.sample / 1000000.to_f
end

def random_lng
  (-84550821..-84194108).to_a.sample / 1000000.to_f
end

def random_zip
  @zips ||= Location.where(city: 'Atlanta').map(&:zip).reject {|x| x.length != 5 }
  @zips.sample
end

def random_name
  suffix = ['Pines', 'Village', 'Community', 'Estates', 'At the Village', 'By the River', 'Terrace', 'Manor', 'Heights', 'Gardens', 'Place', 'Mountain', 'Valley', 'River']
  base = [Faker::App.name, Faker::Company.name].sample 
  "#{base} #{suffix.sample}"
end

image1 = File.open(File.join(Rails.root, 'app', 'assets', 'images', 'nonsprite', 'sunriseBuckhead1.jpeg'))
image2 = File.open(File.join(Rails.root, 'app', 'assets', 'images', 'nonsprite', 'renaissancePeachtree1_compressed.jpeg'))

puts
puts Faker::Hacker.say_something_smart
puts
puts "Creating 1000 communities"

1000.times do 
  com = Community.create!({
    location: Location.new,
    name: random_name, 
    city: "Atlanta", 
    state_code: "GA", 
    zip: random_zip, 
    lat: random_lat,
    lng: random_lng,
    address: Faker::Address.street_address
  })
  com.attachments.create!(kind: 'hero', attachment: [image1, image2].sample)
  print '.'
end

puts 
Community.reindex