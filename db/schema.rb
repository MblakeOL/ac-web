# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150114201720) do

  create_table "choice_collection_communities", force: true do |t|
    t.integer  "choice_collection_id", limit: 4
    t.integer  "community_id",         limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "choice_collections", force: true do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "admin_id",           limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "name",               limit: 255
    t.text     "recipients_emails",  limit: 65535
    t.datetime "sent_on"
    t.boolean  "sent",               limit: 1
    t.text     "customer_email",     limit: 65535
    t.text     "community_email",    limit: 65535
    t.text     "personal_message",   limit: 65535
    t.integer  "advocate_user_id",   limit: 4
    t.boolean  "viewed",             limit: 1
    t.integer  "search_template_id", limit: 4
    t.string   "user_email_subject", limit: 255
    t.text     "user_email_body",    limit: 65535
    t.text     "user_email_sig",     limit: 65535
    t.string   "comm_email_subject", limit: 255
    t.text     "comm_email_body",    limit: 65535
    t.text     "comm_email_sig",     limit: 65535
  end

  add_index "choice_collections", ["user_id"], name: "index_choice_collections_on_user_id", using: :btree

  create_table "communities", force: true do |t|
    t.string   "name",                    limit: 255
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "slug",                    limit: 255
    t.string   "salesforce_id",           limit: 255
    t.string   "tracking_number",         limit: 255
    t.string   "industry_type",           limit: 255
    t.text     "description",             limit: 65535
    t.integer  "number_of_units",         limit: 4
    t.boolean  "is_live",                 limit: 1
    t.boolean  "is_snapshot",             limit: 1
    t.integer  "ranking",                 limit: 4,     default: 3
    t.text     "availability",            limit: 65535
    t.text     "specials",                limit: 65535
    t.datetime "availability_updated_at"
    t.datetime "specials_updated_at"
    t.string   "contract_status",         limit: 255
    t.string   "internal_number",         limit: 255
    t.string   "email",                   limit: 255
    t.string   "fax_number",              limit: 255
    t.string   "management_company",      limit: 255
    t.string   "website",                 limit: 255
    t.string   "invoicing_rating",        limit: 255
    t.integer  "number_of_floors",        limit: 4
    t.boolean  "va_bridge",               limit: 1
  end

  create_table "community_attachments", force: true do |t|
    t.string   "community_id",            limit: 255
    t.string   "kind",                    limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "attachment_file_name",    limit: 255
    t.string   "attachment_content_type", limit: 255
    t.integer  "attachment_file_size",    limit: 4
    t.datetime "attachment_updated_at"
    t.integer  "position",                limit: 4
    t.integer  "floorplan_id",            limit: 4
  end

  create_table "community_dinings", force: true do |t|
    t.integer  "meals_per_day",          limit: 4
    t.text     "meals_description",      limit: 65535
    t.string   "breakfast_hours",        limit: 255
    t.string   "lunch_hours",            limit: 255
    t.string   "dinner_hours",           limit: 255
    t.string   "kitchen_hours",          limit: 255
    t.string   "guest_breakfast_charge", limit: 255
    t.string   "guest_lunch_charge",     limit: 255
    t.string   "guest_dinner_charge",    limit: 255
    t.integer  "community_id",           limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "community_glossary_terms", force: true do |t|
    t.integer "community_id",     limit: 4
    t.integer "glossary_term_id", limit: 4
  end

  create_table "community_pdfs", force: true do |t|
    t.string   "name",             limit: 255
    t.integer  "community_id",     limit: 4
    t.string   "internal_name",    limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "pdf_file_name",    limit: 255
    t.string   "pdf_content_type", limit: 255
    t.integer  "pdf_file_size",    limit: 4
    t.datetime "pdf_updated_at"
  end

  create_table "community_tags", force: true do |t|
    t.integer  "community_id", limit: 4
    t.integer  "tag_id",       limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "deposit_fees", force: true do |t|
    t.string   "name",         limit: 255
    t.integer  "fee",          limit: 4
    t.integer  "deposit",      limit: 4
    t.integer  "community_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "dinings", force: true do |t|
    t.integer "meals_per_day",          limit: 4
    t.text    "meals_description",      limit: 65535
    t.string  "breakfast_hours",        limit: 255
    t.string  "lunch_hours",            limit: 255
    t.string  "dinner_hours",           limit: 255
    t.string  "kitchen_hours",          limit: 255
    t.string  "guest_breakfast_charge", limit: 255
    t.string  "guest_lunch_charge",     limit: 255
    t.string  "guest_dinner_charge",    limit: 255
    t.integer "community_id",           limit: 4
  end

  create_table "favorites", force: true do |t|
    t.integer  "user_id",      limit: 4
    t.integer  "community_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "floorplans", force: true do |t|
    t.string   "starting_at",        limit: 255
    t.string   "name",               limit: 255
    t.integer  "baths",              limit: 4
    t.string   "features",           limit: 255
    t.integer  "square_feet",        limit: 4
    t.integer  "beds",               limit: 4
    t.integer  "community_id",       limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "kind",               limit: 255
    t.integer  "legacy_id",          limit: 4
    t.integer  "care_type_id",       limit: 4
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "glossary_terms", force: true do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "kind",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identities", force: true do |t|
    t.integer  "user_id",    limit: 4
    t.string   "provider",   limit: 255
    t.string   "uid",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "leads", force: true do |t|
    t.string   "first_name",   limit: 255
    t.string   "last_name",    limit: 255
    t.string   "email",        limit: 255
    t.string   "phone",        limit: 255
    t.text     "comments",     limit: 65535
    t.string   "kind",         limit: 255
    t.integer  "community_id", limit: 4
    t.string   "sf_id",        limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "locations", force: true do |t|
    t.string   "state_code",     limit: 255
    t.string   "city",           limit: 255
    t.string   "zip",            limit: 255
    t.float    "lat",            limit: 24
    t.float    "lng",            limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "state",          limit: 255
    t.integer  "locatable_id",   limit: 4
    t.string   "locatable_type", limit: 255
    t.string   "address",        limit: 255
    t.integer  "radius",         limit: 4
  end

  create_table "notes", force: true do |t|
    t.integer  "choice_collection_id",           limit: 4
    t.integer  "community_id",                   limit: 4
    t.integer  "user_id",                        limit: 4
    t.text     "body",                           limit: 65535
    t.integer  "admin_id",                       limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "choice_collection_community_id", limit: 4
  end

  create_table "other_services", force: true do |t|
    t.string   "name",             limit: 255
    t.boolean  "included_in_rent", limit: 1
    t.integer  "monthly_cost",     limit: 4
    t.integer  "community_id",     limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "pet_policies", force: true do |t|
    t.integer  "community_id",       limit: 4
    t.boolean  "pets_allowed",       limit: 1
    t.boolean  "dogs_allowed",       limit: 1
    t.string   "dogs_allowed_notes", limit: 255
    t.boolean  "cats_allowed",       limit: 1
    t.string   "cats_allowed_notes", limit: 255
    t.boolean  "breed_restriction",  limit: 1
    t.string   "weight_limit",       limit: 255
    t.string   "pet_deposit",        limit: 255
    t.string   "pet_rent",           limit: 255
    t.string   "pet_fee",            limit: 255
    t.boolean  "pet_services",       limit: 1
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "searches", force: true do |t|
    t.integer  "user_id",    limit: 4
    t.string   "name",       limit: 255
    t.text     "json",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", force: true do |t|
    t.string   "name",         limit: 255
    t.text     "description",  limit: 65535
    t.string   "phone_number", limit: 255
    t.string   "email",        limit: 255
    t.string   "website",      limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "slug",         limit: 255
  end

  create_table "tags", force: true do |t|
    t.string   "name",               limit: 255
    t.string   "kind",               limit: 255
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "value",              limit: 255
    t.integer  "community_id",       limit: 4
    t.boolean  "admin_searchable",   limit: 1,   default: false
    t.string   "group",              limit: 255
    t.boolean  "user_display_value", limit: 1
  end

  create_table "users", force: true do |t|
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role",                   limit: 4,   default: 0
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "phone",                  limit: 255
    t.string   "headshot_file_name",     limit: 255
    t.boolean  "external",               limit: 1,   default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
