class AddCommunityIdToTags < ActiveRecord::Migration
  def change
    add_column :tags, :community_id, :integer
  end
end
