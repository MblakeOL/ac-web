class ChangeAttachmentsCommunityIdToString < ActiveRecord::Migration
  def change
    change_column :community_attachments, :community_id, :string
  end
end
