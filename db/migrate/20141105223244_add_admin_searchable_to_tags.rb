class AddAdminSearchableToTags < ActiveRecord::Migration
  def change
    add_column :tags, :admin_searchable, :boolean, default: false
  end
end
