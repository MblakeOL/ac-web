class ChangeTypeToKind < ActiveRecord::Migration
  def change
    rename_column :community_attachments, :type, :kind
  end
end
