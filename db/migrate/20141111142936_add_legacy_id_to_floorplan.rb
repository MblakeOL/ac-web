class AddLegacyIdToFloorplan < ActiveRecord::Migration
  def change
    add_column :floorplans, :legacy_id, :integer
  end
end
