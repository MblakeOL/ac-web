class ChangeCommunityLatAndLngToFloat < ActiveRecord::Migration
  def change
    change_column :communities, :lat, :float
    change_column :communities, :lng, :float
  end
end
