class AddUserDisplayValueToTags < ActiveRecord::Migration
  def change
    add_column :tags, :user_display_value, :boolean
  end
end
