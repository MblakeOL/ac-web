class AddKindToFloorplans < ActiveRecord::Migration
  def change
    add_column :floorplans, :kind, :string
  end
end
