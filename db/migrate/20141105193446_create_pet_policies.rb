class CreatePetPolicies < ActiveRecord::Migration
  def change
    create_table :pet_policies do |t|
      t.integer :community_id
      t.boolean :pets_allowed
      t.boolean :dogs_allowed
      t.string :dogs_allowed_notes
      t.boolean :cats_allowed
      t.string :cats_allowed_notes
      t.boolean :breed_restriction
      t.string :weight_limit
      t.string :pet_deposit
      t.string :pet_rent
      t.string :pet_fee
      t.boolean :pet_services

      t.timestamps null: false
    end
  end
end
