class AddSentAndSentOnToChoiceCollections < ActiveRecord::Migration
  def change
    add_column :choice_collections, :sent_on, :datetime
    add_column :choice_collections, :sent, :boolean
  end
end
