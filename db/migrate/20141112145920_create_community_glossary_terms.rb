class CreateCommunityGlossaryTerms < ActiveRecord::Migration
  def change
    create_table :community_glossary_terms do |t|
      t.integer :community_id
      t.integer :glossary_term_id
    end
  end
end
