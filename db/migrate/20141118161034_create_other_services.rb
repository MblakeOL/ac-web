class CreateOtherServices < ActiveRecord::Migration
  def change
    create_table :other_services do |t|
      t.string :name
      t.boolean :included_in_rent
      t.integer :monthly_cost
      t.integer :community_id

      t.timestamps null: false
    end
  end
end
