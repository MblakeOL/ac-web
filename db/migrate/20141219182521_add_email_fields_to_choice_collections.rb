class AddEmailFieldsToChoiceCollections < ActiveRecord::Migration
  def change
    add_column :choice_collections, :user_email_subject, :string
    add_column :choice_collections, :user_email_body, :text
    add_column :choice_collections, :user_email_sig, :text
    add_column :choice_collections, :comm_email_subject, :string
    add_column :choice_collections, :comm_email_body, :text
    add_column :choice_collections, :comm_email_sig, :text
  end
end
