class AddCareTypeIdToFloorplan < ActiveRecord::Migration
  def change
    add_column :floorplans, :care_type_id, :integer
  end
end
