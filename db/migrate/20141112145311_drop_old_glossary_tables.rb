class DropOldGlossaryTables < ActiveRecord::Migration
  def up
    drop_table :glossary_terms
    drop_table :glossaries
  end

  def down
    create_table :glossary_terms do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end

    create_table :glossaries do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
