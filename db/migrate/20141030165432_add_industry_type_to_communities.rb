class AddIndustryTypeToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :industry_type, :string
  end
end
