class CreateChoiceCollectionCommunities < ActiveRecord::Migration
  def change
    create_table :choice_collection_communities do |t|
      t.integer :choice_collection_id 
      t.integer :community_id
      t.timestamps null: false
    end
  end
end
