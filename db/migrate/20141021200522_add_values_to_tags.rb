class AddValuesToTags < ActiveRecord::Migration
  def change
    add_column :tags, :value, :string
  end
end
