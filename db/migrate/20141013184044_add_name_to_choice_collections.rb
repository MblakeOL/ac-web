class AddNameToChoiceCollections < ActiveRecord::Migration
  def change
    add_column :choice_collections, :name, :string
  end
end
