class CreateCommunityPdfs < ActiveRecord::Migration
  def change
    create_table :community_pdfs do |t|
      t.string :name
      t.integer :community_id
      t.string :internal_name

      t.timestamps null: false
    end
  end
end
