class MoveCommunityFieldsToLocations < ActiveRecord::Migration
  def up
    begin
      # for dev, doesn't really matter if this fails
      Community.all.each do |c|
        c.location = Location.new
        c.lat = c['lat']
        c.lng = c['lng']
        c.city = c['city']
        c.state = c['state']
        c.zip = c['zip']
        c.address = c['address']
        c.state_code = c['state_code']
        c.save
      end
      Community.reindex
    rescue => ex
      puts "failed to migrate existing communities"
    end

    remove_column :communities, :lat
    remove_column :communities, :lng
    remove_column :communities, :city
    remove_column :communities, :state
    remove_column :communities, :zip
    remove_column :communities, :address
    remove_column :communities, :state_code
  end

  def down
    puts "No such luck!"
    raise ActiveRecord::IrreversibleMigration
  end
end
