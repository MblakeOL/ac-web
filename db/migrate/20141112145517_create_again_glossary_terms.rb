class CreateAgainGlossaryTerms < ActiveRecord::Migration
  def change
    create_table :glossary_terms do |t|
      t.string :name
      t.text :description
      t.string :kind
      t.timestamps
    end
  end
end
