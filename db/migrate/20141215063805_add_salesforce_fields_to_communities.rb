class AddSalesforceFieldsToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :availability, :text
    add_column :communities, :specials, :text
    add_column :communities, :availability_updated_at, :datetime
    add_column :communities, :specials_updated_at, :datetime
  end
end
