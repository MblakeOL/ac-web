class AddRecipientsEmailToChoiceCollection < ActiveRecord::Migration
  def change
    add_column :choice_collections, :recipients_emails, :text
  end
end
