class CreateGlossaryTerms < ActiveRecord::Migration
  def change
    create_table :glossary_terms do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
