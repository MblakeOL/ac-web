class AddAttachmentAttachmentToCommunityAttachments < ActiveRecord::Migration
  def self.up
    change_table :community_attachments do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :community_attachments, :attachment
  end
end
