class AddChoiceCollectionCommunityIdToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :choice_collection_community_id, :integer
  end
end
