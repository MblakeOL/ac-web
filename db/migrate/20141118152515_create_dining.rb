class CreateDining < ActiveRecord::Migration
  def change
    create_table :dinings do |t|
      t.integer :meals_per_day
      t.text :meals_description
      t.string :breakfast_hours
      t.string :lunch_hours
      t.string :dinner_hours
      t.string :kitchen_hours
      t.string :guest_breakfast_charge
      t.string :guest_lunch_charge
      t.string :guest_dinner_charge
      t.integer :community_id
    end
  end
end
