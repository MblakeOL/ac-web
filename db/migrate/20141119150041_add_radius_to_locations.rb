class AddRadiusToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :radius, :integer
  end
end
