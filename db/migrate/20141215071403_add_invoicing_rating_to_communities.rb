class AddInvoicingRatingToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :invoicing_rating, :string
  end
end
