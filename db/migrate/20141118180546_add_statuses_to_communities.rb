class AddStatusesToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :is_live, :boolean
    add_column :communities, :is_snapshot, :boolean
  end
end
