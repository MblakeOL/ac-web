class AddAttachmentPdfToCommunityPdfs < ActiveRecord::Migration
  def self.up
    change_table :community_pdfs do |t|
      t.attachment :pdf
    end
  end

  def self.down
    remove_attachment :community_pdfs, :pdf
  end
end
