class CreateDepositFees < ActiveRecord::Migration
  def change
    create_table :deposit_fees do |t|
      t.string :name
      t.integer :fee
      t.integer :deposit
      t.integer :fee
      t.integer :community_id

      t.timestamps null: false
    end
  end
end
