class AddCountInfoToCommunity < ActiveRecord::Migration
  def change
    add_column :communities, :number_of_floors, :integer
  end
end
