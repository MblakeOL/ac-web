class AddNumberOfUnitsToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :number_of_units, :integer
  end
end
