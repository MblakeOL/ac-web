class AddTrackingNumberToCommunity < ActiveRecord::Migration
  def change
    add_column :communities, :tracking_number, :string
  end
end
