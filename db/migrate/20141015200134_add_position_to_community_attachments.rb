class AddPositionToCommunityAttachments < ActiveRecord::Migration
  def change
    add_column :community_attachments, :position, :integer
  end
end
