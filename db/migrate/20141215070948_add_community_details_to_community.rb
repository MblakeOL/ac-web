class AddCommunityDetailsToCommunity < ActiveRecord::Migration
  def change
    add_column :communities, :email, :string
    add_column :communities, :fax_number, :string
    add_column :communities, :management_company, :string
    add_column :communities, :website, :string
  end
end
