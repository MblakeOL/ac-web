class AddRankingToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :ranking, :integer, default: 3
  end
end
