class CreateCommunityTags < ActiveRecord::Migration
  def change
    create_table :community_tags do |t|
      t.integer :community_id
      t.integer :tag_id

      t.timestamps null: false
    end
  end
end
