class AddFloorplanIdToCommunityAttachments < ActiveRecord::Migration
  def change
    add_column :community_attachments, :floorplan_id, :integer
  end
end
