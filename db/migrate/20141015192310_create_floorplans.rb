class CreateFloorplans < ActiveRecord::Migration
  def change
    create_table :floorplans do |t|
      t.string :starting_at
      t.string :name
      t.integer :baths
      t.string :features
      t.integer :square_feet
      t.integer :beds
      t.integer :community_id

      t.timestamps null: false
    end
  end
end
