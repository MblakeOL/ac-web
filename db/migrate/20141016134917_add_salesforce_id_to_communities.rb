class AddSalesforceIdToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :salesforce_id, :string
  end
end
