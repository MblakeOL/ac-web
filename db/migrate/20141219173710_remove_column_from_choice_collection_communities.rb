class RemoveColumnFromChoiceCollectionCommunities < ActiveRecord::Migration
  def change
    remove_column :choice_collection_communities, :notes
  end
end
