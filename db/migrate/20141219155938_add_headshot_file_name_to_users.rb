class AddHeadshotFileNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :headshot_file_name, :string
  end
end
