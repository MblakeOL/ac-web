class AddStateCodeToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :state_code, :string, limit: 2
  end
end
