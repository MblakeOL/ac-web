class AddInternalNumberToCommunity < ActiveRecord::Migration
  def change
    add_column :communities, :internal_number, :string
  end
end
