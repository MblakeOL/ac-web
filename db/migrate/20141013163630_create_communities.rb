class CreateCommunities < ActiveRecord::Migration
  def change
    create_table :communities do |t|
      t.string :name
      t.decimal :lat
      t.decimal :lng
      t.string :city
      t.string :state
      t.string :zip

      t.timestamps null: false
    end
  end
end
