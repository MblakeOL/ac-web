class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.string :phone_number
      t.string :email
      t.string :website
      t.text :description

      t.timestamps null: false
    end
  end
end
