class CreateChoiceCollections < ActiveRecord::Migration
  def change
    create_table :choice_collections do |t|
      t.integer :user_id, index: true
      t.integer :admin_id
      
      t.timestamps null: false
    end
  end
end
