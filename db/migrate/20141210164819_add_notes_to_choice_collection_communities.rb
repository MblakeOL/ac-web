class AddNotesToChoiceCollectionCommunities < ActiveRecord::Migration
  def change
    add_column :choice_collection_communities, :notes, :text
  end
end
