class AddContractStatusToCommunities < ActiveRecord::Migration
  def change
    add_column :communities, :contract_status, :string
  end
end
