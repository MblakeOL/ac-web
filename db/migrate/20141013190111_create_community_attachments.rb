class CreateCommunityAttachments < ActiveRecord::Migration
  def change
    create_table :community_attachments do |t|
      t.integer :community_id
      t.string :type

      t.timestamps null: false
    end
  end
end
