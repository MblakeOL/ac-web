class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :choice_collection_id
      t.integer :community_id
      t.integer :user_id
      t.text :body
      t.integer :admin_id

      t.timestamps null: false
    end
  end
end
