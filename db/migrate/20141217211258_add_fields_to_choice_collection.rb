class AddFieldsToChoiceCollection < ActiveRecord::Migration
  def change
    add_column :choice_collections, :customer_email, :text
    add_column :choice_collections, :community_email, :text
    add_column :choice_collections, :personal_message, :text
    add_column :choice_collections, :advocate_user_id, :integer
    add_column :choice_collections, :viewed, :boolean
    add_column :choice_collections, :search_template_id, :integer
  end
end
