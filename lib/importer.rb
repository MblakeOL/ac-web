class Importer < ActiveRecord::Base
  @key_pairs = {}
  self.abstract_class = true
  attr_accessor :odb

  def self.import(config, reverse=false, from=0, to=1)
    config.keys.each do |key|
      objs = map key, config[key], reverse, from, to
    end
  end

  def self.map(key, map, reverse, from, to)
    @key_pairs[key] = {}
    unless map["primary_keys"]
      from_key = "id"
      to_key = "id"
    else
      #TODO MAKE THIS MORE FLEXIBLE, allow setting of :primary_keys attribute
    end

    unless reverse
      odb = establish_connection "old_database_#{Rails.env}".to_sym
    else
      ndb = establish_connection Rails.env.to_sym
    end

    new_objs = []
    old = odb.connection.execute("select * from #{map['tables'][from].to_s};")
    old.each do |obj|
      hsh = {}
      ######DEAL WITH SINGLE FOREIGN KEY RELATIONSHIP
      if map["parent"]
        hsh[map["foreign_key"][from]] = @key_pairs[map["parent"]][obj[map["foreign_key"][from]].to_s]
      end
      ######END######
      ######DEAL WITH MULTIPLE FOREIGN KEY RELATIONSHIPS
      if map["parents"]
        if map["foreign_key_lookups"]
          map["foreign_keys"].each_with_index do |fk, index|
            puts "\n\n\n\n\n\n\n"
            puts obj[from_key]
            puts "\n\n\n\n\n\n\n\n\n\n\n"
            lookup = map['foreign_key_lookups'][index][0]
            instance_variable_set(:"@#{fk[0]}_lookup_#{obj[from_key]}", 
              (connection.execute("select * from #{lookup[0]} where #{lookup[1]} = #{obj[lookup[2]]}").first rescue nil))
          end  
        end
      end

      instance_variable_set(:"@original_#{obj[from_key]}", obj)
      ######END#####
      @key_pairs[key][obj[from_key]] = nil
      hsh["from_key"] = obj[from_key]
      if map["columns"]
        map["columns"].each do |column|
          hsh[column[to]] = obj[column[from]].try(:strip)
        end
      end
      new_objs << hsh
    end
    unless reverse
      ndb = establish_connection Rails.env.to_sym
    else
      odb = establish_connection "old_database_#{Rails.env}".to_sym
    end
    new_objs.each do |obj|
      fkey = obj["from_key"]
      obj.delete("from_key")
      # begin
      ar_obj = map['tables'][to].camelize.singularize.constantize.new
      ar_obj.attributes = obj
      puts ar_obj.inspect
      #####FOREIGN KEY LOOKUPS
      if map["foreign_key_lookups"]
        map["foreign_keys"].each_with_index do |fk, index|
          if map['foreign_key_lookups'][index][1].length == 1
            val = (instance_variable_get(:"@#{fk[0]}_lookup_#{fkey}")[map['foreign_key_lookups'][index][0][3]] rescue nil)
            ar_obj.send("#{fk[1]}=", val)
            next
          end
          lookup = map['foreign_key_lookups'][index][1]
          map['foreign_key_lookups'][index][0][3]
          begin
            val = instance_variable_get(:"@#{fk[0]}_lookup_#{fkey}")[map['foreign_key_lookups'][index][0][3]]
          rescue Exception => ex
            next
          end
          puts lookup.inspect
          where = ActiveRecord::Base.send(:sanitize_sql_array, ["#{lookup[1]} = ?", val])
          nk = connection.select("select #{lookup[2]} from #{lookup[0]} where #{where}").first[lookup[2]]
          puts val.inspect
          puts fk.inspect
          ar_obj.send("#{fk[1]}=", nk)
          if map["paperclip_path"]
            str = build_paperclip_string(map, instance_variable_get(:"@original_#{fkey}"))
            begin
              ar_obj.send("#{map["paperclip_name"]}=", URI(str))
            rescue Exception => ex
              puts str
              puts ex
            end
          end
        end  
      end
      ######PAPERCLIP HANDLING
      if map["fixed_values"]
        map["fixed_values"].each do |fv|
          ar_obj.send("#{fv[0]}=", fv[1])
        end
      end
      # raise ar_obj.inspect
      if map['rescue_validations']
        saved = ar_obj.save rescue nil
      elsif map['skip_validations']
        saved = ar_obj.save!(:validate=>false)
      else
        saved = ar_obj.save!
      end
      if saved
        @key_pairs[key][fkey] = ar_obj.send(to_key)
      end
        # raise ar_obj.id.to_s
      # rescue Exception=>ex
        # puts key
        # puts obj.inspect
      # end
    end
    return new_objs
  end

  def self.build_paperclip_string(map, obj)
    str = obj[map["paperclip_url_key"]]
    while str.length < 9
      str.prepend("0")
    end
       img = "#{map['paperclip_path']}/#{str[0..2]}/#{str[3..5]}/#{str[6..8]}/original/#{obj[map['paperclip_file_name']]}"
       # raise img
  end

end


def parse_read_yml
  config = YAML.load_file("#{Rails.root}/lib/temp.yml")
  Importer.import(config)
end