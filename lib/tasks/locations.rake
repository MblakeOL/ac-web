namespace :locations do
  desc "TODO"
  task import_all: :environment do
    Location.delete_all
    require 'csv'

    file = File.join(Rails.root, 'db', 'locations.csv')

    CSV.foreach(file, "r:ISO-8859-15:UTF-8") do |row|
      begin
        next unless row[1] == "US" && row[2] == "GA"
        p row

        # fields from the maxmind file
        locId,country,region,city,postalCode,latitude,longitude,metroCode,areaCode = *row
        next if latitude.blank? || longitude.blank? || region.blank?

        Location.create({
          state_code: region,
          city: city,
          zip: postalCode,
          lat: latitude.to_f,
          lng: longitude.to_f
        })

      rescue => ex
        puts ex
      end
    end
  end
end
