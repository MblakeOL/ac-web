namespace :tags do
  desc "TODO"
  task update_groupings: :environment do
    Tag.all.each do |tag|
      tag.update_attribute(:group, tag.kind)
    end
  end

  task pull_medical_services: :environment do

    client = Databasedotcom::Client.new
    client.client_secret = "1853984187827144717"
    client.client_id = "3MVG9VmVOCGHKYBQ7xjQxv40xRgvi8SpY4Sp3ko9sCDm0Zqrs0.2kLHqPIwKaH_d14yeXA5DEaQRHxVu6xtyK"
    client.authenticate :username => "jens@assistedchoice.com", :password => "Password4247qAcIi9hEtfDz81yI7c7cLN8"
    account_class = client.materialize("Account")
    floor_plans_class = client.materialize("Floor_Plans__c")
    care_types_class = client.materialize("Care_Levels__c")
    #QUESTIONS:
    #EMAIL? 
    Community.all.each do |comm|
      begin 
        acc = account_class.find_by_Name(comm.name)
      rescue
        next
      end
      next unless acc
      parent = nil
      if acc.ParentId
        parent = account_class.find_by_Id(acc.ParentId)
      end
      #--------------COMMUNITY DETAILS/NOT TAGS------------------#
      #IMPORT Contract_Status__c , Specials__c , Availability__c , Last_Availability_Update__c, Last_Specials_Update__c
      #IMPORT Phone, Website, Fax, ParentId AS management_company, Invoicing_Rating__c
      comm.update_attributes("industry_type"=>nil, "availability"=>acc.Availability__c, 
        "specials"=>acc.Specials__c, "availability_updated_at"=>acc.Last_Availability_Update__c, "specials_updated_at"=>acc.Last_Specials_Update__c, 
        "contract_status"=>acc.Contract_Status__c, "internal_number"=>acc.Phone, "email"=>nil, "fax_number"=>acc.Fax, 
        "management_company"=>parent.try(:Name), "website"=>acc.Website, "invoicing_rating"=>acc.Invoicing_Rating__c, :number_of_floors=>acc.Total_Floor_Count__c, :va_bridge=>acc.VA_Bridge__c)
      #--------------MEDICAL CARE SERVICES/Tags------------------#
      #IMPORT Grooming__c, Bathing__c, Dressing__c, Oral_Hygiene__c, Toileting__c, Redireciton__c, Level_of_Medicine_Administration__c
      #IMPORT Diabetic_Management__c, Transfer_Assistance_Offered__c, Incontinence_Management__c, Oxygen_Management_Provided1__c, Behavioral_Care_Offered__c
      #IMPORT Emergency_Pendant_Offered1__c, Emergency_Pull_Cord1__c, Wander_Guard_Offered__c, Ostomy_Assistance__c
      #IMPORT Special_Medications_Administered__c, Feeding_Assistance_Offered__c, Feeding_Tube_Care_Provided__c
      #IMPORT Wound_Care__c, Catheter_Care_Offered__c, Empty_Change_Catheter_Bags__c, MC_Features__c, Progressive_MC_Living_Areas__c
      #IMPORT Respite_allowed_in_MC__c, Day_Care_allowed_in_MC__c, Secure_Outdoor_Access__c, Secure_Outdoor_Access__c
      #IMPORT MC_Integrated_Dining_with_AL__c, IL_Dining_Program__c, Special_Diets__c
      services = ["Lease_Terms__c","Grooming__c", "Bathing__c", "Dressing__c", "Oral_Hygiene__c", "Toileting__c", "Redireciton__c", "Level_of_Medicine_Administration__c", "Diabetic_Management__c", "Transfer_Assistance_Offered__c", "Incontinence_Management__c", "Oxygen_Management_Provided1__c", "Behavioral_Care_Offered__c","Emergency_Pendant_Offered1__c", "Emergency_Pull_Cord1__c", "Wander_Guard_Offered__c", "Ostomy_Assistance__c","Special_Medications_Administered__c", "Feeding_Assistance_Offered__c", "Feeding_Tube_Care_Provided__c", "Wound_Care__c", "Catheter_Care_Offered__c", "Empty_Change_Catheter_Bags__c", "MC_Features__c", "Progressive_MC_Living_Areas__c", "Respite_allowed_in_MC__c", "Day_Care_allowed_in_MC__c", "Secure_Outdoor_Access__c", "Secure_Outdoor_Access__c", "MC_Integrated_Dining_with_AL__c", "IL_Dining_Program__c", "Special_Diets__c"]
        service_keys = services.map {|s| s.gsub(/1__c/, '')}
        service_keys = service_keys.map {|s| s.gsub(/__c/, '')}
        service_keys = service_keys.map {|s| s.gsub(/_/, ' ')}
        puts comm.id
      services.each_with_index do |service, index|
        value = acc.send(service.to_sym)
        next if value.to_s == ""
        if value.class == Array
          value.each do |val|
            addTag = true
            if val.class == FalseClass || val.class == TrueClass
              addTag = val
              val = service_keys[index]
            end
            tag = Tag.find_or_create_by(:kind=>"medical care service", :name=>val, :group=>service_keys[index])
            unless comm.tags.include? tag
              begin
                if addTag
                  comm.tags << tag
                end
              rescue Exception => ex
                puts tag.id 
                puts comm.id
              end
            end
          end
        else
          addTag = true
          group = service_keys[index]
          if value.class == FalseClass || value.class == TrueClass
            addTag = value
            value = service_keys[index]
            group = "medical care service"
          end
          tag = Tag.find_or_create_by(:kind=>"medical care service", :name=>value, :group=>group)

          unless comm.tags.include? tag
            begin
              if addTag == true
                comm.tags << tag
              end
            rescue
              puts tag.id
              puts comm.id
            end
          end
        end

      end

    end



  end

end