namespace :communities do
  desc "TODO"

  task update_data_for_view: :environment do
    #COMMUNITIES
    Community.all.each do |c|
      tags = c.tags.where(:kind=>"Care Type")
      if tags.length == 1 and tags.include? Tag.where(:name=>"Personal Care").first
        c.tags << Tag.where(:name=>"Assisted Living").first
      end
    end
    #FLOORPLANS
    Floorplan.where(:community_id=>nil).delete_all
    Floorplan.all.each do |fp|
      if fp.care_type_id == Tag.where(:name=>"Personal Care").first.id
        fp.update_attribute(:care_type_id, Tag.where(:name=>"Assisted Living").first.id)
      end
    end
  end

  task import_all: :environment do
    def random_lat
      (33771263..33879359).to_a.sample / 1000000.to_f
    end

    def random_lng
      (-84550821..-84194108).to_a.sample / 1000000.to_f
    end

    print "Delete old Community, Tag, Floorplan models? (y/n) "
    input = STDIN.gets.chomp.downcase
    raise "please type 'y' or 'n'" unless ['y', 'n'].include?(input)

    if input == 'y'
      puts "Deleting old data"
      Community.delete_all
      Tag.delete_all
      Floorplan.delete_all
    end
    puts "Starting import from SalesForce..."

    client = Databasedotcom::Client.new
    client.client_secret = "1853984187827144717"
    client.client_id = "3MVG9VmVOCGHKYBQ7xjQxv40xRgvi8SpY4Sp3ko9sCDm0Zqrs0.2kLHqPIwKaH_d14yeXA5DEaQRHxVu6xtyK"
 
   client.authenticate :username => "jens@assistedchoice.com", :password => "Password4247qAcIi9hEtfDz81yI7c7cLN8"

    account_class = client.materialize("Account")
    floor_plans_class = client.materialize("Floor_Plans__c")
    care_types_class = client.materialize("Care_Levels__c")

    accs = Account.find_all_by_RecordTypeId("01270000000MygwAAC")
    next_page = true
    i = 0
    while i < 10000

      begin
        i += 1
        print '.'
        next_page = accs.next_page?
        accs.each do |acc|
          # next if Community.where(:salesforce_id=>acc.Id).first
          next if acc.Listing_Type__c != "Snapshot Live" && acc.Listing_Type__c != "Full Live"
          comm = Community.create(location: Location.new, :name=>acc.Name, :city=>acc.BillingCity, :state_code=>acc.BillingState, :zip=>acc.BillingPostalCode, :address=>acc.BillingStreet, :salesforce_id=>acc.Id, :industry_type=>acc.Industry, :units=>acc.Unit_Count__c)
          next if comm.errors.messages.length > 0
          comm.update_attributes(:lng=>random_lng, :lat=>random_lat)
          acc_plans = floor_plans_class.find_all_by_Account_Name__c(acc.Id)
          acc_plans.each do |plan|
            Floorplan.create( starting_at: plan.Starting_Price__c.to_i, kind: plan.Apartment_Type__c, name: plan.Name, baths: plan.Bathrooms__c, 
              features: plan.Features__c.join(","), square_feet: plan.Square_Footage__c.to_i, beds: plan.Apartment_Type__c.to_i, community_id: comm.id)
          end
          ###Care Types

          care_types = care_types_class.find_all_by_Account_Name__c(acc.Id)
          care_types.each do |care_type|
            Tag.create(:kind=>"care type", :name=>care_type.Care_Type__c, :value=>care_type.Care_Description__c,:community_id=>comm.id)
          end
          ###Apartment Features
          apartment_features = acc.Apartment_Features__c
          apartment_features.each do |feat|
            Tag.create(:kind=>"apartment feature", :name=>feat,:community_id=>comm.id)
          end
          ###Community Features
          community_features = acc.Community_Features__c
          community_features.each do |feat|
            Tag.create(:kind=>"community feature", :name=>feat,:community_id=>comm.id)
          end
          ###Services in Base Rent
          community_features = acc.Services_Included_in_Base_Rent__c
          community_features.each do |feat|
            Tag.create(:kind=>"included services", :name=>feat,:community_id=>comm.id)
          end
          ###Breakfast Hours
          Tag.create(:kind=>"dining info", :name=>"breakfast hours", :value=>acc.Breakfast_Hours__c,:community_id=>comm.id)
          ###Lunch Hours
          Tag.create(:kind=>"dining info", :name=>"lunch hours", :value=>acc.Lunch_Hours__c,:community_id=>comm.id)
          ###Dinner Hours
          Tag.create(:kind=>"dining info", :name=>"dinner hours", :value=>acc.Dinner_Hours__c,:community_id=>comm.id)
          ###Pet Restrictions
          Tag.create(:kind=>"pet info", :name=>"pet restrictions", :value=>acc.Pet_Restrictions__c,:community_id=>comm.id)
          ###Pet Services
          Tag.create(:kind=>"pet info", :name=>"pet services", :value=>acc.Pet_Services__c,:community_id=>comm.id)
          ###Cats Allowed
          Tag.create(:kind=>"pet info", :name=>"cats allowed", :value=>acc.Cats_Allowed__c,:community_id=>comm.id)
          ###Dogs Allowed
          Tag.create(:kind=>"pet info", :name=>"dogs allowed", :value=>acc.Dogs_Allowed__c,:community_id=>comm.id)


        end
        accs = accs.next_page
      rescue => ex
        puts "RESCUED! - most likely a timeout from SnailsForce"
        puts ex.message
        i -= 1
      end
    end
    puts "\nDone"
  end

  task check_images: :environment do
    Floorplan.all.each do |fp|
      next if fp.photo.blank?
      url = fp.photo.url
      if Net::HTTP.get_response(URI(url)).code != "200"
        puts "not working: #{url}"
        fp.photo = nil
        fp.save
      else
        print '.'
      end
    end
  end

  # /images/66/fp/102/sm.jpg
  # /public/images/:community_id/[fp|pdf|img]/:attachment_id/[orig|sm|md|lg].jpg

  task import_images_from_old: :environment do
    old_db = ActiveRecord::Base.establish_connection({
      adapter: 'postgresql',
      encoding: 'utf8',
      database: 'assisted_choice_development',
      username: 'assisted_choice'
    })
    pics = {}
    old_communities = old_db.connection.execute("select * from communities")
    photos = old_db.connection.execute("select * from community_photos")
    floorplans = old_db.connection.execute("select * from floor_plans")
    pdfs = old_db.connection.execute("select cp.custom_name, cp.community_id, cp.id, cpn.name, cp.pdf_file_name
     from community_pdfs cp JOIN community_pdf_names cpn ON cp.community_pdf_name_id = cpn.id")
    photos.each do |photo|
      if photo["community_id"] == "326"
        puts photo["photo_file_name"]
      end
      pics[photo["community_id"]] = {} unless pics[photo["community_id"]]
      pics[photo["community_id"]][photo["id"]] = photo["photo_file_name"]
      if photo["community_id"] == "326"
        puts pics[photo["community_id"]]
      end
    end
    new_db = ActiveRecord::Base.establish_connection({
      adapter: 'mysql2',
      encoding: 'utf8',
      username: 'root',
      database: 'assisted_choice_development'
    })
    # old_communities.each do |comm|
    #   community = Community.where(:salesforce_id=>comm['id']).first.update_attributes(:is_live=>comm['live'], :is_snapshot=>comm['snapshot'])
    # end

    # return nil

    # pdfs.each do |pdf|
    #   str = pdf['id']
    #   while str.length < 9
    #     str.prepend("0")
    #   end
    #   img = "http://s3.amazonaws.com/assistedchoice/community_pdfs/pdfs/#{str[0..2]}/#{str[3..5]}/"+str[6..8]+"/original/#{pdf['pdf_file_name']}"
    #   comm = Community.where(:salesforce_id=>pdf['community_id']).first
    #   begin
    #     cpdf = comm.community_pdfs.create(:name=>pdf['custom_name'], :internal_name=>pdf['name'], :pdf=>URI(img))
    #   rescue Exception => ex
    #     puts img
    #   end
    # end
    # # return nil 
    puts floorplans.count
    floorplans.each do |fp|
      nfp = Floorplan.where(:legacy_id=>fp['id']).first
      next if !nfp
      str = fp['id']
      while str.length < 9
        str.prepend("0")
      end
      img = "http://s3.amazonaws.com/assistedchoice/floor_plans/images/#{str[0..2]}/#{str[3..5]}/"+str[6..8]+"/original/#{fp['image_file_name']}"
      if !CommunityAttachment.where(:floorplan_id=>nfp.id, :kind=>"floorplan", :attachment_file_name=>fp['image_file_name']).first and !fp['image_file_name'].blank?
        begin
          nfp.uri = img
          nfp.save
        rescue Exception => ex
          puts ex.backtrace
          puts img
          puts nfp.inspect
          puts img
          puts ex
        end
      end
    end
    return nil
    processed = []
    old_communities.each do |c|
      return nil if processed.include? c['id']
      processed << c['id']
      puts c['id']
      comm = Community.where(:name=>c['name']).first
      next if !comm
      comm.update_attribute(:tracking_number, c['twilio_number'])
      # comm.attachments.create(:)
      hero_image = c['photo_file_name']
      img = "http://s3.amazonaws.com/assistedchoice/communities/photos/000/000/"+("00"+c["id"])[-3..-1]+"/original/#{c['photo_file_name']}"
      if c['id'] == "326"
        puts "Found it"
        puts pics[c['id']]
      end
      # unless comm.attachments.where(:attachment_file_name=>c['photo_file_name']).first
        begin
          puts "TRYING TO SAVE HERO IMAGE"
          attachment = comm.attachments.new(:kind=>"hero")
          attachment.uri = img
          attachment.save
        rescue Exception => ex
          puts ex
          # puts c.inspect
          # photos.
          # puts pics[c['id']]
        end
      # end
      next if !pics[c['id']]
      pics[c['id']].keys.each do |key|
        begin 
          str = key.dup.to_s
          while str.length < 9
            str.prepend("0")
          end
          img = "http://s3.amazonaws.com/assistedchoice/community_photos/photos/#{str[0..2]}/#{str[3..5]}/"+str[6..8]+"/original/#{pics[c['id']][key]}"
          pics[c['id']][key]
          # unless comm.attachments.where(:attachment_file_name=>pics[c['id']][key]).first
            puts "TRYING TO SAVE PHOTO"
            attachment = comm.attachments.new(:kind=>"photo")
            attachment.uri = img
            attachment.save
          # end
        rescue Exception => ex
          puts ex

          puts img
          # raise ex
        end
      end
    end
  end

  desc "needs this"
  task import_service_care_charges: :environment do
    old_db = ActiveRecord::Base.establish_connection({
      adapter: 'postgresql',
      encoding: 'utf8',
      database: 'assisted_choice_development',
      username: 'assisted_choice'
    })
    # ...
  end

  desc "TODO"
  task import_from_old: :environment do
    old_db = ActiveRecord::Base.establish_connection({
      adapter: 'postgresql',
      encoding: 'utf8',
      database: 'assisted_choice_development',
      username: 'assisted_choice'
    })

    # old associations
    class CommunityCareType < ActiveRecord::Base
      belongs_to :community
      belongs_to :care_type
    end

    class Community < ActiveRecord::Base
      attr_accessor :care_types_array

      has_many :care_types, :through => :community_care_types
      has_many :community_care_types, :dependent => :delete_all, :autosave => true
    end

    class CareType < ActiveRecord::Base
      has_many :community_care_types
    end

    old_communities = Community.all.map do |c|
      c.care_types_array = c.care_types.map(&:name).compact.uniq.sort
      c
    end

    # load OUR newer version of Community
    require './app/models/community'

    new_db = ActiveRecord::Base.establish_connection({
      adapter: 'mysql2',
      encoding: 'utf8',
      username: 'root',
      database: 'assisted_choice_development'
    })

    old_communities.each do |c|
      newcom = Community.find_or_initialize_by({
        name: c['name'].strip
      })

      newcom.address ||= (c['address'] || '').strip
      newcom.city ||= (c['city'] || '').strip
      newcom.state_code ||= (c['state'] || 'GA').strip
      newcom.zip ||= c['zip']
      newcom.lat ||= c['latitude']
      newcom.lng ||= c['longitude']

      begin
        unless newcom.id.nil? # don't create new records
          newcom.save 

          c.care_types_array.each do |name|
            newcom.tags.create({ kind: 'care type', name: name })
          end
        end
      rescue => ex
        pp newcom 
        pp newcom.errors.full_messages
      end
      print '.'
    end
    Community.reindex

  end


end
