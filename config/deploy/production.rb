set :stage, :production

server '74.207.227.139',  user: 'deploy', roles: %w{app web db} # web1
server '173.230.128.170', user: 'deploy', roles: %w{app web db} # web2

server '173.230.132.223', user: 'deploy', roles: %w{mysql}, no_release: true
server '66.228.61.235',   user: 'deploy', roles: %w{elasticsearch}, no_release: true

set :branch, 'production'
