set :stage, :staging

server '50.116.33.183', user: 'deploy', roles: %w{app web db mysql}
server '50.116.33.183', user: 'deploy', roles: %w{elasticsearch}, no_release: true

set :branch, 'master'
