# config valid only for Capistrano 3.2.1
lock '3.2.1'
require_relative 'environment'
set :application, 'ac_web'
set :repo_url, "git@github.com:AssistedChoice/#{fetch(:application)}.git"

set :deploy_to, "/var/www/#{fetch(:application)}"

set :pty, true

set :linked_files, %w{config/database.yml}

set :linked_dirs, %w{log tmp bin public/uploads}

set :default_env, { 
  path: "/usr/local/bin:$PATH",
 'ELASTICSEARCH_URL' => fetch(:stage) == :production ? '192.168.138.29' : '127.0.0.1'
}

set :filter, :roles => %w{app web}

namespace :deploy do

  desc 'Install base system requirements'
  task :bootstrap do

    # !!!do this first to all nodes
    # useradd -m -s /bin/bash deploy
    # echo 'deploy ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
    # sudo su - deploy
    # mkdir.ssh && add desired key to authorized keys

    on roles(:app) do
      # create ac_web user and group
      execute "sudo useradd -m -s /bin/bash ac_web; true"

      # packages
      execute "sudo apt-get -y update"

      %w(curl autoconf build-essential libssl-dev libyaml-dev 
        libreadline6 libreadline6-dev zlib1g zlib1g-dev libxml2-dev libxslt-dev 
        libc6-dev ncurses-dev automake libtool bison pkg-config nginx nodejs
        build-essential git htop mysql-client libmysqlclient-dev monit imagemagick).each do |pkg|
        execute "sudo apt-get -y install #{pkg}"
      end

      begin
        execute "test -f /usr/local/bin/ruby"
      rescue
        execute "wget http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.2.tar.gz"
        execute "tar -xzvf ruby-2.1.2.tar.gz"
        # --with-readline-dir=/usr/lib/x86_64-linux-gnu/libreadline.so
        execute "cd ruby-2.1.2 && ./configure && make && sudo make install"
      end
      execute "sudo chown -R ac_web:ac_web /usr/local/lib/ruby"

      begin
        execute "test -f /usr/local/bin/bundle"
      rescue
        execute "sudo /usr/local/bin/gem install bundler"
      end      

      # these need to exist before you can do anything else
      execute "sudo mkdir -p #{fetch(:deploy_to)}"
      execute "sudo chown -R deploy:deploy #{fetch(:deploy_to)}"
    end

    on roles(:elasticsearch) do
      execute "sudo apt-get -y update"
      execute "sudo apt-get install -y monit"
      begin
        execute "test -f /etc/init.d/elasticsearch"
      rescue
        execute "sudo apt-get -y install openjdk-7-jre-headless"
        execute "wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.2.1.deb"
        execute "sudo dpkg -i elasticsearch-1.2.1.deb"
        execute "rm elasticsearch-1.2.1.deb"
        execute "sudo update-rc.d elasticsearch defaults 95 10"
        execute "sudo /etc/init.d/elasticsearch start"
      end
    end

    on roles(:mysql) do
      # just run this by hand:
      #
      # execute "sudo apt-get -y update"

      # %w(mysql-server).each do |pkg|
      #   execute "sudo apt-get -y install #{pkg}"
      # end

      # execute %Q{ echo 'create database if not exists assisted_choice;' | mysql -uroot -p#{password} }
      # execute %Q{ echo "GRANT ALL PRIVILEGES ON assisted_choice.* TO assisted_choice IDENTIFIED BY '#{db_pass}'" | mysql -uroot -p#{password} }
    end
  end

  # don't fail if linked files aren't present, we'll provide them in copy_configs
  # Rake::Task["deploy:check:linked_files"].clear

  desc 'Upload relevant configuration before deploying'
  # not done here, but you need to `crontab -e` as deploy user
  # and paste in the content of users_cron.rb
  task :upload_configs do
    on roles(:app) do
      within shared_path do
        # these need to exist before you can do anything else
        execute "sudo mkdir -p #{shared_path}"
        execute "sudo mkdir -p #{fetch(:deploy_to)}"
        execute "sudo mkdir -p #{shared_path}/tmp/pids"
        execute "sudo mkdir -p #{shared_path}/config"

        execute "sudo chown -R deploy:deploy #{fetch(:deploy_to)}"

        upload!("config/database.yml", "#{shared_path}/config/database.yml")

        upload!("config/#{fetch(:stage)}_bashrc", "bashrc")
        execute "sudo mv ~/bashrc /etc/profile.d/#{fetch(:application)}.sh"

        # monit
        upload!('config/base_monitrc', 'monitrc')
        execute "sudo mv monitrc /etc/monit/monitrc"
        execute "sudo chown -R root:root /etc/monit"
        execute "sudo chmod -R 700 /etc/monit"

        upload!("config/#{fetch(:stage)}.monitrc", 'ac_web.monitrc')
        execute "sudo mv ac_web.monitrc /etc/monit/conf.d/ac_web.monitrc"
        execute "sudo monit; sudo monit reload; true"
        execute "sudo update-rc.d monit defaults 95 10"

        # nginx
        upload!('config/nginx.conf', 'nginx.conf')
        execute "sudo mv nginx.conf /etc/nginx/nginx.conf"

        upload!("config/#{fetch(:stage)}_nginx.conf", 'nginx.conf')
        execute "sudo mv nginx.conf /etc/nginx/sites-enabled/nginx.conf"

        execute "sudo mkdir -p /var/log/nginx/ac_web"
        execute "sudo touch /var/log/nginx/#{fetch(:application)}/access.log /var/log/nginx/#{fetch(:application)}/error.log"

        upload!("config/#{fetch(:stage)}_logrotate.conf", 'ac_web.conf')
        execute 'sudo mv ac_web.conf /etc/logrotate.d/ac_web'
      end
    end
  end

  desc 'Restart application'
  task :restart do
    on(roles(:app), in: :sequence) do |server|
      within release_path do
        if fetch(:stage) == :production
          execute :rake, "sitemap:refresh", "RAILS_ENV=#{fetch(:stage)}"
          execute "sudo ln -sf /mnt/files #{release_path}/public/system"
        else
          # link assets to shared dir if not production
          execute "sudo ln -sf /var/www/ac_web/shared/public/system #{release_path}/public/system"
        end
      end

      within deploy_path do
        execute "sudo mkdir -p public/uploads/tmp"
        execute "sudo chown -R ac_web:ac_web public"
      end

      execute "sudo chown -R ac_web:ac_web /var/www/ac_web/shared" # /var/www/ac_web/public
      execute "sudo chown -R ac_web:ac_web /var/www/ac_web/shared/tmp"
      execute "sudo monit restart puma"
      execute "sudo service nginx restart"
      # purge cache
    end
  end

  before :starting, :upload_configs
  after :publishing, :restart
end
