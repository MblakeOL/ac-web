require File.join(Rails.root, 'lib', 'state')

Rails.application.routes.draw do
  resources :leads, only: [:new, :create]

  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks', registrations: 'registrations' } 
  devise_scope :user do
    delete '/users/delete_headshot', to: 'registrations#delete_headshot', as: :delete_headshot
  end

  namespace :admin do

    resources :communities do
      collection do
        post :search
      end
      scope module: 'community_associations' do
        resources :pet_policies
        resources :photos
        resources :floorplans
        resources :attachments
        resources :community_dining
        resources :costs, only: [:index]
        resources :deposit_fees
        resources :other_services
      end
    end
    resources :users do 
      collection do
        get :search
        get :report
      end
    end
    resources :choice_collections, except: [:new]
    resources :choice_collection_communities, only: [:create, :destroy]
    resources :glossary_terms
    resources :geolocations, only: [:index]
    resources :tags, except: [:destroy] do
      member do 
        get :category_changed
      end
    end
    root to: 'dashboard#index'
  end

  resources :communities, except: [:index, :create] do
    collection do
      get :autocomplete
      post :map_search
    end
  end

  get '/:state/:city/:id', to: 'communities#show', as: :community_vanity, constraints: { state: State::REGEXP }

  resources :searches
  resources :notes
  resources :choice_collections, only: [:index, :show]
  
  get '/favorites', to: 'communities#favorites', as: :favorites
  post '/communities/:id/favorite', to: 'communities#favorite'
  post '/communities/:id/unfavorite', to: 'communities#unfavorite'

  get '/about', to: 'pages#about'

  get '/policy', to: 'pages#policy'

  get '/terms', to: 'pages#terms'

  get '/users/preregistration', to: 'users#preregistration'

  get '/users', to: 'pages#users'

  get '/user-show', to: 'pages#user_show'

  get '/dashboard', to: 'dashboard#index'

  root to: 'communities#index', via: [:get, :post]

  get '/popup', to: redirect('/')

  match '/:state/:city', to: 'communities#city_lander', via: [:get, :post], constraints: { state: State::REGEXP }
end
