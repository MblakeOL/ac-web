# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.assistedchoice.com"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  vanity_urls = {}
  Community.find_each do |community|
    next unless community.valid? && (community.is_live || community.is_snapshot) && community.state
    add community_vanity_path(community.state, community.city.parameterize, community.slug), lastmod: community.updated_at
    city_state_lander = "/#{community.state}/#{community.city.parameterize}"
    unless vanity_urls[city_state_lander]
      add city_state_lander
    end
  end
end
