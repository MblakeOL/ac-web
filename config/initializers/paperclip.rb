Paperclip::Attachment.default_options[:url] = ':s3_domain_url'
Paperclip::Attachment.default_options[:path] = '/:class/:attachment/:id_partition/:style/:filename'
Paperclip.interpolates('id_partition') do |attachment, style|
  attachment.instance.community.id.to_s.parameterize
end
