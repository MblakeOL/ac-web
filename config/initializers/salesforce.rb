config = YAML.load_file(File.join(::Rails.root, 'config', 'salesforce.yml'))
config = config.has_key?(::Rails.env) ? config[::Rails.env] : config

SF_CONFIG = config unless defined?(SF_CONFIG)