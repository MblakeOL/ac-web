Feature: New User Signup
  In order to create personalized content
  As a customer
  I need to be able to create a user account


  Scenario: View signup page
    Given I am on "users/sign_in"
    When I click "Sign up"
    Then I should see "Email"

  Scenario: New user signup
    Given I am on "users/sign_up"
    When I fill in "user_email" with "test@test.com"
    And I fill in "user_password" with "newpassword"
    And I fill in "user_password_confirmation" with "newpassword"
    And I press "Sign up"
    Then I should be on the home page