Feature: User login
  In order to create personalized content
  As a customer
  I need to be able to create a user account


  Scenario: User can view sign in page
    Given I am on "users/sign_in"
    Then I should see "Email"

  Scenario: User can log in
    Given I am on "users/sign_in"
    And there is a valid user for "login_user@test.com" with "newpassword"
    When I fill in "user_email" with "login_user@test.com"
    And I fill in "user_password" with "newpassword"
    And I press "Log in"
    Then I should be on the home page