
Given(/^I am on "(.*?)"$/) do |arg1|
  visit arg1 # express the regexp above with the code you wish you had
end

When(/^I click "(.*?)"$/) do |arg1|
  click_link arg1 # express the regexp above with the code you wish you had
end

Then(/^I should see "(.*?)"$/) do |arg1|
  find_field('user_password').visible? # express the regexp above with the code you wish you had
end

When(/^I fill in "(.*?)" with "(.*?)"$/) do |arg1, arg2|
  fill_in arg1, :with=>arg2 # express the regexp above with the code you wish you had
end

When(/^I press "(.*?)"$/) do |arg1|
  click_button arg1 # express the regexp above with the code you wish you had
end

Then(/^I should be on the home page$/) do
  assert_equal(root_path, current_path) # express the regexp above with the code you wish you had
end

Given(/^there is a valid user for "(.*?)" with "(.*?)"$/) do |arg1, arg2|
  User.create!(:email=>arg1, :password=>arg2) # express the regexp above with the code you wish you had
end