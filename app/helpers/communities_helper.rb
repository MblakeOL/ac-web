module CommunitiesHelper

  def cost(amt)
    return 'N/A' if amt.to_i == 0
    "$#{number_with_delimiter(amt, delimeter: ',')}"
  end

  def community_image(src, opts={})
    src = "nonsprite/nophoto.png" if src.blank?
    if opts[:small]
      src = src.sub("medium", "small")
      opts.delete(:small)
    end
    image_tag(src, opts.merge({ onerror: "this.onerror=null; this.src='#{asset_path("nonsprite/nophoto.png")}'" }))
  end

  def get_location_string(community)
    zip = if community.location
      community.location['zip']
    else
      community['zip']
    end
    "#{community.city_state} #{zip}"
  end

  def community_path(community)
    return unless community.city && community.state && community.slug
    community_vanity_path(community.state, community.city.parameterize, community.slug)
  end

  def phone_number(community)
    num = raw_phone_number(community)
    return link_to(num, "tel:#{num}")
  end

  def raw_phone_number(community)
    number_to_phone(community.try(:tracking_number).try(:gsub, /[^0-9]/, '') || '4043345500', area_code: true)
  end

  def normalized_bool(val)
    val.blank? ? "No" : "Yes"
  end

  def normalized_beds(intval)
    return 'Shared' if intval == -1
    return 'Studio' if intval == 0
    intval
  end

  def favorites_class(result)
    return 'saved' if action_name == 'favorites'
    (@favorite_ids || []).include?(result.id) ? 'saved' : ''
  end

  def tag_section(community, title, kind)
    tags = community.tags_for(kind)
    return if tags.blank?

    res = content_tag(:h4, title)
    res += content_tag :ul do
      tags.each do |tag|
        concat content_tag(:li, tag.name)
      end
    end
    res
  end

  def check_dining(dining)
    return false if dining.blank?
    dining.attributes.values.reject{|fp| fp.blank? || fp == "0.0"}.count > 4
  end

  def check_pet_policy(policy)
    return true if policy and (policy.dogs_allowed? or policy.cats_allowed?)
    false
  end

  def user_or_admin(ccc)
    return unless ccc
    if current_user == ccc.choice_collection.user_id
    else
      return ccc.choice_collection.advocate.try(:name_or_email) || ccc.choice_collection.admin.name_or_email
    end
  end
end
