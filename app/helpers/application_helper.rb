module ApplicationHelper

  def seo_title
    title = "Senior Living Atlanta, Georgia - Assisted Choice"
    if params[:city] && params[:state]
      title = "Senior Living #{params[:city].try(:titleize)}, #{params[:state].try(:titleize)} - Assisted Choice"
    end
    if controller_name == "communities" && action_name == "show"
      name = @search_result.try(:name)
      title = "#{name.blank? ? '' : name + ' - '}#{title}"
    end
    title
  end

  def formatted_phone(stg)
    return '' if stg.blank?
    stg = stg.gsub(/[^0-9]/, '')
    number_to_phone(stg, area_code: true)
  end
end
