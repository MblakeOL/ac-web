class Note < ActiveRecord::Base
  belongs_to :choice_collection_community
  belongs_to :user
  belongs_to :admin, class_name: "User", foreign_key: "admin_id"
  belongs_to :community
end
