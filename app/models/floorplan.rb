class Floorplan < ActiveRecord::Base
  belongs_to :community
  belongs_to :care_type, class_name: 'Tag'

  OpenURI::Buffer.send :remove_const, 'StringMax' if OpenURI::Buffer.const_defined?('StringMax')
  OpenURI::Buffer.const_set 'StringMax', 0

  attr_accessor :tmp_file, :uri

  before_save :process

  def process
    return true if self.uri.blank? and self.tmp_file.blank?
    generate_identifier
    create_tmp_dir
    get_file
    resize
    compress
    move_files
  end

  def create_tmp_dir
    FileUtils.mkdir_p(File.join(Rails.root, 'public/system/floorplans', self.community_id.to_s, self.photo_file_name))
  end

  def generate_identifier
    self.photo_file_name = SecureRandom.hex
  end

  def get_file
    if self.uri =~ URI::regexp
      self.tmp_file = open(self.uri)
    end
  end

  def image_path
    File.join('/system/floorplans', self.community_id.to_s, self.photo_file_name, 'image.jpg')
  end

  def resize
    `convert #{tmp_file.path} -resize 800x #{tmp_file.path}.jpg`  
  end

  def compress
    `mogrify -compress JPEG -quality 70 #{tmp_file.path}.jpg`
  end

  def move_files
    FileUtils.mv("#{tmp_file.path}.jpg", File.join(Rails.root, 'public/system/floorplans', self.community_id.to_s, self.photo_file_name, "image.jpg"))
  end
end
