class ChoiceCollection < ActiveRecord::Base
  has_many :choice_collection_communities
  has_many :communities, through: :choice_collection_communities
  belongs_to :admin, class_name: "User", foreign_key: "admin_id"
  belongs_to :user
  belongs_to :advocate, class_name: "User", foreign_key: "advocate_user_id"

  # validates_presence_of :personal_message, allow_nil: false, allow_blank: false

  def owner
    self.advocate || self.admin
  end
end

