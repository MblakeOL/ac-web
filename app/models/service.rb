class Service < ActiveRecord::Base
  include Locatable
  validates :address, :city, :zip, presence: true, blank: false

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  def slug_candidates
    [ :name, [:name, :zip], [:name, :zip, :id] ]
  end
end
