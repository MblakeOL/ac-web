require File.join(Rails.root, 'lib/state')

class UserSearch

  def initialize(params)
    [:controller, :action, :authenticity_token, :format].each {|x| params.delete(x) }
    @term = params[:q]
  end

  def admin_search
    User.search(@term, {
      fields: [
        { first_name: :word_start }, 
        {  last_name: :word_start }, 
        {      email: :word_start }, 
        {      phone: :word_start }
      ], 
      where: {},
      load: false,
      operator: :or
    })
  end
end
