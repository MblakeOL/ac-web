class Lead < ActiveRecord::Base
  validates :first_name, :last_name, :email, :phone, presence: true, blank: false
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates :phone, length: { minimum: 14, message: 'is invalid' }

  belongs_to :community

  def save_to_salesforce
    self.community ||= Community.new
    
    datetime = Time.now.in_time_zone("Eastern Time (US & Canada)").strftime("%m/%d/%y  %I:%M %p")

    uri = URI.parse("https://#{SF_CONFIG['host']}/servlet/servlet.WebToLead?encoding=UTF-8")

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(uri.request_uri)

    request.set_form_data({
       first_name: self.first_name,
        last_name: self.last_name,
            phone: self.phone,
            email: self.email,
          company: "#{self.first_name} #{self.last_name} Family",
              oid: SF_CONFIG['web_to_lead_oid'],
             city: self.community.city,
            state: self.community.state_code,
      lead_source: 'Email - Property Page',

      '00N70000003Np3z' => self.comments,      # description
      '00N70000003FXxs' => datetime,           # EST time
      '00N70000003GcpD' => '0',                # ATC attempts
      '00N70000003FlG4' => 'Unknown',          # care type needed
      '00N70000003NpDz' => self.community.name # lead source community

      # 'CF00N70000003G8HF_lkid' => '0017000000vwWpv',
      # 'CF00N70000003G8HF' => 'A.G. Rhodes Home, Inc - Cobb',
      # '00N70000003FSmJ' => 'Waiting WebToLead Contact Decision Maker',
    })

    response = http.request(request)
    if response.code != "200"
      raise "Non 200 response from SalesForce (#{response.code})! #{response.body}"
    end
    return response
  rescue => ex
    Rails.logger.error("Failed to submit lead to SalesForce with id: #{self.id}!!!")
    Rails.logger.error(ex.message)
    Rails.logger.error(ex.backtrace.join("\n"))
  end
end
