class CommunityPdf < ActiveRecord::Base
  has_attached_file :pdf
  do_not_validate_attachment_file_type :pdf

  belongs_to :community
end
