require File.join(Rails.root, 'lib', 'state')

class Community < ActiveRecord::Base

  SEARCHABLE_CARE_TYPES = ['Assisted Living', 'Independent Living', 'Memory Care', 'Personal Care'] # TODO: add these back at some point: 'Continuing Care', 'Residential Care'
  PUBLIC_CARE_TYPES = ['Assisted Living', 'Independent Living', 'Memory Care', 'Respite Care']

  include Locatable
  validates :address, :city, :zip, presence: true, blank: false
  validates_numericality_of :ranking, greater_than_or_equal_to: 0, less_than_or_equal_to: 5

  attr_accessor :unselected_tags

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  def slug_candidates
    [ :name, [:name, :zip], [:name, :zip, :id] ]
  end

  def generate_slug_preview
    set_slug
  end
  
  belongs_to :choice_collections
  has_many :choice_collection_communities
  has_many :attachments, class_name: "CommunityAttachment"
  has_many :floorplans
  has_many :deposit_fees
  has_many :community_tags
  has_many :tags, through: :community_tags
  has_many :community_pdfs
  has_many :community_glossary_terms
  has_many :glossary_terms, through: :community_glossary_terms
  has_one  :pet_policy
  has_one  :community_dining
  has_many :favorites
  has_many :other_services
  has_many :leads
  has_many :notes

  validates :name, blank: false, presence: true

  validate :name_uniqueness

  def name_uniqueness
    existing = self.class.where(name: name).
      joins(:location).
      where('locations.city' => self.city).
      where('locations.state_code' => self.state_code).first
    if existing && existing.id != self.id
      self.errors[:name] = 'is not unique in this city and state'
    end
  end

  before_save :generate_slug_preview
  after_save :maybe_save_location
  searchkick locations: ["coordinates"], word_start: [:name, :city, :zip], autocomplete: [:city_state, :zip, :name]
  
  def maybe_save_location
    if should_geocode?
      location.save 
      self.reindex
    end
  end

  # def should_index?
  #   valid? && !(lat.nil? && lng.nil?)
  # end

  def search_data
   data = {
                         id: self.id,
                       name: self.name, 
                        lat: self.lat, 
                        lng: self.lng, 
                       city: self.city,
                      state: self.state,
                    address: self.address,
                        zip: self.five_digit_zip,
                       slug: self.slug,
                coordinates: self.coordinates,
                 city_state: self.city_state,
                 hero_image: self.hero_image_index,
                 care_types: self.care_types_index,
                  tag_names: self.tag_names_index,
                    tag_ids: self.tag_ids_index,
                starting_at: self.starting_at_index,
                   bedrooms: self.bedrooms_index,
                     prices: self.prices_index,
            tracking_number: self.tracking_number,
                      units: self.number_of_units.to_i,
                  published: (self.is_live || self.is_snapshot || false),
                    ranking: self.ranking,
               availability: self.availability,
                   specials: self.specials,
    availability_updated_at: self.availability_updated_at,
        specials_updated_at: self.specials_updated_at,
            contract_status: self.contract_status,
            internal_number: self.internal_number,
                      email: self.email,
                 fax_number: self.fax_number,
         management_company: self.management_company,
                    website: self.website,
           invoicing_rating: self.invoicing_rating,
                  va_bridge: self.va_bridge,
    }
    data.merge!(starting_at_by_type)
    data
  end



  # SLOW: don't use _index methods on the frontend, only when indexing

  def tag_names_index
    name_tags = self.tags.
      select(:name).
      map {|x| (x.name || '') }

    if pets = self.pet_policy
      name_tags << 'cats allowed' if pets.cats_allowed
      name_tags << 'dogs allowed' if pets.dogs_allowed
    end

    union = name_tags + self.care_types_index(all: true) + self.bedrooms_index
    union.map(&:downcase).reject(&:blank?).uniq
  end

  def starting_at_by_type
    fps = self.floorplans.order(:starting_at).all.group_by(&:care_type_id)
    fps.stringify_keys!
    res = {}
    fps.keys.each do |fp|
      if !fp.blank?
        care_type = Tag.find(fps[fp].first.care_type_id)
      else
        care_type = Tag.where(:name=>"Independent Living", :kind=>"care type").first
      end
      if care_type
        ct_name = care_type.name.downcase.gsub(" ", "_")
      else
        next
      end
      res["#{ct_name}_starting_at"] = fps[fp].first.starting_at
    end
    return res
  end

  def tag_ids_index
    tag_ids = self.tags.pluck(:id)

    if pets = self.pet_policy
      tag_ids << Tag.fake_id('cats allowed') if pets.cats_allowed
      tag_ids << Tag.fake_id('dogs allowed') if pets.dogs_allowed
    end

    union = tag_ids + self.care_types_index(all: true).map {|x| Tag.fake_id(x) } + self.bedrooms_index.map {|x| Tag.fake_id(x) }
    union.map {|x| x.is_a?(String) ? x.downcase : x }.reject(&:blank?).uniq
  end

  def prices_index
    self.floorplans.
      select(:starting_at).
      map {|x| x.starting_at.to_i }.
      reject {|x| x == 0 }.
      sort.uniq
  end

  def bedrooms_index
    # existing types we have so far:
    # ["Private", "Companion", nil, "2 Bedroom", "Studio", "1 Bedroom", "Cottage"]

    retval = self.floorplans.
      map do |fp| 
        kind = (fp.kind || '').downcase 
        if kind.blank?
          kind = case fp.beds
            when -1 then 'shared'
            when 0 then 'studio'
            when 1 then '1 bedroom'
            when 2 then '2 bedroom'
            when 3 then '3 bedroom'
            else nil
          end
        end
        kind
      end.
      reject(&:blank?).uniq

    retval = ['studio'] if retval.blank?
    retval
  end

  def care_types_index(opts={})

    care_type_tags = self.tags.
      where(kind: 'care type').
      where('name is not null').
      order(:name).map(&:name).uniq || []

    unless opts[:all] == true
      care_type_tags.reject! {|x| !PUBLIC_CARE_TYPES.include?(x) }
    end
    # puts care_type_tags
    care_type_tags
  end

  def starting_at_index
    self.floorplans.
      order(starting_at: :asc).
      where("starting_at != '0'").
      where("starting_at is not null").
      map {|x| x.starting_at.try(:to_i) }.
      sort.
      first
  end

  def hero_image_index
    self.attachments.
      where(kind: :hero).
      first.
      try(:medium_path)
  end

  def tags_for(kind)
    @tags ||= self.tags.sort_by(&:name).group_by(&:kind)
    (@tags[kind] || []).uniq
  end

  def images
    # all but hero, since already in the index
    @images ||= self.attachments.
      where('kind = ?', :photo).
      order(:position).
      map &:medium_path
  end

  def self.internal_search(params)
    query    = params[:q]
    page     = params[:page]  || 1
    per_page = params[:limit] || 20

    self.search(query, {
      fields: [{ name: :word_start }, { city: :word_start }, { state: :word_start }, { zip: :word_start }],
      operator: :or,
      page: page,
      per_page: per_page
    })
  end

  def self.autocomplete(params)
    query    = params[:q]
    per_page = (params[:limit] || 3).to_i
    
    # either zip or city_state
    kind     = (params[:kind]  || :city_state).to_s

    query_hash = {
      fields: [
        { city:  :word_start },
        { state: :word_start },
        { zip:   :word_start },
      ],
      where: {},
      per_page: 0,
      facets: { kind => { limit: per_page / 2 } },
      smart_facets: true,
      operator: :or
    }

    query_hash[:where] = { published: true } if params[:show_unpublished].blank?

    facets = self.search(query, query_hash).facets

    locations = (facets[kind]['terms']).map {|x| { term: x['term'], kind: kind } }

    query_hash = {
      fields: [ { name: :word_start } ],
      per_page: 0,
      where: {},
      facets: { name: { limit: per_page / 2 } },
      smart_facets: true
    }

    query_hash[:where] = { published: true } if params[:show_unpublished].blank?

    # name search
    facets = self.search(query, query_hash).facets

    name_terms = facets['name']['terms'].map {|x| { term: x['term'], kind: :name } }
    return locations + name_terms
  end

  def validate?
    (is_live || is_snapshot)
  end
end