class GlossaryTerm < ActiveRecord::Base
  validates :name, :description, :kind, presence: true
  validates_uniqueness_of :name

  has_many :community_glossary_terms
  has_many :communities, through: :community_glossary_terms
end