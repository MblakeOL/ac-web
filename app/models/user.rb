class User < ActiveRecord::Base
  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
  validates_presence_of :phone
  validates_presence_of :first_name
  validates_presence_of :last_name

  searchkick word_start: [:first_name, :last_name, :phone, :email]

  enum role: [:user, :admin, :advocate]

  attr_accessor :send_invitation_email

  has_many :managed_collections, class_name: "ChoiceCollection", foreign_key: "admin_id"
  has_many :choice_collections
  has_many :favorites
  has_many :notes
  has_many :searches
  
  include Headshot

  def search_data
    self.as_json(only: [:email, :role, :first_name, :last_name, :email, :phone])
  end

  def set_password_reset_token
    raw_token, hashed_token = Devise.token_generator.generate(self.class, :reset_password_token)

    self.password = SecureRandom.hex
    self.reset_password_sent_at = Time.now.utc
    self.reset_password_token = hashed_token
    self.save(validate: false)

    return raw_token
  end

  def has_previously_logged_in?
    self.sign_in_count.to_i > 0
  end

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.save!
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def name_or_email 
    if first_name && last_name
      "#{first_name} #{last_name}" 
    else
      email
    end
  end

  def internal?
    self[:role] > 0
  end

  def self.create_user_from_lead(first_name, last_name, email, phone)
    @raw_token, hashed_token = Devise.token_generator.generate(User, :reset_password_token)
    @user = User.create({
                first_name: first_name,
                 last_name: last_name,
                     email: email,
                     phone: phone,
                  password: SecureRandom.hex,
    reset_password_sent_at: Time.now.utc,
      reset_password_token: hashed_token,
                  external: true,
                       })
  end
end
