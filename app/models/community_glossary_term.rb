class CommunityGlossaryTerm < ActiveRecord::Base
  belongs_to :community 
  belongs_to :glossary_term
end
