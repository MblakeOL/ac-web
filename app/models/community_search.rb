require File.join(Rails.root, 'lib/state')

class CommunitySearch

  # DEFAULT_LOCATION = [33.749, -84.388] # Atlanta
  DEFAULT_LOCATION = [33.9873, -84.3506] # Sandy Springs

  FUZZY_FIELDS = [{ city: :word_start }, { state: :word_start }, { name: :word_start }, { city_state: :word_start }]

  def initialize(params)
    [:controller, :action, :authenticity_token, :format].each {|x| params.delete(x) }

    @query      = params[:q]
    @page       = params[:page]  || 1
    @per_page   = params[:limit] || 100
    @sort       = params[:sort]  || 'ranking'
    @within     = '50mi'
    @term       = params[:term].blank? ? '*' : params[:term]
    @kind       = params[:kind]
    @box        = params[:box]
    @center     = params[:center]
    @ids        = params[:ids]
    @prices     = params[:price]
    @filters    = {}
    @location   = nil
    @lat, @lng  = nil, nil
    @comm_types = (params[:community_type] || {}).keys
    @tags       = (params[:tags] || {}).keys
    @beds       = (params[:beds] || {}).keys
    @city       = params[:city]
    @state_code = State::MAP.invert[params[:state].try(:downcase)]

    @show_unpublished = false
    @show_unpublished = true if params[:internal_search] || params[:show_unpublished]

    @internal_search = params[:internal_search] || {}  

    @is_map_search = false
  end

  def main_search(opts={})
    check_for_zip
    find_location
    determine_lat_lng
    determine_price

    results = opts[:map_search] ? map_search : default_search

    log_counts(results)
    return results
  end

  def drilldown_search
    @per_page = @internal_search[:limit] || 100
    @sort = @internal_search[:sort]

    determine_price

    @internal_search = @internal_search.reject {|k,v| k.include?("qp_") }
    tag_ids_and = @internal_search.select {|k,v| v == "1" && !k.include?('_or') }.compact.keys
    tag_ids_or  = @internal_search.select {|k,v| v == "1" && k.include?('_or') }.compact.keys.map {|x| x.sub('_or', '') }

    facets = { tag_ids: { where: { tag_ids: { all: tag_ids_and, in: (tag_ids_or+tag_ids_and).uniq } } }  }
    facets = [:tag_ids] if tag_ids_and.blank?

    where = {}
    where.merge!(prices: @price_min..@price_max) unless @prices.nil?
    where.merge!(tag_ids: { all: tag_ids_and })  unless tag_ids_and.blank?

    @term = @internal_search[:term].blank? ? '*' : @internal_search[:term]
    @kind = @internal_search[:kind] || 'name'

    check_for_zip
    find_location

    # handle geocoded address
    if !@internal_search[:lat].blank? && !@internal_search[:lng].blank?
      @lat, @lng = @internal_search[:lat], @internal_search[:lng]  
      where.merge!(coordinates: { near: [@lat, @lng], within: "#{@internal_search[:radius]}mi" })
    elsif @location
      @term = '*'
      @lat, @lng = @location.lat, @location.lng
      where.merge!(coordinates: { near: [@lat, @lng], within: "#{@internal_search[:radius]}mi" })
    end

    @lat, @lng = *DEFAULT_LOCATION unless @lat && @lng

    search_query = { 
      facets: facets,
      where: where,
      smart_facets: true,
      load: false, 
      per_page: @per_page,
      page: @page
    }

    # boost OR tags
    if tag_ids_or.blank?
      search_query[:order] = handle_admin_order
    else
      search_query[:boost_where] = { tag_ids: { value: tag_ids_or, factor: 1000 } }  
    end

    puts "\nSEARCH QUERY DEBUG:"
    p [@term, search_query]

    results = Community.search(@term, search_query)

    # extract admin searchable tags from returned facets
    tag_hash = Tag.all.group_by {|x| x.id.to_s }
    raw_facets = results.facets['tag_ids']['terms']
    returned_facets = {}

    beds = results.results.map(&:bedrooms).flatten.uniq.map {|x| Tag.fake_id(x) }
    pets = ['cats allowed', 'dogs allowed'].map {|x| Tag.fake_id(x) }

    raw_facets.each do |f|
      if rows = tag_hash[f['term'].to_s]
        returned_facets[rows.first.kind] ||= []
        f['admin_searchable'] = rows.first.admin_searchable
        f['name'] = rows.first.name
        f['group'] = rows.first.group
        returned_facets[rows.first.kind] << f
      elsif beds.include?(f['term'])
        returned_facets['bedrooms'] ||= []
        f['admin_searchable'] = true
        f['name'] = Tag.fake_id_to_s(f['term'])
        returned_facets['bedrooms'] << f
      elsif pets.include?(f['term'])
        returned_facets['pets'] ||= []
        f['admin_searchable'] = true
        f['name'] = Tag.fake_id_to_s(f['term'])
        returned_facets['pets'] << f
      end
    end

    return { facets: returned_facets, results: results.results, total: results.total_count }
  end

protected

  def map_search
    @is_map_search = true

    Community.search('*', {
      where: handle_where,
      order: handle_order,
      per_page: @per_page,
      page: 1,
      load: false
    })
  end

  def default_search
    @lat, @lng = *DEFAULT_LOCATION unless @lat && @lng
    @term = '*' if @location && @term.blank?

    search_query = {
      fields: FUZZY_FIELDS,
      where: handle_where,
      order: handle_order,
      per_page: @per_page / 2,
      page: @page, load: false
    }

    search_query[:boost_where] = { city: @city } if @city
    search_query[:boost_where] = {  zip: @zip  } if @zip

    puts "\nSEARCH QUERY DEBUG:"
    p [@term, search_query]

    Community.search(@term, search_query)
  end

  def handle_where
    where = {
      # wider search for initial page load or fuzzy query
      coordinates: determine_coordinates,
      id: ids_to_ignore
    }

    where.merge!(published: true) unless @show_unpublished

    where.merge!(care_types: @comm_types)        unless @comm_types.blank? || @comm_types.length == Community::SEARCHABLE_CARE_TYPES.length - 1
    where.merge!(tag_names: { all: @tags })      unless @tags.blank?
    where.merge!(bedrooms: @beds)                unless @beds.blank? || @beds.length == 6
    where.merge!(prices: @price_min..@price_max) unless @prices.nil?

    where
  end

  def handle_order
    # always go by distance alone for map searches
    if @is_map_search
      return { _geo_distance: { coordinates: "#{@lat},#{@lng}", order: 'asc' } }
    end

    if @sort == 'price_low_to_high'
      { starting_at: :asc }
    elsif @sort == 'price_high_to_low'
      { starting_at: :desc }
    elsif @sort == 'num_units_high_to_low'
      { units: :desc }
    else
      { _score: :desc, ranking: :desc, _geo_distance: { coordinates: "#{@lat},#{@lng}", order: 'asc' } }
    end
  end

  def handle_admin_order
    ord = case @sort

    when 'al_lth'
      { _score: :desc, assisted_living_starting_at: :asc }
    when 'al_htl'
      { _score: :desc, assisted_living_starting_at: :desc }
    when 'ind_lth'
      { _score: :desc, starting_at: :asc }
    when 'ind_htl'
      { _score: :desc, starting_at: :desc }
    when 'mc_lth'
      { _score: :desc, memory_care_starting_at: :asc }
    when 'mc_htl'
      { _score: :desc, memory_care_starting_at: :desc }
    when 'specials_updated'
      { _score: :desc, specials_updated_at: :desc }
    when 'num_units'
      { _score: :desc, units: :desc }
    else
      # default to distance & ranking search
      handle_order
    end

    ord
  end

  def check_for_zip
    # is this a zip we don't have communities for?
    @kind = 'zip' if @kind.blank? && @term.match(/\d{5}/) 
    if @city && @city.match(/\d{5}/)
      @kind = 'zip' 
      @term = @city
      @zip = @city
      @city = nil
    end
  end

  def find_location 
    if @kind == 'city_state'
      @location = find_city_state
      @term = nil
    elsif @kind == 'zip'
      @location = find_zip
      @term = nil
      @within = "5mi"
    elsif @kind.blank? and @state_code and @city
      @location = find_city_state
    end

    # allow individual locations to have a search radius
    @within = "#{@location.radius}mi" if @location.try(:radius)

    @location
  end

  def find_city_state
    @city, @state_code = @term.split(', ') unless @city or @state_code
    base_q = { city: @city , state_code: @state_code }

    loc   = Location.where(base_q.merge(zip: '', locatable_id: nil)).first
    loc ||= Location.where(base_q.merge(locatable_id: nil)).first
    loc ||= Location.where(base_q).first

    @city = loc.city if loc.try(:city) # use proper capitalization on city name

    loc
  end

  def find_zip
    loc   = Location.where(zip: @term, locatable_id: nil).first
    loc ||= Location.where(zip: @term).first
    loc
  end

  def determine_lat_lng
    if @center
      @lat, @lng = @center.split(",").map(&:to_f)
    elsif @location
      @lat, @lng = @location.lat, @location.lng
    end
  end

  def determine_coordinates
    if @box
      bb = @box.split(',').map(&:to_f)
      return { top_left: [bb[2], bb[1]], bottom_right: [bb[0], bb[3]], near: [@lat, @lng] }
    else
      within = @within

      # broaden search radius for fuzzy search, or exact name
      if @kind == 'name' 
        within = '5000mi'
      # tighten things up on initial landing
      # elsif @sort == 'ranking'
      #   within = '10mi'
      end

      # use a specific radius if set
      within = @location.radius if @location.try(:radius)

      return { near: [@lat, @lng], within: within }
    end
  end

  def determine_price
    return if @prices.nil?

    default_min = 0
    default_max = 100_000

    @price_min = (@prices[:min] || default_min).to_i
    @price_max = (@prices[:max] || default_max).to_i

    # ignore price if default
    @prices = nil if @price_min == default_min && @price_max == default_max
  end

  def ids_to_ignore
    { not: (@ids || []).split(',') }
  end

  def log_counts(results)
    Rails.logger.info("#{results.length} results found (of #{results.total_count})")
  end
end
