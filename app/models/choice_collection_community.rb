class ChoiceCollectionCommunity < ActiveRecord::Base
  belongs_to :community
  belongs_to :choice_collection
  has_many :notes

  # for accepting from simpleform
  attr_accessor :note
  
  def self.find_for_detail_page(current_user, community_id)
    return [] unless current_user
    self.
      joins(:choice_collection).
      where('choice_collections.user_id' => current_user.id).
      where(community_id: community_id).
      all.reject {|x| x.notes.blank? }
  end

  def self.find_for_index(current_user)
    return [] unless current_user
    self.
      joins(:choice_collection).
      where('choice_collections.user_id' => current_user.id).
      all.reject {|x| x.notes.blank? }
  end
end
