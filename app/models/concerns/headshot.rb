module Headshot
  extend ActiveSupport::Concern

  OpenURI::Buffer.send :remove_const, 'StringMax' if OpenURI::Buffer.const_defined?('StringMax')
  OpenURI::Buffer.const_set 'StringMax', 0

  included do
    attr_accessor :headshot, :headshot_tmp_file, :headshot_uri
    before_save :process_headshot
  end

  def process_headshot
    return true if self.headshot_uri.blank? && self.headshot_tmp_file.blank?
    generate_headshot_identifier
    create_tmp_headshot_dir
    get_headshot_file
    resize_headshot
    compress_headshot
    move_headshot_file
  end

  def create_tmp_headshot_dir
    FileUtils.mkdir_p(File.join(Rails.root, 'public/system/users', self.id.to_s, self.headshot_file_name))
  end

  def generate_headshot_identifier
    self.headshot_file_name = SecureRandom.hex
  end

  def get_headshot_file
    if self.headshot_uri =~ URI::regexp
      self.headshot_tmp_file = open(self.headshot_uri)
    end
  end

  def headshot_path
    return nil unless self.headshot_file_name
    File.join('/system/users', self.id.to_s, self.headshot_file_name, 'headshot.jpg')
  end

  def resize_headshot
    `convert #{headshot_tmp_file.path} -resize 400x #{headshot_tmp_file.path}.jpg`  
  end

  def compress_headshot
    `mogrify -compress JPEG -quality 70 #{headshot_tmp_file.path}.jpg`
  end

  def move_headshot_file
    FileUtils.mv("#{headshot_tmp_file.path}.jpg", File.join(Rails.root, 'public/system/users', self.id.to_s, self.headshot_file_name, 'headshot.jpg'))
  end
end