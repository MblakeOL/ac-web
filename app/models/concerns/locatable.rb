module Locatable
  extend ActiveSupport::Concern

  included do 
    has_one :location, as: :locatable

    attr_accessor :should_geocode

    delegate :city, :city=, :address, :address=, :state, :state=, :state_code, :state_code=, \
      :zip, :zip=, :lat, :lat=, :lng, :lng=, to: :location

    after_initialize do
      # make sure we never delegate to nil
      self.location ||= Location.new
    end

    geocoded_by :full_address, latitude: :lat, longitude: :lng
    # TODO: make this happen automatically after development
    before_validation :geocode, if: lambda{ |c| c.should_geocode? } #c.address_changed? || c.city_changed? || c.state_changed? || c.zip_changed? }

    validates :state_code, length: { maximum: 2, message: 'is not valid. Please use a two letter state code.' }, presence: true

    before_validation :normalize_state_names
  end

  def should_geocode?
    loc.lat.nil? || loc.lng.nil? || @should_geocode
  end

  def loc
    @loc ||= self.location
  end

  def normalize_state_names
    loc.state_code = (loc.state_code || '').upcase
    loc.state = State::MAP[loc.state_code]
    loc.save if loc.state && self.id
  end

  def five_digit_zip
    loc.zip.try(:sub, /-.*$/, '')
  end

  def full_address
    [loc.address, loc.city, loc.state, loc.zip].compact.join(', ')
  end

  def city_state
    [loc.city, loc.state_code].reject(&:blank?).join(', ')
  end

  def coordinates
    [loc.lat, loc.lng]
  end
end