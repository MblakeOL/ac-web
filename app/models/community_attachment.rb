class CommunityAttachment < ActiveRecord::Base
  belongs_to :community

  OpenURI::Buffer.send :remove_const, 'StringMax' if OpenURI::Buffer.const_defined?('StringMax')
  OpenURI::Buffer.const_set 'StringMax', 0
  attr_accessor :tmp_file, :uri, :skip_process

  STYLES = {
    medium: {
      geometry: "1024x768", 
      default_url: "/images/nophoto.png"
    },        
    small: {
      geometry: "640x480",
      default_url: "/images/nophoto.png"
    }
  }

  before_save :process

  def process
    return true if skip_process
    generate_identifier
    create_tmp_dir
    get_file
    resize
    watermark
    compress
    move_files
  end

  def create_tmp_dir
    FileUtils.mkdir_p(File.join(Rails.root, 'public/system/community_attachments', self.community_id, self.attachment_file_name))
  end

  def generate_identifier
    self.attachment_file_name = SecureRandom.hex
  end

  def get_file
    if self.uri =~ URI::regexp
      self.tmp_file = open(self.uri)
    end
  end

  def medium_path
    File.join('/system/community_attachments', self.community_id, self.attachment_file_name, 'medium.jpg')
  end

  def small_path
    File.join('/system/community_attachments', self.community_id, self.attachment_file_name, 'small.jpg')
  end

  def resize
    STYLES.each do |size, attrs|
      `convert #{tmp_file.path} -resize #{attrs[:geometry]}^ -gravity center -crop #{attrs[:geometry]}+0+0 +repage #{tmp_file.path}_#{size}.jpg`  
    end
  end

  def watermark
    STYLES.each do |size, attrs|
      `composite -gravity southeast #{Rails.root}/public/images/ac_watermark_#{size}.png  #{tmp_file.path}_#{size}.jpg #{tmp_file.path}_#{size}.jpg`
    end
  end

  def compress
    STYLES.each do |size, attrs|
      `mogrify -compress JPEG -quality 70 #{tmp_file.path}_#{size}.jpg`
    end
  end

  def move_files
    STYLES.each do |size, attrs|
      FileUtils.mv("#{tmp_file.path}_#{size}.jpg", File.join(Rails.root, 'public/system/community_attachments', self.community_id, self.attachment_file_name, "#{size}.jpg"))
    end
  end
end