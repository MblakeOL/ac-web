class Tag < ActiveRecord::Base
  has_many :community_tags
  has_many :community, through: :community_tags

  validates :name, uniqueness: { scope: [:group, :kind], message: 'is not unique for this category and/or subcategory' }, presence: true, blank: false
  validates_presence_of :kind

  attr_accessor :new_kind

  # This fake id stuff is just a hack for ElasticSearch.
  # We want to search for some items as if they were tags (that aren't really), because it 
  # simplifies the search code. We assign a negative int value to add to the array of tag ids (must all be ints).
  # This code will generate the same integer for a given string, that is not likely to collide.
  def self.fake_id(string)
    @@fake_ids ||= {}
    return @@fake_ids[string] if @@fake_ids.has_key?(string)

    # start with an MD5 hash of the string, and keep only the numbers
    int = -1*(Digest::MD5.hexdigest(string).tr('^0-9', '').to_i)
    # it can't be too big or no search results come back (ES limitation of some kind).
    int /= 10 while int < -100_000
    # store it for lookup when searching later
    @@fake_ids[string] = int
  end

  def self.fake_id_to_s(int)
    @@fake_ids.invert[int]
  end

  def self.autocomplete(params)
    kind = params[:kind]
    query = params[:q]

    return [] unless query

    base = Tag.where("name like ?", "%#{query}%").select("distinct name, kind").limit(20)

    if kind
      return base.where(kind: kind).all
    else
      return base.all
    end
  end

  def self.find_for_index(params={})
    Tag.group('kind, name, tags.group')
  end
end
