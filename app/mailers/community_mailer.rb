class CommunityMailer < ActionMailer::Base
  default to: "leads@assistedchoice.com"

  append_view_path("#{Rails.root}/app/views/mailers")

  def availability(lead, first_request_url)
    @lead = lead
    @first_request_url = first_request_url

    mail(from: "admin@assistedchoice.com", subject: "[AssistedChoice] Community Availability - #{@lead.community.name}")
  end
end