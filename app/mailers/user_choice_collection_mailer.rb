class UserChoiceCollectionMailer < ActionMailer::Base

  append_view_path("#{Rails.root}/app/views/mailers")
  
  def choice_collection(choice_collection, cc_link)
    @cc = choice_collection
    @cc_link = cc_link

    if @cc.advocate == @cc.admin
      mail(from: @cc.owner.email, to: @cc.user.email, subject: @cc.user_email_subject)
    else
      mail(from: @cc.owner.email, to: @cc.user.email, bcc: @cc.advocate.email, subject: @cc.user_email_subject)
    end
  end
end