class UserAddedNoteMailer < ActionMailer::Base
  default to: "admin@assistedchoice.com"

  append_view_path("#{Rails.root}/app/views/mailers")

  def note_alert(choice_collection, community)
    @choice_collection = choice_collection
    @community = community

    mail(from: "admin@assistedchoice.com", to: "#{@choice_collection.admin.email};#{@choice_collection.advocate.try(:email)}", subject: "User has added note to choice collection")
  end
end