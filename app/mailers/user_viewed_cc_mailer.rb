class UserViewedCcMailer < ActionMailer::Base
  append_view_path("#{Rails.root}/app/views/mailers")

  def viewed(choice_collection)
    @choice_collection = choice_collection
    to = "#{@choice_collection.advocate.email}"
    to += ";#{@choice_collection.admin.email}" unless (@choice_collection.admin_id == @choice_collection.advocate_user_id)

    email_hash = { from: "admin@assistedchoice.com", to: to, subject: "#{@choice_collection.user.name_or_email} has viewed their choice collection." }

    # john michael asked to be copied on all these
    always_copy = 'jm@assistedchoice.com'
    unless email_hash[:to].include?(always_copy)
      email_hash[:bcc] = always_copy
    end

    mail(email_hash)
  end
end