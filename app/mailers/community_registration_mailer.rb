class CommunityRegistrationMailer < ActionMailer::Base

  append_view_path("#{Rails.root}/app/views/mailers")
  
  def registration(choice_collection, community)
    @cc = choice_collection
    @community = community

    @modified_body = @cc.comm_email_body.gsub("{{community name will go here}}", @community.name)
    subject = @cc.comm_email_subject.gsub("{{community name will go here}}", @community.name)

    mail(from: @cc.owner.email, to: @community.email, bcc: 'matchmyemail@assistedchoice.com', subject: subject)
  end
end