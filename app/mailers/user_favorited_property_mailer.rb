class UserFavoritedPropertyMailer < ActionMailer::Base

  append_view_path("#{Rails.root}/app/views/mailers")
  
  def favorite_alert(choice_collection, community, user)
    @choice_collection = choice_collection
    @community = community
    @user = user
    if @choice_collection
      to = "#{@choice_collection.admin.email};#{@choice_collection.advocate.try(:email)}"
    else
      to = "info@assistedchoice.com"
    end
    mail(from: "admin@assistedchoice.com", to: to, subject: "User has added a community to their favorites")
  end
end