class UserCreatedMailer < ActionMailer::Base

  add_template_helper ApplicationHelper

  append_view_path("#{Rails.root}/app/views/mailers")

  default to: "admin@assistedchoice.com"

  def notifier(user)
    @user = user
    mail(from: "admin@assistedchoice.com", subject: "A user has signed up for the site")
  end
end