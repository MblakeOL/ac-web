class InviteUserMailer < ActionMailer::Base
  default to: "admin@assistedchoice.com"

  append_view_path("#{Rails.root}/app/views/mailers")

  def invitation(user, raw_token)
    @user = user
    @raw_token = raw_token

    mail(from: "admin@assistedchoice.com", to: @user.email, subject: "You account has been created at AssistedChoice.com")
  end
end