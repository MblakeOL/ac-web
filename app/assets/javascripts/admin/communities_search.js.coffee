class CommunitiesSearch
  constructor: ->
    $ => 
      @queryDom()
      @bindEvents()
      @refresh()
      @facetLookup = null

  queryDom: ->
    @pageEle = $('#page')
    @resultsHolderEle = $('#results')
    @formEle = $('#admin_search_form')
    @countEle = $('#total_count')
    @searchInputEle = $('#internal_search_term')
    @searchResultsEle = $('#internal_search_autocomplete')
    @searchKindInputEle = $('#internal_search_kind')
    @addressInputEle = $('#internal_search_address')
    @geoLocateBtnEle = $('#geolocate')
    @addressLatEle = $('#internal_search_lat')
    @addressLngEle = $('#internal_search_lng')
    @searchOptionsEle = $('#search_options')
    @bodyEle = $('body')
    @spinnerEle = $('#results_box .spinner_box')
    @searchBoxEle = $('#search_box')
    @optionGroupTitleEle = $('.options_group_box.collapsable .title')
    @optionGroupSubtitleEle = $('.options_group.collapsable .subtitle')
    @detailsHolderEle = $('#details_holder')
    @quickPicksGroupInputEle = $('#quick_picks_group input')

  bindEvents: ->
    @bindAutocomplete()
    @bindInputChange()
    @bindSuggestionClicked()
    @bindAddressGeocode()
    @bindShowHideOptions()
    @handleSectionShowHide()
    @handleQuickPicksGroup()

    @countEle.click => @hideSearch()

    @formEle.bind('submit', (e) => e.preventDefault())

    @searchInputEle.on 'blur', =>
      setTimeout((=>@searchResultsEle.html('')), 300)

    @searchInputEle.on 'keydown', (e) =>
      @searchResultsEle.html('') if e.keyCode == 13

  refresh: ->
    @formEle.find('input:first').trigger('change')

  handleSectionShowHide: ->
    @optionGroupTitleEle.click ->
      parent = $(this).parent()
      parent.toggleClass('active_options')
      if parent.hasClass('active_options')
        parent.find('.options_group').slideDown('fast')
      else
        parent.find('.options_group').slideUp('fast')

    @optionGroupSubtitleEle.click ->
      parent = $(this).next('.options_sub_group')
      parent.toggleClass('active_options')
      if parent.hasClass('active_options')
        parent.slideDown('fast')
        $(this).addClass('active_options')
      else
        parent.slideUp('fast')
        $(this).removeClass('active_options')

  handleQuickPicksGroup: ->
    # rebind a special change handler for quick picks
    @quickPicksGroupInputEle.unbind('change').bind 'change', ->
      box = $(this)
      forInput = $("##{box.attr('data-for')}")
      if box.is(':checked')
        forInput.attr('checked', 'checked').trigger('change')
      else
        forInput.attr('checked', null).trigger('change')

  hideSearch: ->
    @searchOptionsEle.slideUp 'fast'

    if @searchBoxEle.hasClass('active')
      @countEle.text(@countEle.text().replace('View', 'Viewing'))

    @searchBoxEle.removeClass('active')
    # @searchResultsEle.hide()
    # @searchInputEle.removeClass('active_field')

  bindShowHideOptions: ->
    clickOutFunction = (e) =>
      targ = $(e.target)
      if targ.attr('data-kind') or targ.parents('#search_box').length
        # rebind this function (once) if click was within box
        @bodyEle.one('click', clickOutFunction)
      else
        @hideSearch()

    @searchInputEle.on 'click', =>
      @searchOptionsEle.slideDown('fast')
      @searchResultsEle.show()
      @searchInputEle.addClass('active_field')
      @searchBoxEle.addClass('active')
      @countEle.text(@countEle.text().replace('Viewing', 'View'))
      @bodyEle.one 'click', clickOutFunction  

  bindAddressGeocode: ->
    @previousAddressMarker = null

    failures = 0
    geoFunc = =>
      if @addressInputEle.val() != ''
        $.ajax
          url: @addressInputEle.attr('data-url')
          data:
            address: @addressInputEle.val()
          success: (res) =>
            # remove old marker if exists
            @previousAddressMarker.setMap(null) if @previousAddressMarker

            @addressInputEle.val(res.address)
            @addressLatEle.val(res.lat)
            @addressLngEle.val(res.lng)

            if AC?.map?.map
              gpoint = new google.maps.LatLng(res.lat, res.lng)
              @previousAddressMarker = new google.maps.Marker
                position: gpoint
                map: AC.map.map
            
            @refresh()
            failures = 0
          error: ->
            console.log "Geocoder is sleepy right now"
            failures += 1
            geoFunc() if failures < 3

    @geoLocateBtnEle.click geoFunc

    @addressInputEle.on 'keyup', (e) =>
      if @addressInputEle.val() == ''
        @addressLatEle.val('')
        @addressLngEle.val('')
      geoFunc() if e.keyCode == 13

  bindSuggestionClicked: ->
    self = @
    @searchResultsEle.delegate 'a', 'click', ->
      link = $(this)
      self.searchInputEle.val(link.text())
      self.searchResultsEle.html('')
      kind = link.attr('data-kind')
      self.searchKindInputEle.val(kind)
      self.refresh()
      self.hideSearch() if kind == 'name'

  bindAutocomplete: ->
    @searchUrl = @searchInputEle.attr('data-url')
    @searchInputEle.on 'input', => @autocomplete()

    @handleAutocompleteKeyDown()

  handleAutocompleteKeyDown: ->
    @searchInputEle.keydown (e) =>
      @searchKindInputEle.val('') unless e.keyCode == 13
      @searchBoxEle.addClass('active')
      @searchOptionsEle.slideDown('fast')
      if e.keyCode == 13 # enter key pressed
        e.preventDefault()
        # see if there was an active selection & apply it
        activeEntry = @searchResultsEle.find('a.active:first')
        if activeEntry.length == 1
          @searchInputEle.val(activeEntry.attr('data-suggestion'))
          kind = activeEntry.attr('data-kind')
          @searchKindInputEle.val(kind)
          @hideSearch() if kind == 'name'

        @refresh()

      # up or down
      if e.keyCode == 38 or e.keyCode == 40
        e.preventDefault()
        activeEntry = @searchResultsEle.find('a.active:first')
        @searchResultsEle.find('a').removeClass('active')

        if e.keyCode == 38 # up
          if activeEntry.length == 0
            @searchResultsEle.find('a:last').addClass('active')
          else
            toAdd = activeEntry.parent().prev().find('a')
            if toAdd.length == 0
              @searchResultsEle.find('a:last').addClass('active')
            else
              toAdd.addClass('active')

        else if e.keyCode == 40 # down
          if activeEntry.length == 0
            @searchResultsEle.find('a:first').addClass('active')
          else
            toAdd = activeEntry.parent().next().find('a')
            if toAdd.length == 0
              @searchResultsEle.find('a:first').addClass('active')
            else
              toAdd.addClass('active')

  autocomplete: ->
    val = @searchInputEle.val()

    @searchKindInputEle.val('')

    # clear old results
    return @searchResultsEle.html('') if val.length == 0

    # only search if two or more characters present
    return if val.length < 2
    
    url = "#{@searchUrl}?limit=12&q=#{val}&show_unpublished=true"

    # perform a zip search if input is a number
    url += "&kind=zip" if /^\d+$/.test(val)

    $.ajax
      url: url
      success: (res) =>
        @searchResultsEle.html(res)

  commaSeparateNumber: (val) ->
    while (/(\d+)(\d{3})/.test(val.toString()))
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2')
    val

  bindShowLinks: ->
    self = @
    @showLinksEle = $('.admin_show')
    
    @showLinksEle.click (e) ->
      link = $(this)
      e.preventDefault()

      AC.map.maybePanToMarker(link.attr('data-id'))

      $.ajax
        url: link.attr('href')
        success: (res) ->
          self.detailsHolderEle.html(res)
          detailsBoxEle = $('#details_box')
          detailsBoxEle.addClass('active')
          $(window).trigger('resize')
          new TimeHandler
          new Floorplans
          self.bindAddNote()
          closeLink = $('#close_admin_details')
          closeLink.click -> 
            detailsBoxEle.removeClass('active')
            if activeMarker = AC?.map?.activeMarker?.ourid
              AC?.map?.deactivateMarker(activeMarker, true)

  bindAddNote: ->
    $(".choice_collection_note").click ->
      @popupEle = $('#edit_cc_note_popup')
      resultEle = $(this)
      self = @
      @popupEle.find("form").bind "ajax:success", () ->
        $("#advocate_notes").append("<span class='highlighted js_time'>#{new Date().getTime() / 1000;}</span><p>#{$('#edit_cc_note_popup').find('#choice_collection_community_note').val()}</p>")
        new TimeHandler
        self.popupEle.hide()
      @popupEle.popPop
        afterPop: ->
          $('#choice_collection_community_id').val(resultEle.attr('id'))
          $("#edit_cc_note_popup").find('#choice_collection_community_note').val(resultEle.attr("data-note"))

  bindInputChange: ->
    formSubmitFunc = (e) =>
      formData = @formEle.serialize()

      checkedMustHave = []
      checkedNiceToHave = []
      checkedLookup = {}

      if @facetLookup # first landing if this isn't defined yet
        # keep track of what was selected so we can tell them if it matched
        checkedEntries = @searchBoxEle.find('input:checked').serializeArray()
        for entry in checkedEntries
          id = entry.name.match(/\[(.*)\]/)[1]
          if id.match("_or")
            id = id.replace('_or', '')
            checkedNiceToHave.push(Number(id))
          else
            checkedMustHave.push(Number(id))

      @spinnerEle.show()
      $.ajax
        url: @formEle.attr('action')
        data: formData
        method: 'POST'
        success: (res) =>

          unless @facetLookup # build it
            @facetLookup = {}
            for kind, deets of res.facets
              for _, inner of deets
                # if @lastFacets[kind][id]['group']
                # console.log "#{kind}", inner
                displayString = "#{kind} - "
                displayString += "#{inner.group} - " if inner.group
                displayString += "#{inner.name}"
                @facetLookup[inner.term] = displayString

          text = "#{res.total} Result#{if res.total == 1 then '' else 's'}"

          if @searchBoxEle.hasClass('active')
            text = "View #{text}" 
          else
            text = "Viewing #{text}"

          @countEle.text(text)
          @countEle.text('No results') if res.total == 0

          @countEle.addClass('flashy')
          setTimeout((=> @countEle.removeClass('flashy')), 200)
          
          # set all counts to zero before updating
          @formEle.find('.admin_count').each -> $(this).text($(this).text().replace(/\(\d+\)/, "(0)"))

          for kind, facets of res.facets
            for f in facets
              ele = $("#internal_search_#{f.term}")
              old = ele.parents('.box').find('.admin_count')
              old.text(old.text().replace(/\(\d+\)/, "(#{f.count})"))

              ele2 = $("#qp_internal_search_#{f.term}")
              old2 = ele2.parents('.box').find('.admin_count')
              old2.text(old2.text().replace(/\(\d+\)/, "(#{f.count})"))

          toInsert = []  
          tmpl = $.templates('#result_tmpl')
          for c in res.results
            c.url = "/#{c.state}/#{c.city.toLowerCase().replace(' ', '-')}/#{c.slug}"
            c.care_types = c.care_types.join(' | ')
            c.address2 = "#{c.city_state} #{c.zip}"
            c.phone = c.tracking_number or "(404) 334-5500"
            c.starting_at = "Starting at $#{@commaSeparateNumber(c.starting_at or 0)}<span class='small_text'>/mo.</span>"
            if c.assisted_living_starting_at
              c.assisted_living_starting_at = "Starting at $#{@commaSeparateNumber(c.assisted_living_starting_at or 0)}<span class='small_text'>/mo.</span>"
            else
              c.assisted_living_starting_at = "N/A"
            if c.memory_care_starting_at
              c.memory_care_starting_at = "Starting at $#{@commaSeparateNumber(c.memory_care_starting_at or 0)}<span class='small_text'>/mo.</span>"
            else
              c.memory_care_starting_at = "N/A"
            if c.independent_living_starting_at
              c.independent_living_starting_at = "Starting at $#{@commaSeparateNumber(c.independent_living_starting_at or 0)}<span class='small_text'>/mo.</span>"
            else
              c.independent_living_starting_at = "N/A"
            if c.specials_updated_at
              d2 = new Date(c.specials_updated_at)
              c.specials_updated_at = "Updated #{d2.getMonth()+1}/#{d2.getDate()}/#{d2.getFullYear()}"
            else
              c.specials_updated_at = "Updated Never"

            c.mustHaveTags = []
            for id in checkedMustHave
              if @facetLookup[id]
                c.mustHaveTags.push("<li>#{@facetLookup[id]}</li>")

            c.niceToHaveTags = []
            for id in checkedNiceToHave
              if c.tag_ids.indexOf(id) != -1 && @facetLookup[id]
                c.niceToHaveTags.push("<li>#{@facetLookup[id]}</li>")

            c.mustHaveTags = c.mustHaveTags.join('')
            c.niceToHaveTags = c.niceToHaveTags.join('')

            toInsert.push tmpl.render(c)

          showAllLink = ''
          if res.total > res.results.length
            showAllLink = "<div id='show_all_link_box'><a id='show_all_link' href='javascript:void(0);'>Showing the first 100 listings (show all #{res.total})</a></div>"

          @resultsHolderEle.html(toInsert.join('')+showAllLink)
          @bindShowLinks()

          $('#show_all_link').click =>
            $('#internal_search_limit').val(5000)
            formSubmitFunc()
            $('#internal_search_limit').val(100)

          if AC.map
            AC.map.clearMarkers()
            AC.map.loadFromAdminList()
          @spinnerEle.hide()

          new ChoiceCollections

    @formEle.find('input, select').on 'change', formSubmitFunc
    @formEle.find('.auto_complete_in').unbind('change')

new CommunitiesSearch

# needed for map compat
window.AC ?= {}
window.AC.list = new CommunitiesList