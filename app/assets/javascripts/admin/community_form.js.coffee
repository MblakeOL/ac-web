class CommunityForm
  constructor: ->
    $ =>
      @queryDom()
      @bindEvents()

  queryDom: ->
    @formEle = $('form.edit_community, form.new_community')
    @tagPickerRightEle = @formEle.find('.tag_picker_r')
    @tagPickerLeftEle = @formEle.find('.tag_picker_l')
    @selectedTagSelectEle = $('.selected_tags')

  bindEvents: ->
    @handleTagPicking()
    @handleSubmit()

  handleTagPicking: ->
    self = @

    @tagPickerRightEle.click ->
      clicked = $(this)
      kind = clicked.attr('data-for')
      unselectedBox = self.formEle.find(".#{kind}_unselected")
      selectedBox = self.formEle.find(".#{kind}_selected")
      selectedOptions = unselectedBox.find('option:selected')
      selectedBox.append(selectedOptions)

    @tagPickerLeftEle.click ->
      clicked = $(this)
      kind = clicked.attr('data-for')
      unselectedBox = self.formEle.find(".#{kind}_unselected")
      selectedBox = self.formEle.find(".#{kind}_selected")
      selectedOptions = selectedBox.find('option:selected')
      unselectedBox.append(selectedOptions)

  handleSubmit: ->
    @formEle.submit (e) => @selectedTagSelectEle.find('option').attr('selected', true)

new CommunityForm