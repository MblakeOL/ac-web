$ =>
  $(".numeric").bind keydown: (e) ->
    if e.shiftKey is true
      return true  if e.which is 9
      return false
    return false  if e.which > 57
    return false  if e.which is 32
    true