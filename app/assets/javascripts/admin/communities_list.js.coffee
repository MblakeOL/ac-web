class window.CommunitiesList
  constructor: ->
    @currentlyScrolling = false
    $ =>
      @queryDom()
      @bindEvents()
      if $("#map_holder").length > 0
        $("body").addClass("admin_results")
  queryDom: ->
    @resultsEle = $('#results')

  bindEvents: ->
    @handleHover()

  handleHover: ->
    @resultsEle.on 'mouseenter mouseleave', '.result', (e) ->
      jQEle = $(this)
      if e.type == "mouseenter"
        jQEle.addClass('map_active')
        AC.map.activateMarker(jQEle.attr('id'))
      else
        jQEle.removeClass('map_active')
        AC.map.deactivateMarker(jQEle.attr('id'))

  scrollTo: (id) ->
    @resultsEle.scrollTo "##{id}", 
      duration: (if @currentlyScrolling then 1 else 200), 
      onAfter: => @currentlyScrolling = false
    @currentlyScrolling = true
