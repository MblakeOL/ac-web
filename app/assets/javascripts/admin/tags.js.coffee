class Tags
  constructor: ->
    $ =>
      @queryDom()
      @bindEvents()

  queryDom: ->
    @tagInputEle = $('#tags_autocomplete')
    @tagOutputEle = $('#tags_autocomplete_results')
    @searchUrl = @tagInputEle.data('url')

    @tagKindEle = $('#tag_kind')
    @tagFormEle = $('form.edit_tag')
    @tagUrl = @tagFormEle.attr('action')
    @tagSubcatsHolderEle = $('#tag_subcats_holder')

    @checkboxesEle = $('#tag_checkbox_holder').find('input[type=checkbox]')

  bindEvents: ->
    @bindCheckboxes()
    @bindTagKindChanged()

  bindTagKindChanged: ->
    @tagKindEle.on 'change', =>
      $.ajax 
        url: "#{@tagUrl}/category_changed"
        data:
          category: @tagKindEle.val()
        success: (res) =>
          @tagSubcatsHolderEle.html(res)

  bindCheckboxes: ->
    @checkboxesEle.on 'click', ->
      box = $(this)
      url = box.attr('value')
      searchable = box.is(':checked')

      $.ajax
        url: url
        method: 'PATCH'
        data: 
          admin_searchable: searchable
          authenticity_token: $('input[name=authenticity_token]').val()


  liveSearch: ->
    return @tagOutputEle.html('') if @tagInputEle.val().length == 0

    $.ajax
      url: "#{@searchUrl}?q=#{@tagInputEle.val()}"
      success: (res) =>
        @tagOutputEle.html(res)

new Tags