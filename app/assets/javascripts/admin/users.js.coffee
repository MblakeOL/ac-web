class Users
  constructor: ->
    $ =>
      @queryDom()
      return unless @userSearchEle.length

      @bindEvents()

  queryDom: ->
    @userSearchEle = $('#user_search')
    @userResultsEle = $('#user_results')
    @inputField = @userSearchEle.find('.admin_search_field')

  bindEvents: ->
    @inputField.on 'input', =>
      $.ajax
        url: @inputField.attr('data-url')
        data:
          q: @inputField.val()
        success: (results) =>
          tmpl = ['<ul>']
          for user in results
            tmpl.push("<li><a href='/admin/users/#{user.id}'><span class='break_box'>#{user.first_name || ''} #{user.last_name || ''} </span><span class='break_box user_email'>#{user.email || ''}</span><span class='break_box user_phone'>#{user.phone || ''}</span></a></li>")
          @userResultsEle.html(tmpl.join(''))

new Users