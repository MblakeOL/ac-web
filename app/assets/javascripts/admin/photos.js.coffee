class Photos
  constructor: ->
    @queryDom()
    @bindEvents()

  queryDom: ->
    @imageList = $('#image_list')
    @leftArrows = @imageList.find('.icon_arrow_west')
    @rightArrows = @imageList.find('.icon_arrow_east')
    @deleteButtons = @imageList.find('.icon_delete')
    @uploadInput = $('#community_attachment_ids')
    @communityForm = $(".edit_community")

  bindEvents: ->
    # auto-submit upload form when files selected
    if window.location.href.indexOf("attachments") > -1
      @uploadInput.change => 
        conf = confirm("Existing files of the selected type will be overwritten.")
        if conf == true
          @communityForm.submit()
    else
      @uploadInput.change => @communityForm.submit()

    reload = -> window.location.reload()

    @deleteButtons.click ->
      if confirm('Are You Sure?')
        li = $(this).parents('li')
        $.ajax
          url: li.attr('data-url')
          method: 'DELETE'
          success: reload

    @leftArrows.click ->
      li = $(this).parents('li')
      $.ajax
        url: li.attr('data-url')
        method: 'PATCH'
        data:
          index: li.attr('data-index')
          direction: 'left'
        success: reload

    @rightArrows.click ->
      li = $(this).parents('li')
      $.ajax
        url: li.attr('data-url')
        method: 'PATCH'
        data:
          index: li.attr('data-index')
          direction: 'right'
        success: reload

$ =>
  new Photos