$ ->
  $(".remove_community").bind "ajax:complete", () ->
    window.location.reload()

  $(".choice_collection_note").click ->
    @popupEle = $('#edit_cc_note_popup')
    resultEle = $(this)
    @popupEle.find("form").bind "ajax:success", () ->
      window.location.reload()
    @popupEle.popPop
      afterPop: ->
        $('#choice_collection_community_id').val(resultEle.attr('id'))
        $("#edit_cc_note_popup").find('#choice_collection_community_note').val(resultEle.attr("data-note"))

  $(".edit_cc_name").click ->
    @popupEle = $('#edit_cc_name')
    resultEle = $(this)
    @popupEle.find("form").bind "ajax:success", () ->
      window.location.reload();
    @popupEle.popPop
      afterPop: ->
        $('#choice_collection_id').val(resultEle.attr('id'))
        $('#choice_collection_name').val(resultEle.attr("data-name"))