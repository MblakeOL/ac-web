class window.ChoiceCollections
  constructor: ->
    $ =>
      @queryDom()
      @showLinks()
      @bindEvents()
      @bindFormOnce()

  queryDom: ->
    @choiceCollectionDataEle = $('#choice_collection_data')
    @iconStarEle = $('.icon_star')

    return unless @choiceCollectionDataEle.length

    @communityData = JSON.parse(@choiceCollectionDataEle.attr('data-community-data'))
    @communityIDs = []

    for obj in @communityData
      @communityIDs.push(obj.community_id)

    @id = @choiceCollectionDataEle.attr('data-id')
    # @iconNotesEle = $('.icon_notes')
    @popupEle = $('#add_to_cc_popup')

  bindEvents: ->
    self = @

    @iconStarEle.click ->
      favIconEle = $(this)
      resultEle = $("##{favIconEle.attr('data-id')}")
      if resultEle.hasClass('choice')
        if confirm("Remove this community from the choice collection?")
          resultEle.removeClass('choice')
          $.ajax
            method: 'DELETE'
            url: '/admin/choice_collection_communities/0'
            data: { community_id: resultEle.attr('id') }
      else
        # add to cc
        self.popupEle.popPop
          afterPop: ->
            self.popupEle.find('h2').text(resultEle.find('.title .admin_show').text())
            $('#choice_collection_community_community_id').val(resultEle.attr('id'))
            $('#choice_collection_community_note').val('')
        
  showLinks: ->
    return unless @id
    # @iconNotesEle.show()
    @iconStarEle.show()
    for cid in @communityIDs
      $("##{cid}").addClass('choice')
      for data in @communityData
        if data.community_id == cid and data.notes
          notes = $("#notes_for_#{cid}")
          notes.parent().find('.display_block').html(data.notes)
          notes.find('strong').text("#{data.name}'s Notes: ")
          notes.parent().parent().show()

  bindFormOnce: ->
    return unless @popupEle and @popupEle.length
    @popupEle.on 'submit', (e) =>
      e.preventDefault()

      $.ajax
        method: 'POST'
        url: '/admin/choice_collection_communities'
        data: @popupEle.find('form').serialize()
        success: =>
          comm_id = $('#choice_collection_community_community_id').val()
          $("##{comm_id}").addClass('choice')

          notes = $("#notes_for_#{comm_id}")
          notes.parent().find('.display_block').html($('#choice_collection_community_note').val())
          notes.find('strong').text("Your Note: ")
          notes.parent().parent().show()

          @popupEle.find('.close').click()


    ChoiceCollections.prototype.bindFormOnce = ->
