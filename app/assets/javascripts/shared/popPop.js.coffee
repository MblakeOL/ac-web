###
To use this library, your markup should look like this:

<div id="your_unique_id" class="popup_box">
  <div class="popup">
    <a href="javascript:void(0)" class="close">close</a>
    <p>Popup content goes here</p>
  </div>
</div>

* You need the classes popup_box, popup, and close.

Then the link that triggers this popup should look like this: 

<a class="my_popup_link" data-for="your_unique_id" href="javascript:void(0)">Click to Show Popup</a>

Calling $('.my_popup_link').popPop() will open the popup window with the ID that matches data-for="..."

The only option right now is an 'afterPop' callback which can be provided like so:

$('.my_popup_link').popPop({ afterPop: function(){ do stuff after the popup opens here } })

###

class PopPopManager
  constructor: ->
    $ =>
      @windowSize = "Not Detected"
      @queryDom()
      @windowPos = @winEle.scrollTop()
      @setWindowDimensions()
      @determineWindowSize()
      @scrollToTop()
      @handleWindowResize()

  queryDom: ->
    @popupInstances = $('.popup')
    @winEle = $(window)
    @bodyEle = $('body')
    @pageBoxEle = $('#page_box')

  adjustPopup: (popupEle) ->
    windowPos = @winEle.scrollTop()
    popupEle.show()

    content = popupEle.find('.popup')
    @popupWidth = content.outerWidth()
    @popupHeight = content.outerHeight()

    if @windowSize == 'small_screen'
      @pageBoxEle.hide()
      @scrollToTop()
    else if @windowSize == 'big_screen'
      @pageBoxEle.show()
      content.css
        left: (@windowWidth-@popupWidth)/2+"px"
        top: (@windowHeight-@popupHeight)/2+"px"

  determineWindowSize: ->
    if @windowWidth > 767
      @windowSize = 'big_screen'
    else if @windowWidth < 768
      @windowSize = 'small_screen'
      # take over the whole screen with popup on small screens
      @pageBoxEle.hide() if @popupIsVisible()

    @bodyEle.removeClass('big_screen small_screen').addClass(@windowSize)

  popupIsVisible: ->
    @popupInstances.is(':visible')

  scrollToTop: ->
    @bodyEle.scrollTop(1)

  setWindowDimensions: ->
    @windowWidth = @winEle.width()
    @windowHeight = @winEle.outerHeight()

  handleWindowResize: ->
    @winEle.resize =>
      @setWindowDimensions()
      @determineWindowSize()
      @adjustPopup(@popupInstances.filter(':visible').parent()) if @popupIsVisible()

mgr = new PopPopManager

# create a jquery plugin
jQuery.fn.extend
  popPop: (config) ->
    # track any new popups that may have been added to the dom
    mgr.popupInstances = $('.popup')

    return @each ->
      link = $(this)
      popupToShowEle = $('#'+link.data('for'))

      popupToShowEle.css
        minHeight: mgr.windowHeight

      # just in case a popup is already open, close it
      $('.popup_box').hide() if mgr.popupIsVisible()
      # adjust dimensions and show popup
      mgr.adjustPopup(popupToShowEle)
      # call a custom callback if provided
      config.afterPop.call(link) if config?.afterPop
      # trigger a window reflow in case content size changed
      mgr.winEle.trigger('resize')

      # handle closing the popup
      closeButton = popupToShowEle.find('.close')

      # handle escape key pressed when popup open
      escPressedFunc = (e) ->
        closeButton.trigger('click') if e.keyCode == 27

      mgr.winEle.keyup escPressedFunc

      closeButton.click ->
        history.go(-1) if window.location.pathname == '/popup'        
        $(this).parents('.popup_box').hide()
        mgr.pageBoxEle.show()
        mgr.bodyEle.scrollTop(mgr.windowPos)
        mgr.winEle.unbind 'keyup', escPressedFunc
        mgr.winEle.trigger('resize')