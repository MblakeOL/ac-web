class UI
  constructor: ->
    $ =>
      @queryDom()
      @bindWindowResize()
      @bindHoverActive(@navMenuEle, '#nav')
      @bindHoverActive(@sortBoxEle, '#sort_box')
      @bindNavClick('.nav_level2')
      @bindSortClick('.sort_level2')
      # trigger a resize immediately
      @winEle.trigger('resize')

      @errorPopupEle.popPop() if @errorPopupEle
      @personalMessagePopup = $("#personal_message")
      @personalMessagePopup.popPop()

      if @personalMessagePopup.length == 0
        $('#welcome_splash').popPop()
        
  queryDom: ->
    @winEle = $(window)
    @docEle = $(document)
    @windowHeight = @winEle.width()
    @windowWidth = @winEle.outerHeight()
    @errorPopupEle = $('#flash_popup')

    @pageBoxEle = $('#page_box')
    @resultsEle = $('#results')
    @mapCanvasEle = $('#map_canvas')
    @pageEle = $('#page')

    @slideUpEle = $('.slide_up').addClass("slide_up_closed").css
      minHeight: @windowHeight

    @navMenuEle = $('#nav')
    @sortBoxEle = $('#sort_box')
    @iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent )
      
    @menuCaptured = false
  # add 'active' classes on hover or click depending on platform
  bindHoverActive: (element, parent) ->
    element.find('a').on 'mouseenter touchstart click mouseleave', (e) ->
      if e.type == 'mouseenter' and !$(e.target).closest("li").hasClass("nav_level1")
        $(parent).addClass 'active'
        $(this).addClass 'active'
        @menuCaptured = false
        @hoverState = true
      else if e.type == 'mouseenter' and $(e.target).closest("li").hasClass("nav_level1")
        $(parent).addClass 'active'
        $(this).addClass 'active'
        @menuCaptured = true
      else if !($(parent).hasClass 'active') and $(e.target).closest("li").hasClass("nav_level1") and (e.type == 'click')
        $(parent).addClass 'active'
        $(this).addClass 'active'
        @menuCaptured = false
      else if (e.type == 'click') and ($(parent).hasClass 'active') and $(e.target).closest("li").hasClass("nav_level1") and !@menuCaptured == true
        $(parent).removeClass 'active'
        $(this).removeClass 'active'
        @menuCaptured = false
      else if e.type == 'mouseleave'
        $(parent).removeClass 'active'
        $(this).removeClass 'active'
        @menuCaptured = false
      else if e.type == 'to'
      else
        @menuCaptured = false
      #No other events fired for browser, set timeout in case
      setTimeout((=>@menuCaptured = false), 300)

    $('.sort_level2').find('a').click ->
      $(parent).removeClass('active')

  bindNavClick: (parent) ->
    if (!("ontouchstart" in document.documentElement)) and @iOS
      $(parent).find('a').on 'mouseenter', (e) ->
        window.location.href = $(this).attr("href")

  bindSortClick: (parent) ->
    if (!("ontouchstart" in document.documentElement)) and @iOS
      $(parent).find('a').on 'mouseenter', (e) ->
        setTimeout((=>$(e.target).click()), 300)


  bindWindowResize: ->
    @winEle.resize =>
      @windowWidth = @winEle.width()
      @windowHeight = @winEle.outerHeight() 

      @pageBoxEle.css
        width: @windowWidth
        height: @windowHeight

      headerHeight = $('#header').height()

      @resultsEle.css
        height: @windowHeight-($('#search_box').height()+headerHeight+$('#results_controls').outerHeight()+1)

      $('#search_options').css
        height: @windowHeight-($('#search_box').height()+headerHeight) 

      $('#details_box').css
        height: @windowHeight-(headerHeight)

      $('#details').css
        height: @windowHeight-(headerHeight+$('#details_top_holder').height())

      $('#results_holder').css
        top: $('#search_field_holder').outerHeight()+$('#header').outerHeight()+$('#results_controls').outerHeight()-24
        # $('#search_intro_box').outerHeight()+ ADD THIS AFTER TABS ARE BACK IN THE SEARCH BOX and change -24 to -16 for some reason

      AC.swiper.resizeFix(true) if AC?.swiper
      
      @mapCanvasEle.css
        height: @windowHeight-headerHeight

new UI