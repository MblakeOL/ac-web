class window.Floorplans
  constructor: ->
    @queryDom()
    @bindEvents()

  queryDom: ->
    @floorplanPopupLinksEle = $('a.fp_photo_pop')
    @floorplanPopupEle = $('#fp_photo_popup')
    @floorplanPhotoHolderEle = $('#fp_photo_holder')

  bindEvents: ->
    @bindFloorplanPopup()

  bindFloorplanPopup: ->
    self = @
    @floorplanPopupLinksEle.click ->
      link = $(this)
      return unless link.attr('data-photo')

      # get target image from the popup
      targetImg = self.floorplanPopupEle.find('img')
      tmpImg = $("<img src='#{link.attr('data-photo')}' />")

      tmpImg.on 'load', ->
        # after image loads, we know the width & height
        targetImg.css({ width: tmpImg.width(), height: tmpImg.height() })
        targetImg.replaceWith(tmpImg)

        # set title
        self.floorplanPopupEle.find('h2').text(link.attr('data-title') or 'Floorplan')

        # make history entry (so back button works)
        link.popPop
          afterPop: ->
            if AC.history
              AC.history.register({ id: 'popup', domID: link.attr('id') }, '/popup')

      # trigger the image load
      self.floorplanPhotoHolderEle.append(tmpImg)

$ =>
  new Floorplans() if $('#fp_photo_popup')