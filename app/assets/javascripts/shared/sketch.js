$( document ).ready(function() {
  // sketchy stuff here    

  $( "#map_switch" ).click(function() {
    $( "#results_holder" ).toggle();
    $( "#map_canvas" ).toggle();
  });


  // tabs sketch 
  $( "#tab_housing" ).click(function() {
    $( "#intro_at_home" ).hide();
    $( "#intro_resources" ).hide();
    $( "#intro_housing" ).show();

    $( "#search_at_home" ).hide();
    $( "#search_resources" ).hide();
    $( "#search_housing" ).show();

    $( "#tab_at_home" ).removeClass("active");
    $( "#tab_resources" ).removeClass("active");
    $( "#tab_housing" ).addClass("active");
  });

  $( "#tab_at_home" ).click(function() { 
    $( "#intro_housing" ).hide();
    $( "#intro_resources" ).hide();
    $( "#intro_at_home" ).show();

    $( "#search_housing" ).hide();
    $( "#search_resources" ).hide();
    $( "#search_at_home" ).show();

    
    $( "#tab_resources" ).removeClass("active");
    $( "#tab_housing" ).removeClass("active");
    $( "#tab_at_home" ).addClass("active");
  });

  $( "#tab_resources" ).click(function() { 
    $( "#intro_housing" ).hide();
    $( "#intro_at_home" ).hide();
    $( "#intro_resources" ).show();

    $( "#search_housing" ).hide();
    $( "#search_at_home" ).hide();
    $( "#search_resources" ).show();


    $( "#tab_housing" ).removeClass("active");
    $( "#tab_at_home" ).removeClass("active");
    $( "#tab_resources" ).addClass("active");
  });


});




