class Map
  constructor: ->
    @markers = []
    @markerLookup = {}
    @listLookup = {}
    @firstLoadDone = false
    @activeMarker = null
    @idsToLoad = []

    $ =>
      @queryDom()
      @loadMap() if @mapContainerEle.length      

  queryDom: ->
    @winEle = $(window)
    @mapContainerEle = $('#map_canvas')
    @resultTotalEle = $('#result_total_count')
    @searchFormEle = $('#search_form')

    b = $('body')
    @isFavorites = b.hasClass('favorites') or b.hasClass('choice_collections')
    @isLander = b.hasClass('city_lander')
    @isSavedSearch = @getUrlParameter("s")

  bindEvents: ->
    @map = new google.maps.Map @mapContainerEle[0], 
      zoom: 11,
      center: new google.maps.LatLng(33.9873, -84.3506),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      tilt: 45

    # force a reflow
    @winEle.trigger('resize')

    @bindPan()

    if @isFavorites or @isLander or @isSavedSearch
      @loadFromList() 
      @firstLoadDone = true

  getUrlParameter: (sParam) -> 
    sPageURL = window.location.search.substring(1)
    sURLVariables = sPageURL.split('&')
    for variable in sURLVariables
      sParameterName = variable.split('=')
      if sParameterName[0] == sParam
        return sParameterName[1]

  loadFromAdminList: ->
    self = @
    points = []
    AC.list.resultsEle.find('.result').each ->
      result = $(this)
      data =
        lat: result.attr('data-lat')
        lng: result.attr('data-lng')
        id:  result.attr('id')
      points.push(data)
    @drawPoints(results: points)

  loadFromList: ->
    self = @
    points = []
    AC.list.resultsEle.find('.result').each ->
      data = JSON.parse($(this).attr('data-attributes'))
      points.push(data)
    @drawPoints(results: points)

  loadMap: ->
    # expose this function globally for google
    window.mapLoaded = => @bindEvents()

    script = document.createElement("script")
    script.type = "text/javascript"
    script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&async=2&sensor=false&callback=mapLoaded"
    document.body.appendChild(script)

  bindPan: ->
    return if @isFavorites
    google.maps.event.addListener @map, 'projection_changed', => @loadData() # detect initial load
    google.maps.event.addListener @map, 'idle',    => @loadData() if @firstLoadDone
    google.maps.event.addListener @map, 'dragend', => @firstLoadDone = true # drag to disengage a search by name

  loadData: =>
    c = @map.getCenter()

    return unless AC.search

    if AC.search.lastSerialized == null 
      if @getUrlParameter('s')
        AC.search.lastSerialized = @searchFormEle.serializeArray()

      if @getUrlParameter('term') && !window.location.search.match('fill')
        AC.search.lastSerialized = [{ name: 'term', value: @getUrlParameter('term') }]

    # [].concat, poor man's array clone
    searchData = [].concat(AC.search.lastSerialized)

    searchData.push(name: 'ids',    value: Object.getOwnPropertyNames(@markerLookup))
    searchData.push(name: 'box',    value: @boundingBox())
    searchData.push(name: 'center', value: "#{c.lat()},#{c.lng()}")

    $.ajax
      method: 'POST'
      url: @mapContainerEle.data('url')
      dataType: 'json'
      data: searchData
      success: (res) =>
        return if res.length == 0 

        AC.map.drawPoints(res)
        @syncList(res)

        if res.total != 0 && res.length <= res.total && @firstLoadDone
          setTimeout (=>@loadData()), 300
      complete: =>
        setTimeout (=> 
          @loadData() if @firstLoadDone == false
          @firstLoadDone = true
        ), 1000

  syncList: (res) ->
    return if window.location.search.match(/term/)
    # list is empty or close to empty?
    if AC.list.resultsEle.find('.result').length < 6
      i = 0
      for result in res.results
        i += 1
        break if i > 5
        templates = []
        if @listLookup[result._id] is undefined or @listLookup[result._id].length == 0
          tmpl = AC.list.buildCard(result)
          templates.push tmpl[0].outerHTML if tmpl and tmpl[0]
        $('#special_margin').prepend(templates.join())

  clearMarkers: ->
    return unless @markers
    marker.setMap(null) for marker in @markers
    @markers = []
    @markerLookup = {}
    @listLookup = {}

  boundingBox: ->
    bounds = @map.getBounds().toUrlValue()

  searchPerformed: (data, opts) ->
    if @isFavorites
      @isFavorites = false
      @bindPan() # this didn't happen earlier

    @firstLoadDone = false
    @clearMarkers()
    @drawPoints(data, opts)

    # trigger another request if we think there may be more
    if data.results.length > 10
      @firstLoadDone = true
      setTimeout((=> @loadData()), 500)

  activateMarker: (id) ->
    @markerLookup[id]?.setIcon(@activeIcon())

  deactivateMarker: (id, shouldUnset) ->
    if shouldUnset
      @markerLookup[id]?.setIcon(@inactiveIcon())
      @activeMarker = null if @markerLookup[id] == @activeMarker
    else
      @markerLookup[id]?.setIcon(@inactiveIcon()) unless String(@activeMarker?.ourid) == String(id)

  inactiveIcon: ->
    url: "/images/map_pin_inactive.png"
    size: new google.maps.Size(14, 14)
    scaledSize: new google.maps.Size(14, 14)

  activeIcon: ->
    url: "/images/map_pin_active.png"
    size: new google.maps.Size(30, 38)
    scaledSize: new google.maps.Size(30, 38)

  maybePanToMarker: (id) ->
    return setTimeout((=> @maybePanToMarker(id)), 500) if typeof google == "undefined" or typeof google.maps == "undefined" or typeof @map == 'undefined' or typeof @map.getBounds() == 'undefined' or @markers.length == 0

    marker = @markerLookup[id]
    if marker == undefined
      # create pin if it doesn't exit
      data = AC.list.dataForMarker(id)
      marker = @createMarker(data) 
    inBounds = @map.getBounds().contains(@markerLookup[id].getPosition())
    @map.panTo(marker.getPosition()) unless inBounds
    @activeMarker = marker
    @activateMarker(id)

  handleMarkerHover: (marker) ->    
    marker.setIcon(@activeIcon())
    @listLookup[marker.ourid] ?= $("##{marker.ourid}")

    if typeof @listLookup[marker.ourid] is 'undefined' or @listLookup[marker.ourid].length == 0
      # create a list item for this pin
      tmpl = AC.list.buildCard(marker.ourdata)
      targ = AC.list.resultsScrollEle.find('.result:nth-last-child(5)')
      if targ.length > 0
        tmpl.insertAfter(targ)
      else
        AC.list.resultsScrollEle.prepend(tmpl)
      @listLookup[marker.ourid] = $("##{marker.ourid}")

    @listLookup[marker.ourid].addClass('map_active')
    AC.list.scrollTo(marker.ourid)

  deactivateAllMarkers: ->
    for _, marker of @markerLookup
      @deactivateMarker(marker.ourid, true )
    $('.result').removeClass('map_active')

  createMarker: (data) ->
    return setTimeout((=> @createMarker(data)), 500) if typeof google == "undefined" or typeof google.maps == "undefined"

    gpoint = new google.maps.LatLng(data.lat, data.lng)

    marker = new google.maps.Marker
      position: gpoint
      map: @map
      ourid: String(data.id)
      name: data.name
      icon: @inactiveIcon()
      ourdata: data

    @markerLookup[String(data.id)] = marker

    @markers.push marker

    self = @
    google.maps.event.addListener marker, 'mouseover', ->
      # don't open the details on click for ipad
      return if /(iPad|iPhone|iPod)/g.test( navigator.userAgent )

      marker.timeoutID = setTimeout((-> self.handleMarkerHover(marker)), 200)

    google.maps.event.addListener marker, 'click', =>

      @deactivateAllMarkers() if /(iPad|iPhone|iPod)/g.test( navigator.userAgent )

      @handleMarkerHover(marker)
      @deactivateMarker(@activeMarker.ourid, true) if @activeMarker

      # don't open the details on click for ipad
      if /(iPad|iPhone|iPod)/g.test( navigator.userAgent )
        AC.list.closeDetailsFunc() if $('#details_box').hasClass('active')
        return

      @listLookup[marker.ourid].find('.title a').click()
      @activeMarker = marker
      @activeMarker.setIcon(@activeIcon())

    google.maps.event.addListener marker, 'mouseout', =>
      return if /(iPad|iPhone|iPod)/g.test( navigator.userAgent )

      clearInterval(marker.timeoutID) if marker.timeoutID
      marker.setIcon(@inactiveIcon()) unless marker == @activeMarker
      
      if @listLookup[marker.ourid]
        @listLookup[marker.ourid].removeClass('map_active')
        marker.setIcon(@activeIcon()) if @listLookup[marker.ourid].active

    return marker

  drawPoints: (data, opts={}) ->
    return if Object.getOwnPropertyNames(data).length == 0
    return setTimeout((=> @drawPoints(data, opts)), 500) if typeof google == "undefined" or typeof google.maps == "undefined" or typeof google.maps.LatLngBounds == "undefined"

    bounds = new google.maps.LatLngBounds()

    self = @
    $.each data.results, -> 
      return if self.markerLookup[this.id]
      marker = self.createMarker(this)
      bounds.extend(marker.getPosition())

    @map.fitBounds(bounds) unless @firstLoadDone

    # tighten up the view bounds a bit unless told otherwise
    # @map.setZoom(@map.getZoom()+1) unless @firstLoadDone or opts.tightBounds == false

    # don't zoom in too close, usually if there is only one property returned from search
    @map.setZoom(12) if @markers.length == 1 && @map.getZoom() > 16
    @map.setZoom(16) if @markers.length == 2 && @map.getZoom() > 16

    # if not an undefined search or search by community name
    @firstLoadDone = true if opts.freezeMap == false

    google.maps.event.addListenerOnce @map, 'zoom_changed', =>
      # don't zoom in too close, usually if there is only one property returned from search
      @map.setZoom(12) if @markers.length == 1 and @map.getZoom() > 16
      @map.setZoom(16) if @markers.length == 2 and @map.getZoom() > 16

window.AC ?= {}
window.AC.map = new Map