class window.TimeHandler

  constructor: ->
    @months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    $ =>
      @queryDom()
      @localizeTimes()

  queryDom: ->
    @times = $('.js_time')

  localizeTimes: ->
    self = @
    @times.each ->
      thisEle = $(this)
      stamp = thisEle.text()
      if stamp != ''
        stamp = Number(stamp) * 1000
        localTime = self.formatLocal(new Date(stamp))
        thisEle.text(localTime)
        thisEle.removeClass('js_time')

  formatLocal: (date) ->
    hours = date.getHours()
    minutes = date.getMinutes()
    ampm = if hours >= 12 then 'pm' else 'am'
    hours = hours % 12
    hours = if hours then hours else 12
    minutes = if minutes < 10 then '0'+minutes else minutes
    strTime = hours + ':' + minutes + ' ' + ampm
    "#{@months[date.getMonth()]} #{date.getDate()}, #{date.getFullYear()} #{strTime}"

new TimeHandler