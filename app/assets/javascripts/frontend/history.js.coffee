class History
  constructor: ->
    $ =>
      @queryDom()

    @handleBackButton()
    @previousState = null

  queryDom: ->
    @resultsScrollEle = $('#results_scroll')
    @detailsHolderEle = $('#details_holder')
    @pageBoxEle = $('#page_box')

  log: (msg...) ->
    return unless window.console and window.console.log
    console.log(msg)

  register: (state, path) ->
    # don't trigger the same history entry twice
    return if state?.id == @previousState?.id
    return if state?.id == history.state?.id

    return unless window.history.pushState

    @log 'new history entry:', state, path
    window.history.pushState(state, '', path)
    @previousState = state

  handleBackButton: ->
    $(window).on 'popstate', (e) =>
      @previousState ?= { id: 'root' }
      currentState = history.state or { id: 'root' }

      currentStateID = currentState.id

      # treat search the same as root
      currentStateID = 'root' if currentState.id == 'search'

      if @previousState.from == "search"
        @log 'back from search'
        window.location.href = '/'
      else if @previousState.id == 'popup' and currentStateID == 'root'
        @log 'popup closed on main page'
        $('.popup_box').hide()
        @pageBoxEle.show()
      else if @previousState.id.match('details-') and currentStateID == 'root' and window.location.hash == ''
        @log 'detail page closed'
        AC.list.closeDetailsFunc()
      else if @previousState.id.match('details-') and currentStateID == 'root' and window.location.hash.match('search')
        @log 'detail page closed'
        AC.list.closeDetailsFunc()
      else if @previousState.id == 'popup' and currentStateID.match('details-')
        @log 'popup closed on detail page'
        $('.popup_box').hide()
        @pageBoxEle.show()
      else if @previousState.id == 'root' and currentStateID.match('details-')
        @log 'forward button hit to detail page'
        if id = currentStateID.match(/details-(\w+)/)[1]
          $("##{id}").click()
      else if @previousState.id == 'root' and currentStateID == 'root'
        AC.history.register({ id: 'root' }, '/')
        if @detailsHolderEle.find('#details_box').hasClass('active')
          AC.list.closeDetailsFunc()
      else if @previousState.id.match('details-') and currentStateID.match('details-')
        @log 'switch detail page', @previousState, currentState
        if @detailsHolderEle.find('#details_box').hasClass('active')
          AC.list.closeDetailsFunc()
        if id = currentStateID.match(/details-(\w+)/)[1]
          $("##{id}").click()
      else
        @log 'unhandled state:', @previousState.id, currentState

      if currentState.domID and currentStateID == 'popup'
        $("##{currentState.domID}").popPop()

      currentState = { id: 'root', from: 'search' } if currentState.id == 'search'
      @previousState = currentState

window.AC ?= {}
AC.history = new History