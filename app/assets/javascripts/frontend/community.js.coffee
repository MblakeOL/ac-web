class window.Community
  constructor: ->
    window.AC ?= {}
    @queryDom()
    @bindEvents()
    @handleAnchorLinks()
    @bindAddNotes()

  queryDom: ->
    @leftArrowEle = $('.arrow_left')
    @rightArrowEle = $('.arrow_right')
    @swiperContainerEle = $('.swiper-container')

    new Floorplans
    new TimeHandler

    @careChargePopupLinksEle = $('a.care_charge_pop')
    @careChargePopupEle = $('#care_charge_popup')

    @detailsEle = $('#details')
    @detailsTabsEle = $('#details_tabs').find('a')

    @notedLinkEle = $('#details_box .icon_notes')

    @id = $('#details_top').attr('data-id')
    AC?.map?.maybePanToMarker(@id)

  bindEvents: ->
    @bindCarousel()
    @bindCareChargePopup()

  bindCarousel: ->
    # community image slider
    @swiper = new Swiper '.swiper-container',
      loop: true
      grabCursor: true
      calculateHeight: true
      autoResize: true

    AC.swiper = @swiper

    # don't init image slider if none or one present
    if @swiperContainerEle.find('img').length < 4
      # take the src of the first image and replace the slide show with a single image
      if @swiperContainerEle.find('img').length > 0
        @swiperContainerEle.html("<img src='#{@swiperContainerEle.find('img:first').attr('src')}'/>")
        
      @leftArrowEle.remove()
      @rightArrowEle.remove()
      AC.swiper = null
      return

    @leftArrowEle.on 'click', (e) =>
      e.preventDefault()
      @swiper.swipePrev()

    @rightArrowEle.on 'click', (e) =>
      e.preventDefault()
      @swiper.swipeNext()

  bindCareChargePopup: ->
    self = @
    @careChargePopupLinksEle.click ->
      link = $(this)

      # set popup data
      self.careChargePopupEle.find('h2').text(link.attr('data-title') or 'Care Charges')
      self.careChargePopupEle.find('p').text(link.attr('data-description'))

      # make history entry (so back button works)
      link.popPop
        afterPop: ->
          AC.history.register({ id: 'popup', domID: link.attr('id') }, '/popup')

  handleAnchorLinks: ->
    for tab in @detailsTabsEle
      if $($(tab).attr('href')).length == 0
        $(tab).remove()

    self = @
    @detailsTabsEle.click (e) ->
      e.preventDefault()
      self.detailsEle.scrollTo($(this).attr('href'), { duration: 500 })

    @notedLinkEle.click (e) ->
      e.preventDefault()
      self.detailsEle.scrollTo($(this).attr('data-href'), { duration: 500 })


  bindAddNotes: ->
    notePopupEle = $('#add_note_popup')
    notePopupEle.find("form").unbind('ajax:complete').bind "ajax:complete", () ->
      notePopupEle.hide()
      addedBy = "<p class='note_added_by'>Added by <strong><span>#{$('#notes').attr('data-username')}</span></strong> - <span class='js_time small_text note_time'>#{new Date().getTime() / 1000;}</span></p>"
      $("#notes").append("#{addedBy}<p>#{$('#add_note_popup').find('#note_body').val()}</p>")
      $('#notes').find('h4').show()
      $('.add_note_link.solo').hide()
      $('#details').scrollTo('#notes')
      new TimeHandler
      notePopupEle.find("#note_body").val("")

    notePopupEle.find('form').bind 'submit', ->
      notePopupEle.hide()
      
    $(".add_note_link").click ->
      notePopupEle.popPop()
      notePopupEle.find("#note_community_id").val($(this).attr("data-id"))

$ =>
  if $('body').hasClass('communities show')
    new Community
