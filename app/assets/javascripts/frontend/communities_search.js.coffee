class CommunitiesSearch
  constructor: (@list) ->
    @lastSerialized = null
    @url = null
    $ =>
      @queryDom()
      @bindEvents()

  queryDom: ->
    @winEle = $(window)
    @bodyEle = $('body')
    @url = $('#search_field_box').data('url')
    @advancedSearchLinkEle = $('#link_advanced_search')
    @advancedSearchBoxEle = $("#advanced_search_box")
    @searchOptionsEle = $('#search_options')
    @radioButtonsEle = @searchOptionsEle.find('input[type=radio]')
    @radioGroupBoxesEle = @searchOptionsEle.find('.radio_box')
    @resultTotalEle = $('#result_total_count')
    @searchFormEle = $('#search_form')
    @searchTabsEle = $('#search_tabs')
    @sortFieldEle = @searchFormEle.find('input[name=sort]')
    @sortBoxEle = $('#sort_box')
    @sortBoxFirstLinkEle = @sortBoxEle.find('a:first')
    @noResultsEle = $('#no_results_found')
    @autocompleteEle = $('#communities-autocomplete-input')
    @spinnerEle = $('#results_box .spinner_box')

    @saveSearchFormEle = $('#save_search_form')
    @saveSearchEle = $('#user_save_search')

  bindEvents: ->
    @bindAdvancedSearchLink()
    @bindTabsClicked()
    @bindSortChanged()
    @bindSaveSearch()

    # triggered by communities autocomplete
    @winEle.on 'community-search', (_, opts) =>
      AC.history.register({ id: 'search' }, '/')
      $.ajax
        method: 'POST'
        url: @url
        data: @searchFormEle.serialize()
        success: (res) => @searchSuccess(res, opts)

  bindSaveSearch: ->
    self = @
    @saveSearchEle.click =>
      @saveSearchFormEle.find('form').show()
      firstPart = $('#communities-autocomplete-input').val()
      firstPart = "My" if firstPart == ''
      $('#search_name').val("#{firstPart} Search")
      $('#save_search_saved_message').hide()
      @saveSearchFormEle.popPop()

    @saveSearchFormEle.find('form').unbind('submit').bind 'submit', (e) =>
      e.preventDefault()
      searchData = [].concat(@lastSerialized)
      searchData.push(name: 'search_name', value: $('#search_name').val())

      $.ajax
        method: 'POST'
        url: @saveSearchEle.find('a').attr('data-url')
        data: searchData
        success: =>
          @saveSearchFormEle.find('form').hide()
          $('#save_search_saved_message').show()
          # .find('.close').trigger('click').hide()

  bindAdvancedSearchLink: ->
    @advancedSearchLinkEle.click (e) =>
      @advancedSearchBoxEle.slideToggle('fast')
      if @advancedSearchLinkEle.text().toLowerCase().match(/more/)
        @advancedSearchLinkEle.text('Less Options')
      else
        @advancedSearchLinkEle.text('More Options')

  bindTabsClicked: ->
    tabs = @searchTabsEle.find('a')
    tabs.click (e) =>
      @advancedSearchLinkEle.hide()
      if $(this).attr('id') == 'tab_housing'
        @advancedSearchLinkEle.show()
        if @advancedSearchLinkEle.text().toLowerCase().match(/less/)
          @advancedSearchLinkEle.trigger('click')        

  bindSortChanged: ->
    @sortBoxEle.click (e) =>
      targ = $(e.target)
      return if targ.parent().hasClass('sort_level1')
      sortType = targ.attr('data-sort')

      dist = @sortBoxEle.find('a[data-sort=distance]')
      priceLow = @sortBoxEle.find('a[data-sort=price_low_to_high]')
      priceHigh = @sortBoxEle.find('a[data-sort=price_high_to_low]')
      unitsLow = @sortBoxEle.find('a[data-sort=num_units_high_to_low]')

      @sortBoxEle.find('a').show()

      if sortType == "price_low_to_high"
        priceLow.hide()
        @sortBoxFirstLinkEle.text('Price Low')
      else if sortType == "price_high_to_low"
        priceHigh.hide()
        @sortBoxFirstLinkEle.text('Price High')
      else if sortType == "distance"
        dist.hide()
        @sortBoxFirstLinkEle.text('Distance')
      else if sortType == "num_units_high_to_low"
        unitsLow.hide()
        @sortBoxFirstLinkEle.text('# of Units')

      @sortFieldEle.val(sortType)

      @spinnerEle.show()

      # pass { sorted: true } to zoom in a bit and tighten up the map
      @winEle.trigger 'community-search', { sorted: false } 

  searchSuccess: (res, opts={}) =>
    jRes = $(res)
    results = jRes.filter('.result')

    @bodyEle.removeClass('favorites')

    @spinnerEle.hide()

    if results.length == 0
      # did they type something that was already suggested, but ignored it?
      if opts.topSuggestion
        $('#search_kind').val(opts.topSuggestion.kind)
        $('#communities-autocomplete-input').val(opts.topSuggestion.term)
        return @winEle.trigger 'community-search', topSuggestion: null

      @noResultsEle.popPop
        afterPop: =>
          AC.history.register({ id: 'popup', domID: 'no_results_found' }, '/popup')
      return

    @lastSerialized = @searchFormEle.serializeArray()

    AC.list.searchPerformed(jRes)

    @updateTotal()
    @saveSearchEle.show()

    mapData = { results: [] } 
    results.each -> mapData.results.push($(this).data('attributes'))

    AC.map.searchPerformed mapData, 
      tightBounds: (opts.sorted or false)

  updateTotal: ->
    totalFound = Number($('#total_result_count').text())

    lastWord = "Found"
    lastWord = "Saved" if @bodyEle.hasClass('favorites')

    if totalFound == 1
      @resultTotalEle.html("<span>#{totalFound} Community </span> <span class='break_box'>#{lastWord}</span>")
    else
      @resultTotalEle.html("<span>#{totalFound} Communities </span> <span class='break_box'>#{lastWord}</span>")

window.AC ?= {}
window.AC.search = new CommunitiesSearch