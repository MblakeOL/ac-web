$ ->
  if $('#infinite_scrolling').size() > 0
    $("#results").on 'scroll', ->
      more_posts_url = $('.pagination a[rel=next]').attr('href')
      if more_posts_url && $("#results").scrollTop() >  $("#results_scroll").height() - $("#results").height() - 760

          parts = more_posts_url.split("/")
          if parts.length > 3
            more_posts_url = more_posts_url.replace(more_posts_url.substring(more_posts_url.lastIndexOf("/"), more_posts_url.lastIndexOf("?")), "");

          $('.pagination').html('')

          searchData = [].concat(AC.search.lastSerialized)
          idsToIgnore = $('#results').find('.result').map -> $(this).attr('id') 
          # idsToIgnore = (Object.getOwnPropertyNames(AC?.map?.markerLookup) || []).join(',')
          searchData.push(name: 'ids', value: idsToIgnore)

          $.ajax
            type: 'POST'
            data: searchData
            url: more_posts_url
            success: (data) ->
              $('#special_margin').remove()
              $("#results_scroll").append(data)

      if !more_posts_url
        $('.pagination').html("")
    return