class Notes
  constructor: ->
    $ =>
      @queryDom()
      @bindEvents()

  queryDom: ->
    @detailsHolderEle = $('#details_holder')
    @detailsEle = $('#details')

  handleRedirectToNote: ->
    return if window.location.search.indexOf('note=true') == -1
    setTimeout(( =>
      @detailsEle.scrollTo('#advocate_notes', { duration: 500 })
      $('.add_note_link:first').trigger('click')
    ), 1500)
    

  bindEvents: ->
    @handleRedirectToNote()

    detailPageFunc = ->
      if !$("#user-exists").length > 0
        ele = $(this)
        resultEle = ele.parents("#details_top")
        fid = resultEle.attr("data-id")
        $("#user_favorite_login").attr("href","/users/preregistration?note=#{fid}&path=sign_in")
        $("#user_favorite_join").attr("href","/users/preregistration?note=#{fid}&path=sign_up")
        $('#user_not_logged_in').popPop         
          afterPop: ->
            AC.history.register({ id: 'popup', domID: 'user_not_logged_in' }, '/popup')
            $('#user_not_logged_in').find('p:first').text("You must be logged in to add notes.")
            $("#user_favorite_login").click("alert")

    @detailsHolderEle.delegate '.icon_notes', 'click', detailPageFunc    

  
new Notes