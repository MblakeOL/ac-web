class CommunitiesList
  constructor: ->
    $ =>
      @queryDom()
      @bindEvents()
      @bindReadMore()

  queryDom: ->
    @resultsScrollEle = $('#results_scroll')
    @resultsEle = $('#results')
    @detailsBoxEle = $('#details_box')
    @detailsHolderEle = $('#details_holder')
    @searchBoxEle = $('#search_box')
    @winEle = $(window)
    @bodyEle = $('body')
    @pageBoxEle = $('#page_box')

  scrollTo: (id) ->
    @resultsEle.scrollTo "##{id}", 
      duration: (if @currentlyScrolling then 1 else 200), 
      onAfter: => @currentlyScrolling = false
    @currentlyScrolling = true

  closeDetailsFunc: =>
    @detailsBoxEle.removeClass('active')
    if @bodyEle.hasClass('small_screen')
      @resultsEle.show()
      @searchBoxEle.show()
      @winEle.trigger('resize')

    @resultsEle.find('.result').removeClass('map_active')

    if activeMarker = AC?.map?.activeMarker?.ourid
      AC?.map?.deactivateMarker(activeMarker, true)

  bindEvents: ->
    self = @

    @detailsHolderEle.delegate '.close_details', 'click', => 
      # navigate to root only if close button clicked and no history present
      AC.history.register({ id: 'root' }, '/')
      @closeDetailsFunc()
    
    @resultsScrollEle.delegate '.result', 'click', (e) ->
      opts = {}

      targ = $(e.target)
      return true if targ.parents('.cta_box').length or targ.parents('.favorite_box').length or targ.parents(".lead_in").length

      result = $(this)
      url = result.attr('data-url')
      return true unless url

      return true if /(iPad)/g.test(navigator.userAgent) and !targ.hasClass('details_link')

      opts['scrollTo'] = '#advocate_notes' if targ.hasClass('view_detail_notes')

      e.preventDefault()

      # pan to center the point if it not in view
      id = result.attr('id')

      # this detail is already open
      return if id == $('#details_top').attr('data-id') && self.detailsBoxEle.hasClass('active')

      AC.history.register({ id: "details-#{id}", url: url }, url)

      # self.detailsBoxEle.addClass('active')
      self.getCommunityDetail(url, opts)

    if /(iPad)/g.test( navigator.userAgent )
      @resultsScrollEle.on 'click', '.result', (e) ->
        AC.map.deactivateAllMarkers()
        jQEle = $(this)
        AC.map.activateMarker(jQEle.attr('id'))
        jQEle.addClass('map_active')
    else
      @resultsScrollEle.on 'mouseenter mouseleave', '.result', (e) ->
        jQEle = $(this)
        if e.type == "mouseenter"
          jQEle.addClass('map_active')
          AC.map.activateMarker(jQEle.attr('id'))
        else
          jQEle.removeClass('map_active')
          AC.map.deactivateMarker(jQEle.attr('id'))

  buildCard: (d) ->
    url = "/#{d.state}/#{d.city.toLowerCase().replace(' ', '-')}/#{d.slug}"
    numberOnly = (d.tracking_number or "4043345500").replace(/[^0-9.]/g, '')

    tmpl = $(@resultsScrollEle.find('.result:first').clone())
    tmpl.attr('id', d.id).attr('data-attributes', JSON.stringify(d))
    tmpl.removeClass('saved')
    tmpl.attr('data-url', url)
    tmpl.find('img').attr('src', null).attr('src', d.hero_image)
    tmpl.find('.title a').attr('href', url).text(d.name)
    tmpl.find('.address1').text(d.address)
    tmpl.find('.address2').text("#{d.city_state} #{d.zip}")
    tmpl.find('.category').text(d.care_types.join(' | '))

    phoneEle = tmpl.find('.phone')
    phoneEle.text(numberOnly).mask('(999) 999-9999')
    formattedPhone = phoneEle.text()
    phoneEle.html("<a href='tel:#{formattedPhone}'>#{formattedPhone}</a>")

    if d.starting_at == null or d.starting_at == "0"
      tmpl.find('.price').remove()
    else 
      tmpl.find('.price').html("Starting at $#{@commaSeparateNumber(d.starting_at)}<span class='small_text'>/mo.</span>")

    tmpl

  commaSeparateNumber: (val) ->
    while (/(\d+)(\d{3})/.test(val.toString()))
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2')
    val

  dataForMarker: (id) -> 
    $("##{id}").data('attributes')

  getCommunityDetail: (url, opts={}) ->
    $.ajax
      url: url
      success: (res) =>
        @detailsHolderEle.html(res)
        @detailsBoxEle = $('#details_box')
        new Community
        setTimeout((=>
          @detailsBoxEle.addClass('active')
          setTimeout((=>
            if @bodyEle.hasClass('small_screen')
              @resultsEle.hide()
              @searchBoxEle.hide()
            @winEle.trigger('resize')
            $('#details').scrollTo(opts.scrollTo, { duration: 200 }) if opts['scrollTo']
          ), 150)
        ), 10)

  searchPerformed: (data) ->
    @resultsScrollEle.html(data)
    @resultsEle.scrollTo(0)

  bindReadMore: ->
    $(".read_more_notes").click ->
      prnt = $(this).closest(".lead_in")
      prnt.parent().find(".read_more").hide()
      prnt.parent().find(".hidden").show()
    $(".close_more_notes").click ->
      prnt = $(this).closest(".lead_in")
      prnt.parent().find(".read_more").show()
      prnt.parent().find(".hidden").hide()

window.AC ?= {}
window.AC.list = new CommunitiesList