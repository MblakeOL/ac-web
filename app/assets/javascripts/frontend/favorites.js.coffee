class Favorites
  constructor: ->
    $ =>
      @queryDom()
      @bindEvents()

  queryDom: ->
    @resultsEle = $('#results')
    @detailsHolderEle = $('#details_holder')
    @otherFavsEle = if @resultsEle.length == 0 and @detailsHolderEle.length == 0 then $('.icon_favorite') else $()

  bindEvents: ->
    # this delegate handles the list results
    @resultsEle.delegate '.icon_favorite', 'click', ->
      if !$("#user-exists").length > 0
        ele = $(this)
        fid = ele.closest(".result").attr("id")
        $("#user_favorite_login").attr("href","/users/preregistration?favorite=#{fid}&path=sign_in")
        $("#user_favorite_join").attr("href","/users/preregistration?favorite=#{fid}&path=sign_up")
        $('#user_not_logged_in').popPop         
          afterPop: ->
            AC.history.register({ id: 'popup', domID: 'user_not_logged_in' }, '/popup')
            $('#user_not_logged_in').find('p:first').text("You must be logged in to save to favorites.")
            $("#user_favorite_login").click("alert")
      else
        ele = $(this)
        resultEle = ele.parents(".result")
        commId = resultEle.attr("id")
        url = if resultEle.hasClass("saved") then "/communities/#{commId}/unfavorite" else "/communities/#{commId}/favorite"
        $.ajax
          method: "post"
          url: url
          success: (res) =>
            ele.parents('.result').toggleClass('saved')

    # these two bindings are for the detail view (ajax and regular)
    detailPageFunc = ->
      if !$("#user-exists").length > 0
        ele = $(this)
        resultEle = ele.parents("#details_top")
        fid = resultEle.attr("data-id")
        $("#user_favorite_login").attr("href","/users/preregistration?favorite=#{fid}&path=sign_in")
        $("#user_favorite_join").attr("href","/users/preregistration?favorite=#{fid}&path=sign_up")
        $('#user_not_logged_in').popPop         
          afterPop: ->
            AC.history.register({ id: 'popup', domID: 'user_not_logged_in' }, '/popup')
            $('#user_not_logged_in').find('p:first').text("You must be logged in to save to favorites.")
            $("#user_favorite_login").click("alert")
      else
        ele = $(this)
        resultEle = ele.parents("#details_top")
        commId = resultEle.attr("data-id")
        url = if resultEle.hasClass("saved") then "/communities/#{commId}/unfavorite" else "/communities/#{commId}/favorite"
        $.ajax
          method: "post"
          url: url
          success: (res) =>          
            ele.parents('#details_top').toggleClass('saved')

    @detailsHolderEle.delegate '.icon_favorite', 'click', detailPageFunc
    @otherFavsEle.click detailPageFunc

new Favorites