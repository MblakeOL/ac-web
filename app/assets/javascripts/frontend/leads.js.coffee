class LeadForm
  constructor: ->
    $ =>
      @queryDom()
      @bindForm()
      @bindPopup()
      @bindPopup(@loginLinkBox)

  queryDom: ->
    @winEle = $(window)
    @mainContainerEle = $('#check_availability_form')
    @formContainerEle = $('#lead_form_container')
    @thanksContainerEle = $('#thank_you_container')
    @leadFormHeaderEle = $('#lead_form_header')
    @resultsBoxEle = $('#results_box, #dashboard')
    @loginLinkBox = $(".login_link_box")

  queryFormDom: ->
    @formEle = $('#new_lead') 
    @titleEle = @mainContainerEle.find('h2')
    @phoneSectionEle = @mainContainerEle.find('#phone_section')
    @phoneEle = $('#lead_phone')
    @leadFormTrigger = $('.lead_form_trigger')
    @communityIdEle = $('#lead_community_id')
    @errorElements = $('#error_explanation, #new_lead .error')
    @fieldErrorEle = $('#new_lead .field_with_errors')

    # spam prevention, must be present to actually submit form
    @formEle.append("<input type='hidden' name='no_spam' value='true'/>")

  resetForm: ->
    @thanksContainerEle.hide()
    @formContainerEle.show()
    @leadFormHeaderEle.show()
    @errorElements.hide()
    @fieldErrorEle.removeClass('field_with_errors')

  bindForm: ->
    @queryFormDom()
    @phoneEle.mask '(000) 000-0000'
    @formEle.submit (e) =>
      e.preventDefault()
      @submitLead()

  bindPopup: (targetEle) ->
    self = @
    targetEle = @resultsBoxEle unless targetEle
    targetEle.click (e) =>
      targ = $(e.target)
      return true unless targ.hasClass('lead_form_trigger')
      e.preventDefault()
      # throw('Error: this popup needs a data-community-id attribute to function') if typeof targ.data('community-id') == "undefined"

      @resetForm()

      targ.popPop
        afterPop: ->
          AC.history.register({ id: 'popup', domID: targ.attr('id') }, '/popup')

          # pull from 'data-' attributes of the clicked link
          self.communityIdEle.val(@data('community-id'))
          self.titleEle.text(@data('title') or 'Request a Tour')
          if num = @data('phone')
            self.phoneSectionEle.find('.phone_number').html("<a href='tel:#{num}'>#{num}</a>")
            self.phoneSectionEle.show()
          else
            self.phoneSectionEle.hide()

  submitLead: ->
    $.ajax
      method: 'POST'
      url: @formEle.attr('action')
      data: @formEle.serialize()
      success: (res) =>
        @formContainerEle.hide()
        @leadFormHeaderEle.hide()
        @thanksContainerEle.html(res).show()
      error: (err) =>
        @leadFormHeaderEle.show()
        @formContainerEle.html(err.responseText)
        @bindForm()
        @errorElements.show()
      complete: =>
        @winEle.trigger('resize')

new LeadForm