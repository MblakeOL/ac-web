class CommunitiesAutocomplete
  constructor: ->
    @currentlyQuerying = false
    @searchUrl = ''
    @topSuggestion = null

    $ =>
      @queryDom()
      @bindEvents()
      @searchOptionsEle.hide()

  queryDom: ->
    @searchInputEle   = $('#communities-autocomplete-input')
    @searchResultsEle = $('#communities-autocomplete-output')
    @searchUrl = @searchInputEle.data('url')
    @searchBoxEle = $('#search_box')
    @searchOptionsEle = $('#search_options')
    @searchButtonEle = @searchBoxEle.find('.btn_search')
    @cancelSearchLinkEle = $('#cancel_search_link')
    @searchKindInput = $('#search_kind')
    @bodyEle = $('body')
    @winEle = $(window)

  bindEvents: ->
    @bindShowHideOptions()
    @handleKeyDown()

    self = @
    @searchResultsEle.delegate 'a', 'mouseover mouseout', (e) ->
      if e.type == 'mouseover'
        self.searchResultsEle.find('a').removeClass('active')
        $(this).addClass 'active'
      else
        self.searchResultsEle.find('a').removeClass('active')
        $(this).removeClass 'active'

    # handle search input changed
    @searchInputEle.on 'input', => @autocomplete()

    @searchButtonEle.click (e) => 
      e.preventDefault()
      @search()

    # handle suggestion clicked
    @searchResultsEle.on 'click', (e) =>
      if (href = $(e.target)) && href.attr('data-suggestion')
        @searchInputEle.val(href.attr('data-suggestion'))
        @searchKindInput.val(href.attr('data-kind'))
        @search() if href.attr('data-kind') == 'name'
        @searchResultsEle.html('')

    @cancelSearchLinkEle.click => @hideSearch()

  handleKeyDown: ->
    @searchInputEle.keydown (e) =>
      @searchKindInput.val('') unless e.keyCode == 13
      @searchBoxEle.addClass('active')
      @searchOptionsEle.slideDown('fast')
      if e.keyCode == 13 # enter key pressed
        e.preventDefault()
        # see if there was an active selection & apply it
        activeEntry = @searchResultsEle.find('a.active:first')
        if activeEntry.length == 1
          @searchInputEle.val(activeEntry.attr('data-suggestion'))
          @searchKindInput.val(activeEntry.attr('data-kind'))
        @search()

      # up or down
      if e.keyCode == 38 or e.keyCode == 40
        e.preventDefault()
        activeEntry = @searchResultsEle.find('a.active:first')
        @searchResultsEle.find('a').removeClass('active')

        if e.keyCode == 38 # up
          if activeEntry.length == 0
            @searchResultsEle.find('a:last').addClass('active')
          else
            toAdd = activeEntry.parent().prev().find('a')
            if toAdd.length == 0
              @searchResultsEle.find('a:last').addClass('active')
            else
              toAdd.addClass('active')

        else if e.keyCode == 40 # down
          if activeEntry.length == 0
            @searchResultsEle.find('a:first').addClass('active')
          else
            toAdd = activeEntry.parent().next().find('a')
            if toAdd.length == 0
              @searchResultsEle.find('a:first').addClass('active')
            else
              toAdd.addClass('active')

  hideSearch: ->
    @searchOptionsEle.slideUp 'fast', => @searchBoxEle.removeClass('active')          
    @searchResultsEle.hide()
    @searchInputEle.removeClass('active_field')

  bindShowHideOptions: ->
    clickOutFunction = (e) =>
      targ = $(e.target)
      if targ.data('suggestion') or targ.parents('#search_box').length
        # rebind this function (once) if click was within box
        @bodyEle.one('click', clickOutFunction)
        @searchResultsEle.hide()
      else
        @hideSearch()

    @searchInputEle.on 'click', =>
      @searchBoxEle.addClass('active')
      @searchOptionsEle.slideDown('fast')
      @searchResultsEle.show()
      @searchInputEle.addClass('active_field')
      @bodyEle.one 'click', clickOutFunction
      # @searchInputEle.trigger('input') if @searchInputEle.val() != ''

  search: ->
    @searchBoxEle.removeClass('active')
    @searchOptionsEle.slideUp('fast')
    @searchResultsEle.hide()
    
    # fire search event for listeners
    @winEle.trigger 'community-search', topSuggestion: @topSuggestion

  autocomplete: ->
    # throttle search if connection slow or result takes too long
    # return setTimeout((=> @autocomplete()), 200) if @currentlyQuerying

    val = @searchInputEle.val()

    # clear old results
    return @searchResultsEle.html('') if val.length == 0

    # only search if two or more characters present
    return if val.length < 2

    @currentlyQuerying = true

    url = "#{@searchUrl}?limit=12&q=#{val}"

    # perform a zip search if input is a number
    url += "&kind=zip" if /^\d+$/.test(val)

    $.ajax
      url: url
      complete: =>
        @currentlyQuerying = false
      success: (res) =>
        if @searchOptionsEle.is(':visible')
          @searchResultsEle.show()
          @searchResultsEle.html(res)
          @topSuggestion = null
          if (sugg = @searchResultsEle.find('a:first')).length == 1
            @topSuggestion = { kind: sugg.attr('data-kind'), term: sugg.attr('data-suggestion') }


new CommunitiesAutocomplete