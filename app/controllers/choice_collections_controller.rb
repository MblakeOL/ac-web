class ChoiceCollectionsController < ApplicationController

  def index
    force_login
    @choice_collections = current_user.choice_collections # TODO: where(sent: true) or something like that
  end

  def show
    @choice_collection = ChoiceCollection.find(params[:id])

    if current_user.nil?
      if @choice_collection.user.has_previously_logged_in?
        # returning user with password set, force them to login
        force_login
      elsif params[:token]
        # new account that has never logged in, need to set a password
        session[:previous_url] = request.fullpath
        redirect_to edit_user_password_path(reset_password_token: params[:token], cc_id: params[:id])
      end
      return
    end

    @favorite_ids = current_user.favorites.pluck(:community_id) # still need this in case they fav'd these
    @note_ids = current_user ? Note.where(user_id: current_user.id).pluck(:community_id) : []
    @search = {}
    
    unless current_user.internal?
      @choice_collection = current_user.choice_collections.find(params[:id])
    end

    @choice_ids = @choice_collection.communities.pluck(:community_id)
    @results = Community.search "*", { where:{ id: @choice_ids }, load: false }
    if @choice_collection.user_id == current_user.id
      UserViewedCcMailer.viewed(@choice_collection).deliver_now
      @choice_collection.update_attribute(:viewed, true)
    end
    render template: '/communities/index'
  end



end
