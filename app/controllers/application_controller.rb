class ApplicationController < ActionController::Base
  include Pundit
  before_action :configure_permitted_parameters, if: :devise_controller?


  after_filter :update_session
  before_filter :check_session
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def update_session
    return if session[:first_request_url]
    session[:first_request_url] = "#{request.protocol}#{request.host_with_port}#{request.fullpath}"
  end

  def check_session
    if current_user && session[:favorite]
      current_user.favorites.create(:community_id=>session[:favorite])
      session[:favorite] = nil
      return true
    end

    if current_user && session[:note]
      c = Community.find(session[:note])
      redirect_to community_vanity_path(c.state, c.city.parameterize, c.slug, note: true)
      session[:note] = nil
      return true
    end
  end

  def ensure_signup_complete
    # Ensure we don't go into an infinite loop
    return if action_name == 'finish_signup'

    # Redirect to the 'finish_signup' page if the user
    # email hasn't been verified yet
    if current_user && !current_user.email_verified?
      redirect_to finish_signup_path(current_user)
    end
  end

protected

  def after_sign_in_path_for(resource)
    return session[:previous_url] unless session[:previous_url].blank?
    resource.internal? ? admin_root_path : root_path
  end

  def force_login
    unless current_user
      session[:previous_url] = request.fullpath
      flash[:popup] = "You need to <a href='#{new_user_session_path}'>log in</a> to continue"
      redirect_to root_path and return
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) << [:first_name, :last_name, :phone, :headshot]
    devise_parameter_sanitizer.for(:sign_up) << [:first_name, :last_name, :phone, :headshot]
  end
end
