class DashboardController < ApplicationController

  before_action :force_login

  def index
    @choice_collections = current_user.choice_collections.order("created_at desc")
    @advocate = @choice_collections.first.try(:admin)
    @favorites_count = current_user.favorites.count
    @saved_search_count = current_user.searches.count
  end
end