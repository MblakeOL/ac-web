class CommunitiesController < ApplicationController

  def autocomplete
    @suggestions = Community.autocomplete(q: params[:q], kind: params[:kind], limit: params[:limit] || 3, show_unpublished: params[:show_unpublished])
    render layout: false
  end

  def city_lander
    index
    render action: :index unless request.xhr?
  end

  def index
    load_saved_search
    handle_matched_plus_backfill

    @results = CommunitySearch.new(@search.blank? ? params : @search).main_search

    @favorite_ids = current_user.try(:favorites).try(:pluck, :community_id) || []
    @note_ids = current_user ? Note.where(user_id: current_user.id).pluck(:community_id) : []

    render partial: 'shared/result' if request.xhr?
  end

  def map_search
    @results = CommunitySearch.new(params).main_search(map: true)
    render action: :index
  end

  def show
    load_saved_search
    params.delete(:page)

    @result = Community.friendly.find(params[:id])

    @favorite_ids = current_user.try(:favorites).try(:pluck, :community_id) || []
    @note_ids = current_user ? Note.where(user_id: current_user.id).pluck(:community_id) : []

    @floorplans = @result.floorplans.includes(:care_type).order(:starting_at).reject {|fp| fp.attributes.values.reject(&:blank?).count == 5;}.sort_by { |fp| fp.starting_at.to_i}
    @care_charge = @result.glossary_terms.where(kind: 'care charge').first
    @pdfs = @result.community_pdfs
    @results = CommunitySearch.new(params.merge(center: "#{@result.lat},#{@result.lng}", show_unpublished: false)).main_search
    @search_result = Community.search("*", where: { id: @result.id }, load: false).first

    @user_notes = current_user ? current_user.notes.where(community_id: @result.id).where(admin_id: nil) : []
    @advocate_notes = current_user ? Note.where(community_id: @result.id).where('admin_id is not null').where(user_id: current_user.id) : []
    @all_notes = @user_notes + @advocate_notes

    # chrome needs this so it will request the full page on 'back'
    response.headers['Vary'] = 'Accept'

    if request.xhr?
      render action: :show, layout: false
    else
      render action: :index
    end
  end

  def favorites
    load_saved_search
    
    @favorite_ids = current_user.favorites.pluck(:community_id)
    @note_ids = current_user ? Note.where(user_id: current_user.id).pluck(:community_id) : []

    @results = Community.search "*", { where:{ id: @favorite_ids }, load: false }
    if request.xhr?
      render partial: 'shared/result'
    else
      render action: :index
    end
  end

  def favorite
    result = Community.find(params[:id])
    Favorite.find_or_create_by(:user_id=>current_user.id, :community_id=>result.id)
    Thread.new {
      UserFavoritedPropertyMailer.favorite_alert(current_user.choice_collections.last, result, current_user).deliver_now
    }
    render :text=>"Success"
  end

  def unfavorite
    result = Community.find(params[:id])
    fav = Favorite.where(:user_id=>current_user.id, :community_id=>result.id).delete_all
    render :text=>"Success"
  end

private

  def load_saved_search
    @search = {}
    return unless params[:s]
    @search = JSON.parse(Search.find_by_id(params[:s]).try(:json) || '{}').with_indifferent_access
  end

  def handle_matched_plus_backfill
    # GET /georgia/atlanta?term=emeritus&fill
    return unless (params.has_key?(:fill) && params[:term])
    @matched_results = CommunitySearch.new(params).main_search
    params.delete(:term)
    params[:ids] = @matched_results.map(&:id)
  end
end