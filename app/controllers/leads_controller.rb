class LeadsController < ApplicationController
  before_action :set_lead, only: [:show, :edit, :update, :destroy]

  def new
    @lead = Lead.new
  end

  def create
    return render(action: :thank_you, layout: false) unless params[:no_spam]

    @lead = Lead.new(lead_params)

    if @lead.save
      Thread.new { deliver! }
      handle_user
      render action: :thank_you, layout: false
    else
      render action: :new, layout: false, status: 422
    end
  end

private

  def handle_user
    if current_user
      current_user.update_attribute(:phone, lead_params[:phone]) unless current_user.phone == lead_params[:phone]
    else
      # TODO: handle logged out existing user

      user = User.new({
        first_name: lead_params[:first_name],
        last_name: lead_params[:last_name],
        phone: lead_params[:phone],
        email: lead_params[:email],
        password: SecureRandom.hex,
      })
      
      if user.save
        @user = user
        @raw_token = @user.set_password_reset_token
      end

      # TODO: deliver email using @raw_token
      # SomeMailer.whatever(user, @raw_token).deliver_now
    end
  end

  def deliver!
    @lead.save_to_salesforce if Rails.env == 'production'
    CommunityMailer.availability(@lead, session[:first_request_url]).deliver_now
  end

  def lead_params
    params.require(:lead).permit(:first_name, :last_name, :email, :phone, :comments, :kind, :community_id, :sf_id)
  end
end
