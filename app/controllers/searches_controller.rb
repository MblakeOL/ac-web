class SearchesController < ApplicationController

  before_action :force_login

  def index
    @searches = current_user.searches.order('created_at desc')
  end

  def show
    redirect_to root_path(s: params[:id])
  end

  def create
    @search = Search.new
    @search.user_id = current_user.id
    @search.name = params[:search_name] unless params[:search_name].blank?
    @search.name ||= params[:term]

    [:authenticity_token, :controller, :action, :format, :search_name].each do |key|
      params.delete(key)
    end

    @search.json = params.to_json

    if @search.save
      head 201
    else
      head 422
    end
  end

  def destroy
    current_user.searches.find(params[:id]).destroy
    redirect_to searches_path
  end
end