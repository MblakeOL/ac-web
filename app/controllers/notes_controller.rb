class NotesController < ApplicationController
  def create
    note = Note.new
    if !current_user.internal?
      note.user_id = current_user.id
    else
      note.admin_id = current_user.id
    end
    note.community_id = params[:note][:community_id]
    note.body = params[:note][:body]
    note.save

    Thread.new {
      UserAddedNoteMailer.note_alert(current_user.choice_collections.last, note.community).deliver_now
    }
    head 200
  end

  def destroy
    note = Note.where(admin_id: current_user.id).find(params[:id])
    note.delete
    if params[:choice_collection_id]
      redirect_to admin_choice_collection_path(params[:choice_collection_id])
    else
      head 200
    end
  end
end
