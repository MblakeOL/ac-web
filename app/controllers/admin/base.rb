module Admin
  class Base < ApplicationController
    layout "admin"

    before_action :require_advocate, :load_choice_collection

    protected

    def require_advocate
      unless current_user.try(:internal?)
        flash[:popup] = "You must be logged in and an admin to access this section"
        redirect_to root_path 
      end
    end

    def require_admin
      unless current_user.try(:admin?)
        flash[:popup] = "<p>You do not have adequate admin permission to access this section</p><p>If you think this is in error, please contact your administrator.</p>"
        current_user.try(:advocate?) ? redirect_to(admin_root_path) : redirect_to(root_path)
      end
    end

    def load_choice_collection
      return unless session[:choice_collection_id]
      @choice_collection = ChoiceCollection.find(session[:choice_collection_id]) rescue nil
    end
  end

  module Communities
  end
end