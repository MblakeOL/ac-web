class Admin::CommunitiesController < Admin::Base

  before_action :require_admin, except: [:index, :search, :show]

  def index
    search
  end

  def search
    @search = {}
    @results = CommunitySearch.new(params).drilldown_search
    render json: @results if request.xhr?
  end

  def new
    @community = Community.new
    load_care_charges
    load_tags
  end

  def create
    @community = Community.new
    @community.attributes = community_params
    if @community.save!(validate: @community.validate?)
      redirect_to edit_admin_community_path(@community.friendly_id)
    else
      render action: :new, status: 422
    end
  end

  def show
    @community = Community.friendly.find(params[:id])
    @result = @community
    @floorplans = @community.floorplans.includes(:care_type).order(:starting_at).reject {|fp| fp.attributes.values.reject(&:blank?).count == 5;}.sort_by { |fp| fp.starting_at.to_i}
    @care_charge = @community.glossary_terms.where(kind: 'care charge').first
    @pdfs = @community.community_pdfs
    @search_result = Community.search("*", where: { id: @community.id }, load: false).first

    @user_notes = []
    @advocate_notes = []
    @ccc = nil

    if @choice_collection
      @user_notes = @choice_collection.user.notes.where(community_id: @result.id).where(admin_id: nil)
      @advocate_notes = Note.where(community_id: @result.id).where('admin_id is not null').where(user_id: @choice_collection.user.id)
      @ccc = @choice_collection.choice_collection_communities.where(community_id: @result.id).first
    end

    @all_notes = @user_notes + @advocate_notes

    render layout: false if request.xhr?
  end

  def edit
    @community = Community.friendly.find(params[:id])

    load_care_charges
    load_tags

    @tags = Tag.find_for_index
    @tags_by_kind = @tags.group_by &:kind
  end

  def update
    @community = Community.friendly.find(params[:id])
    @community.should_geocode = true

    # glossary terms (service/care charge)
    unless params[:community][:glossary_terms].blank?
      @community.glossary_terms = [GlossaryTerm.find(params[:community][:glossary_terms])]
    end

    # tag ids
    @community.tag_ids = (params[:community][:tags] || []).reject &:blank?

    handle_updated_at_times

    unless @community.update_attributes(community_params)
      load_care_charges
      load_tags
      return render action: :edit, status: 422
    end

    redirect_to edit_admin_community_path(@community)
  end

private

  def load_tags
    @community_tag_ids = @community.tags.pluck(:id) || []
    @tags = Tag.find_for_index
    @tags_by_kind = @tags.group_by &:kind
  end

  def load_care_charges
    @care_charges = GlossaryTerm.where(kind: 'care charge').all.reject {|x| x.name.blank? }.uniq {|x| x.name }
  end

  def community_params
    params.require(:community).permit([:name, :address, :city, :state_code, :zip, :is_live, :is_snapshot, :ranking, :internal_number, :specials, :availability, :contract_status, :va_bridge, :email, :fax_number, :management_company, :website, :invoicing_rating])
  end

  def attachments_params
    accessible = [ attachments_attributes: [ :_destroy, :id, attachment: [] ]] # extend with your own params
    params.require(:community).permit(accessible)
  end

  def handle_updated_at_times
    @community.specials_updated_at = Time.now unless community_params[:specials] == @community.specials
    @community.availability_updated_at = Time.now unless community_params[:availability] == @community.availability
  end
end