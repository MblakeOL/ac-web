class Admin::GeolocationsController < Admin::Base
  def index
    res = Geocoder.search(params[:address]).first
    loc = res.geometry['location']
    render json: { 
      address: res.formatted_address, 
      lat: loc['lat'], 
      lng: loc['lng'] 
    }
  rescue => ex
    Rails.logger.info(ex.inspect)
    head 422
  end
end