class Admin::CommunityAssociationsController < Admin::Base
  before_action :load_community
  after_action :reindex_community, only: [:create, :update, :destroy]

  before_action :require_admin
  
protected

  def load_community
    @community = Community.friendly.find(params[:community_id])
  end

  def reindex_community
    @community.reindex
  end
end