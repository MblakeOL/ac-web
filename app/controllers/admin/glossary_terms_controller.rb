class Admin::GlossaryTermsController < Admin::Base

  before_action :require_admin
  
  def index
    @glossary_terms = GlossaryTerm.order(created_at: :desc).all
  end

  def new
    @glossary_term = GlossaryTerm.new
  end

  def create
    @glossary_term = GlossaryTerm.new(glossary_term_params)
    if @glossary_term.save
      redirect_to admin_glossary_terms_path
    else
      render action: :new, status: 422
    end
  end

  def edit
    @glossary_term = GlossaryTerm.find(params[:id])
  end

  def update
    @glossary_term = GlossaryTerm.find(params[:id])
    if @glossary_term.update_attributes(glossary_term_params)
      redirect_to admin_glossary_terms_path
    else
      render action: :edit, status: 422
    end
  end

  def destroy
    GlossaryTerm.find(params[:id]).delete
    redirect_to admin_glossary_terms_path
  end

private

  def glossary_term_params
    params.require(:glossary_term).permit([:name, :kind, :description])
  end 
end