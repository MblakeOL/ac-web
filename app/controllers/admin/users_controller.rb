class Admin::UsersController < Admin::Base
  def index
    search
  end

  def search
    @results = UserSearch.new(params).admin_search
    render json: @results if request.xhr?
  end

  def new
    require_advocate
    @user = User.new
  end

  def create
    require_advocate

    @user = User.new(user_params)
    @user.role = params[:user].delete(:role).to_i if params[:user][:role] && current_user.admin?
    @user.password = SecureRandom.hex

    if @user.save
      if user_params[:send_invitation_email] == "1"
        @raw_token = @user.set_password_reset_token
        InviteUserMailer.invitation(@user, @raw_token).deliver_now
      end
      flash[:popup] = "User created!"
      redirect_to admin_user_path(@user)
    else
      render action: :new, status: 422
    end
  end

  def show
    @choice_collection = nil
    @user = User.find(params[:id])
  end

  def edit
    require_advocate
    @user = User.find(params[:id])
  end

  def update
    require_advocate
    
    @user = User.find(params[:id])
    @user.role = params[:user].delete(:role).to_i if params[:user][:role] && current_user.admin?
    @user.attributes = user_params

    if @user.save
      flash[:popup] = "User updated!"
      redirect_to admin_user_path(@user)
    else
      render action: :edit, status: 422
    end
  end

  def report
    base = User.order('created_at desc')

    @new_users = base.limit(200).includes(:choice_collections).all
    @total_count = base.count
    @daily_count = base.where('created_at > ?', Date.today - 24.hours).count
    @weekly_count = base.where('created_at > ?', Date.today-7.days).count
    @monthly_count = base.where('created_at > ?', Date.today-30.days).count
  end

private
  
  def user_params
    params.require(:user).permit([:first_name, :last_name, :email, :phone, :send_invitation_email])
  end 

end