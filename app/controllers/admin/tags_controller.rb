class Admin::TagsController < Admin::Base

  before_action :require_admin, except: [:index]

  def index
    @tags = Tag.find_for_index(params)
    @tags_by_kind = @tags.group_by &:kind
    @tag_categories = Tag.select('distinct kind').all
  end

  def new
    @tag = Tag.new
    load_cats
  end
  
  def edit
    @tag = Tag.find(params[:id])
    load_cats
  end

  def create
    @tag = Tag.new(tag_params)
    if @tag.save
      redirect_to admin_tags_path
    else
      load_cats
      render action: :new, status: 422
    end
  end

  def update
    @tag = Tag.find(params[:id])

    if params[:admin_searchable]
      @tag.update_attribute(:admin_searchable, params[:admin_searchable])
      return head 200
    else
      if @tag.update_attributes(tag_params)
        redirect_to admin_tags_path
      else
        load_cats
        render action: :edit, status: 422
      end
    end
  end

  def category_changed
    @tag = Tag.find(params[:id])
    @tag_subcats = Tag.where(kind: params[:category]).map(&:group).uniq.reject(&:blank?).map(&:titleize)
    render partial: 'subcats'
  end

private

  def tag_params
    orig = params.require(:tag).permit([:name, :kind, :new_kind, :group])
    orig[:kind] = orig[:kind].downcase if orig[:kind]
    orig[:kind] = orig[:new_kind].downcase unless orig[:new_kind].blank?
    orig
  end

  def load_cats
    @tag_categories = Tag.select('distinct kind').map(&:kind).reject(&:blank?).map(&:titleize)
    if @tag && @tag.id
      @tag_subcats = Tag.where(kind: @tag.kind).map(&:group).uniq.reject(&:blank?).map(&:titleize)
    end
  end
end