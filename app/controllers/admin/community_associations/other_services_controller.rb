class Admin::CommunityAssociations::OtherServicesController < Admin::CommunityAssociationsController
  def new
    @other_service = OtherService.new
  end

  def create
    @other_service = OtherService.new(other_service_params)
    if @other_service.save
      redirect_to admin_community_costs_path(@community)
    else
      render action: :new, status: 422
    end
  end

  def edit
    @other_service = @community.other_services.find(params[:id])
  end

  def update
    @other_service = @community.other_services.find(params[:id])
    if @other_service.update_attributes(other_service_params)
      redirect_to admin_community_costs_path(@community)
    else
      render action: :edit, status: 422
    end
  end

  def destroy
    @community.other_services.find(params[:id]).delete
    redirect_to admin_community_costs_path(@community)
  end

private

  def other_service_params
    orig = params.require(:other_service).permit([:name, :monthly_cost, :included_in_rent])
    orig.merge(community_id: @community.id)
  end
end
