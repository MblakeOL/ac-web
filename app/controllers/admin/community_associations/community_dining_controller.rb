class Admin::CommunityAssociations::CommunityDiningController < Admin::CommunityAssociationsController
  def index
    # redirect to new or edit
    if @community.community_dining.blank?
      redirect_to new_admin_community_community_dining_path(community_id: @community.slug)
    else
      redirect_to edit_admin_community_community_dining_path(community_id: @community.slug, id: @community.community_dining.id)
    end
  end

  def new
    @dining = CommunityDining.new
  end

  def edit
    @dining = @community.community_dining
  end

  def create
    @dining = CommunityDining.new(dining_params)
    if @dining.save
      redirect_to edit_admin_community_path(@community)
    else
      render action: :new, status: 422
    end
  end

  def update
    @dining = @community.community_dining
    if @dining.update_attributes(dining_params)
      redirect_to edit_admin_community_path(@community)
    else
      render action: :edit, status: 422
    end
  end

  def destroy
    @community.community_dining.delete
    redirect_to edit_admin_community_path(@community)
  end

private

  def dining_params
    orig = params.require(:community_dining).permit([:meals_per_day, :meals_description, :breakfast_hours, :lunch_hours, :dinner_hours, :kitchen_hours, :guest_breakfast_charge, :guest_lunch_charge, :guest_dinner_charge])
    orig.merge(community_id: @community.id)
  end
end