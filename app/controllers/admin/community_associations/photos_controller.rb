class Admin::CommunityAssociations::PhotosController < Admin::CommunityAssociationsController
  def index
    load_attachments
  end

  def create
    attachments = (params[:community] || {})[:attachment_ids] || []
    attachments.each do |attachment|
      unless attachment.content_type.match(/gif|png|jpg|jpeg/)
        @community.reload
        @attachments = @community.attachments
        @content_type_error = "Oops! File type must be a gif, png, or jpg"
        return render action: :index
      end
      ca = @community.attachments.new(kind: (@community.attachments.length == 0 ? :hero : :photo), position: @community.attachments.length)
      ca.tmp_file = attachment.tempfile
      begin
        ca.save
      rescue => ex
        @community.reload
        @attachments = @community.attachments
        @content_type_error = "Oops! There was a problem processing one of your images. Please try a different file."
        return render action: :index
      end
    end
    redirect_to admin_community_photos_path
  end

  # process a new sort order
  def update
    load_attachments
    direction = params[:direction]
    index = params[:index].to_i

    if direction == 'right'
      @attachments[index], @attachments[index+1] = @attachments[index+1], @attachments[index]
    elsif direction == 'left'
      @attachments[index], @attachments[index-1] = @attachments[index-1], @attachments[index]
    end
    sort_attachments
    head 200
  end

  def destroy
    @community.attachments.find(params[:id]).destroy
    load_attachments
    sort_attachments
    head 200
  end

private
  
  def load_attachments
    @attachments = @community.attachments.where('kind = ? or kind = ?', :hero, :photo).order(:position)
  end

  def sort_attachments
    @attachments.each_with_index do |a, i|
      a.skip_process = true
      a.kind = i == 0 ? :hero : :photo
      a.position = i
      a.save
    end
  end

end