class Admin::CommunityAssociations::AttachmentsController < Admin::CommunityAssociationsController
  def index
    @community_pdfs = @community.community_pdfs
    @names = CommunityPdf.pluck(:internal_name).uniq.compact
  end

  def new
  end

  def edit
  end

  def create
    params[:community][:attachment_ids].each do |attach|
      pdf = @community.community_pdfs.find_or_create_by(:internal_name=>params[:community][:internal_name])
      pdf.pdf = attach
      pdf.save!
    end
    redirect_to :action=>:index
  end

  def update
  end

  def destroy
    CommunityPdf.find(params[:id]).delete
    redirect_to :action=>:index
  end
end