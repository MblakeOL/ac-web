class Admin::CommunityAssociations::PetPoliciesController < Admin::CommunityAssociationsController
  def index
    # redirect to new or edit
    if @community.pet_policy.blank?
      redirect_to new_admin_community_pet_policy_path(community_id: @community.slug)
    else
      redirect_to edit_admin_community_pet_policy_path(community_id: @community.slug, id: @community.pet_policy.id)
    end
  end

  def new
    @pet_policy = PetPolicy.new
  end

  def edit
    @pet_policy = @community.pet_policy
  end

  def create
    @pet_policy = PetPolicy.new(pet_policy_params)
    if @pet_policy.save
      redirect_to edit_admin_community_path(@community)
    else
      render action: :new, status: 422
    end
  end

  def update
    @pet_policy = @community.pet_policy
    if @pet_policy.update_attributes(pet_policy_params)
      redirect_to edit_admin_community_path(@community)
    else
      render action: :edit, status: 422
    end
  end

  def destroy
    @community.pet_policy.delete
    redirect_to edit_admin_community_path(@community)
  end

private

  def pet_policy_params
    orig = params.require(:pet_policy).permit([:pets_allowed, :dogs_allowed, :dogs_allowed_notes, :cats_allowed, :cats_allowed_notes, :breed_restriction, :weight_limit, :pet_deposit, :pet_rent, :pet_fee, :pet_services ])
    orig.merge(community_id: @community.id)
  end
end