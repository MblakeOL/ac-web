class Admin::CommunityAssociations::DepositFeesController < Admin::CommunityAssociationsController
  def new
    @deposit_fee = DepositFee.new
  end

  def create
    @deposit_fee = DepositFee.new(deposit_fee_params)
    if @deposit_fee.save
      redirect_to admin_community_costs_path(@community)
    else
      render action: :new, status: 422
    end
  end

  def edit
    @deposit_fee = @community.deposit_fees.find(params[:id])
  end

  def update
    @deposit_fee = @community.deposit_fees.find(params[:id])
    if @deposit_fee.update_attributes(deposit_fee_params)
      redirect_to admin_community_costs_path(@community)
    else
      render action: :edit, status: 422
    end
  end

  def destroy
    @community.deposit_fees.find(params[:id]).delete
    redirect_to admin_community_costs_path(@community)
  end

private

  def deposit_fee_params
    orig = params.require(:deposit_fee).permit([:name, :fee, :deposit])
    orig.merge(community_id: @community.id)
  end
end