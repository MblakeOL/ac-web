class Admin::CommunityAssociations::FloorplansController < Admin::CommunityAssociationsController
  def index
    @floorplans = @community.floorplans.order(:kind)
  end

  def new
    @floorplan = Floorplan.new
  end

  def edit
    @floorplan = Floorplan.find(params[:id])
  end

  def update
    @floorplan = Floorplan.find(params[:id])
    # raise params.inspect
    unless params[:floorplan][:photo].blank?
      @floorplan.tmp_file = params[:floorplan][:photo][0].tempfile
      @floorplan.save!
    end
    @floorplan.update_attributes(floorplan_params)
    redirect_to :action=>:index
  end

  def create
    @floorplan = Floorplan.new
    # raise params.inspect
    unless params[:floorplan][:photo].blank?
      @floorplan.tmp_file = params[:floorplan][:photo][0].tempfile
      @floorplan.save!
    end
    @floorplan.update_attributes(floorplan_params)
    redirect_to :action=>:index
  end

  def destroy
    Floorplan.find(params[:id]).delete
    redirect_to :action=>:index
  end

  private

  def floorplan_params
    orig = params.require(:floorplan).permit("starting_at", "name", "baths", "features", "square_feet", "beds", "community_id", "created_at", "updated_at", "kind", "legacy_id", "care_type_id", "photo_file_name", "photo_content_type", "photo_file_size", "photo_updated_at")
    orig.merge(community_id: @community.id)
  end
end