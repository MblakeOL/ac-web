class Admin::ChoiceCollectionCommunitiesController < Admin::Base

  def create
    if params[:choice_collection_community][:id]
      @ccc = ChoiceCollectionCommunity.find(params[:choice_collection_community][:id])
      @ccc.update_attributes(choice_collection_community_params)
    else
      @ccc = @choice_collection.choice_collection_communities.new(choice_collection_community_params)
    end
    if @ccc.save

      @ccc.choice_collection.update_attribute(:updated_at, DateTime.now)
      unless params[:choice_collection_community][:note].blank?
        unless @ccc.notes.where(:body=>params[:choice_collection_community][:note]).first
          @ccc.notes.create!({
            body: params[:choice_collection_community][:note],
            admin_id: current_user.id,
            user_id: @ccc.choice_collection.user_id,
            community_id: @ccc.community_id,
            choice_collection_id: @ccc.choice_collection_id
          })
        end
      end
      head 201
    else
      head 422
    end
  end

  
  def destroy
    @choice_collection.update_attribute(:updated_at, DateTime.now)
    redirect_to admin_communities_path and return unless @choice_collection
    # since we don't know the id from the front end, we just send id=0 (& we'll look it up by community id)
    @ccc = @choice_collection.choice_collection_communities.where(community_id: params[:community_id]).first
    @ccc.delete
    head 200
  end

private

  def choice_collection_community_params
    params.require(:choice_collection_community).permit([:community_id])
  end 
end