class Admin::ChoiceCollectionsController < Admin::Base

  append_view_path("#{Rails.root}/app/views/mailers")

  def index
    @choice_collections = ChoiceCollection.where(admin_id: current_user.id).order(created_at: :desc).all
  end

  def show
    @sent = params[:sent]
    @choice_collection = ChoiceCollection.find(params[:id])
    set_email_fields
  end

  def create
    @choice_collection = ChoiceCollection.new(choice_collection_params)
    @choice_collection.name = "Choice Collection #1"
    set_email_fields
    if @choice_collection.save(validate: false)
      session[:choice_collection_id] = @choice_collection.id
      redirect_to admin_communities_path(edit: true)
    else
      render action: :show, status: 422
    end
  end

  def edit
    @choice_collection = ChoiceCollection.find(params[:id])
    session[:choice_collection_id] = @choice_collection.id
    redirect_to admin_communities_path(edit:true)
  end

  def update
    @choice_collection = ChoiceCollection.find(params[:id])
    if @choice_collection.update_attributes(choice_collection_params)

      # make sure no email fields are blank
      set_email_fields
      @choice_collection.save(validate: false)

      # not just a normal save, save and deliver
      if params[:deliver]

        # final validation check that the personal message exists before sending
        if @choice_collection.personal_message.blank?
          @choice_collection.errors[:personal_message] = "can't be blank"
          render action: :show, status: 422
          return
        end

        deliver!
        flash[:popup] = "Choice collection sent!"
        redirect_to admin_user_path(@choice_collection.user)
      else
        redirect_to admin_choice_collection_path(@choice_collection)
      end
    else
      render action: :show, status: 422
    end
  end

  def destroy
    cc = ChoiceCollection.find(params[:id])
    user = cc.user
    cc.delete
    session[:choice_collection_id] = nil
    redirect_to admin_user_path(user)
  end

private

  def deliver!
    @choice_collection.update_attributes(sent: true, sent_on: DateTime.now)

    unless params[:deliver][:registration_only]
      cc_link = if @choice_collection.user.has_previously_logged_in?
        choice_collection_path(@choice_collection)
      else
        choice_collection_path(@choice_collection, token: @cc.user.set_password_reset_token)
      end
      UserChoiceCollectionMailer.choice_collection(@choice_collection, "http://www.assistedchoice.com#{cc_link}").deliver_now
    end

    unless params[:deliver][:collection_only]
      @choice_collection.communities.each do |comm|
        CommunityRegistrationMailer.registration(@choice_collection, comm).deliver_now unless comm.email.blank?
      end
    end
  end

  def set_email_fields
    @cc = @choice_collection

    @cc.personal_message = render_to_string(partial: 'choice_collections/default_personal_message', layout: false) if @cc.personal_message.blank?

    @cc.user_email_subject = 'Your Personalized Choice Collection is Ready for Review' if @cc.user_email_subject.blank?
    @cc.user_email_body = render_to_string(partial: 'user_choice_collection_mailer/body', layout: false) if @cc.user_email_body.blank?
    @cc.user_email_sig = render_to_string(partial: 'user_choice_collection_mailer/sig', layout: false) if @cc.user_email_sig.blank?

    @cc.comm_email_subject = "AssistedChoice Client Registration - {{community name will go here}}" if @cc.comm_email_subject.blank?
    @cc.comm_email_body = render_to_string(partial: 'community_registration_mailer/body', layout: false) if @cc.comm_email_body.blank?
    @cc.comm_email_sig = render_to_string(partial: 'community_registration_mailer/sig', layout: false) if @cc.comm_email_sig.blank?
  end

  def choice_collection_params
    parameters = params.require(:choice_collection).permit([:user_id, :name, :sent, :sent_on, :advocate_user_id, :personal_message, :user_email_subject, :user_email_body, :user_email_sig, :comm_email_subject, :comm_email_body, :comm_email_sig])
    parameters.merge(admin_id: current_user.id)
  end 

end