class Users::RegistrationsController < Devise::RegistrationsController
  after_filter :add_account

  def update
    unless params[:user][:headshot].blank?
      @user.headshot_tmp_file = params[:user][:headshot].tempfile
    end
    begin
      flash[:popup] = "Account successfully updated"
      @user.update_attributes(user_params)
    rescue => ex
      flash[:popup] = "There was an error processing your account update."
      if params[:user][:headshot]
        flash[:popup] += " This probably means your headshot image is corrupt or not valid."
      end
    end
    redirect_to edit_registration_path(current_user)
  end

  def user_params
    accessible = [ :name, :email, :first_name, :last_name, :phone ] # extend with your own params
    accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params.require(:user).permit(accessible)
  end

  def delete_headshot
    current_user.update_attribute(:headshot_file_name, nil)
    flash[:popup] = "Headshot deleted."
    redirect_to edit_registration_path(current_user)
  end  

  protected

  def add_account
    if action_name == 'create' && resource.persisted? # user is created successfuly
      begin
        UserCreatedMailer.notifier(@user).deliver_now
      rescue => ex
        Rails.logger.error("An error occured when sending user registration mailer!")
        Rails.logger.error(ex.message)
        Rails.logger.error(ex.backtrace.join("\n"))
      end
    end
 end
end